﻿using System;
using PWAPICommon;
using PWAPICommon.Servers;
using PWAPICommon.Config;

namespace PW
{
    class ServerTest
    {
        public static void Main(string[] args)
        {
            PWServerBase.AddLogMessageCallback logMessageCallback = (x, y) => { Console.WriteLine(x); };

            ConfigSettings.Load("settings.xml");
            ConfigSettings.SetKey("WebPort", "8081");

            PWServerApp app = new PWServerApp(args, logMessageCallback);
            PWServer server = new PWServer(20, "DevilsJin", "EU", 1, 1, logMessageCallback, ELoggingLevel.ELL_Verbose, app);
            PWLoginCache loginCache = new PWLoginCache(server);
            PWSocialServer socialServer = new PWSocialServer(20, "DevilsJin", "EU", 1, 1, logMessageCallback, ELoggingLevel.ELL_Verbose, app);

            server.Port = 7887;
            server.StartServer();
            server.AddConnectionList();
            server.AddDBConnection("Server = localhost; Database = blrevive; User Id = SA; Password = M3od4na!r;", "local");
            var sql = server.GetResource<PWResourceSQL>();
            sql.CheckHealth();
            var offerCache = server.AddOfferCache();
            var pmCache = server.AddPMCache();
            var rankedMgr = server.AddRankedManager();
            var storeCache = server.AddStoreCache();
            var transactionCache = server.AddTransactionCache();
            var superServer = server.AddSuperServer();

            Console.ReadLine();
        }
    }
}
