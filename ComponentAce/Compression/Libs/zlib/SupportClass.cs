﻿// Decompiled with JetBrains decompiler
// Type: ComponentAce.Compression.Libs.zlib.SupportClass
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace ComponentAce.Compression.Libs.zlib
{
  [ComVisible(true)]
  public class SupportClass
  {
    public static long Identity(long literal) => literal;

    public static ulong Identity(ulong literal) => literal;

    public static float Identity(float literal) => literal;

    public static double Identity(double literal) => literal;

    public static int URShift(int number, int bits) => number >= 0 ? number >> bits : (number >> bits) + (2 << ~bits);

    public static int URShift(int number, long bits) => SupportClass.URShift(number, (int) bits);

    public static long URShift(long number, int bits) => number >= 0L ? number >> bits : (number >> bits) + (2L << ~bits);

    public static long URShift(long number, long bits) => SupportClass.URShift(number, (int) bits);

    public static int ReadInput(Stream sourceStream, byte[] target, int start, int count)
    {
      if (target.Length == 0)
        return 0;
      byte[] buffer = new byte[target.Length];
      int num = sourceStream.Read(buffer, start, count);
      if (num == 0)
        return -1;
      for (int index = start; index < start + num; ++index)
        target[index] = buffer[index];
      return num;
    }

    public static int ReadInput(TextReader sourceTextReader, byte[] target, int start, int count)
    {
      if (target.Length == 0)
        return 0;
      char[] buffer = new char[target.Length];
      int num = sourceTextReader.Read(buffer, start, count);
      if (num == 0)
        return -1;
      for (int index = start; index < start + num; ++index)
        target[index] = (byte) buffer[index];
      return num;
    }

    public static byte[] ToByteArray(string sourceString) => Encoding.UTF8.GetBytes(sourceString);

    public static char[] ToCharArray(byte[] byteArray) => Encoding.UTF8.GetChars(byteArray);
  }
}
