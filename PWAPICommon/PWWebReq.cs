﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PWWebReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Queries;
using PWAPICommon.Resources;
using System;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class PWWebReq : PWRequestWrapper
  {
    private const int BufferSize = 1024;
    private byte[] Buffer;
    private StringBuilder RequestData;
    private WebRequest WebRequest;
    private Stream ResponseStream;
    private object RequestObject;
    private string RequestURL;
    private PW509Cert RequestCert;
    private Encoding RequestEncoding = (Encoding) new UTF8Encoding();
    private Decoder StreamDecode = Encoding.UTF8.GetDecoder();
    private int RequestTimeoutMS;
    private Type ResultType;
    private object ResultObject;
    private EWebRequestType RequestType;
    private MemoryStream memoryStream;
    private DateTime CreateTime;

    public string ReadData => this.RequestData.ToString();

    public object Result => this.ResultObject;

    public PWWebReq(
      PWConnectionBase InConnection,
      string InURL,
      bool bUseSSL,
      EWebRequestType WebType,
      object ObjectToSend,
      Type ObjectTypeToRead)
      : base(InConnection)
    {
      this.RequestURL = InURL;
      if (bUseSSL)
      {
        this.RequestCert = this.OwningServer.GetResource<PW509Cert>();
        if (this.RequestCert == null)
          this.Log("Failed to find PW509Cert Resource!", false, ELoggingLevel.ELL_Errors);
      }
      this.RequestType = WebType;
      this.RequestTimeoutMS = 20000;
      this.Buffer = new byte[1024];
      this.RequestData = new StringBuilder(1024);
      this.RequestObject = ObjectToSend;
      this.ResultType = ObjectTypeToRead;
      this.WebRequest = (WebRequest) this.CreateDefaultRequest();
      if (ObjectToSend != null)
      {
        this.memoryStream = new MemoryStream();
        XmlWriter xmlWriter = XmlWriter.Create((Stream) this.memoryStream);
        new XmlSerializer(ObjectToSend.GetType()).Serialize(xmlWriter, ObjectToSend);
      }
      this.CreateTime = PWServerBase.CurrentTime;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_WebWrapper;

    private void RequestTimeout(object state, bool timedOut)
    {
      if (!timedOut || !(state is HttpWebRequest httpWebRequest))
        return;
      this.Log("WebRequest Timed Out for URL [" + this.RequestURL + "]", false, ELoggingLevel.ELL_Warnings);
      httpWebRequest.Abort();
    }

    protected virtual HttpWebRequest CreateDefaultRequest()
    {
      HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(this.RequestURL);
      httpWebRequest.Method = this.RequestType == EWebRequestType.EWRT_POST ? "POST" : "GET";
      httpWebRequest.ContentType = "text/xml";
      httpWebRequest.KeepAlive = false;
      httpWebRequest.ProtocolVersion = HttpVersion.Version10;
      httpWebRequest.PreAuthenticate = true;
      httpWebRequest.Timeout = this.RequestTimeoutMS;
      httpWebRequest.ReadWriteTimeout = this.RequestTimeoutMS;
      if (this.RequestCert != null)
        httpWebRequest.ClientCertificates.Add((X509Certificate) this.RequestCert);
      return httpWebRequest;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      try
      {
        ThreadPool.RegisterWaitForSingleObject((this.memoryStream == null ? this.WebRequest.BeginGetResponse(new AsyncCallback(this.WebResponseCallback), (object) this) : this.WebRequest.BeginGetRequestStream(new AsyncCallback(this.GetRequestStreamCallback), (object) this.WebRequest)).AsyncWaitHandle, new WaitOrTimerCallback(this.RequestTimeout), (object) this.WebRequest, this.RequestTimeoutMS, true);
        return true;
      }
      catch (WebException ex)
      {
        this.HandleException((Exception) ex);
      }
      return false;
    }

    private void GetRequestStreamCallback(IAsyncResult asynchronousResult)
    {
      try
      {
        Stream requestStream = this.WebRequest.EndGetRequestStream(asynchronousResult);
        string s = this.RequestEncoding.GetString(this.memoryStream.ToArray());
        this.Log("Sending Web Request [" + s + "]", false, ELoggingLevel.ELL_Verbose);
        byte[] bytes = this.RequestEncoding.GetBytes(s);
        requestStream.Write(bytes, 0, bytes.Length);
        requestStream.Close();
        this.WebRequest.BeginGetResponse(new AsyncCallback(this.WebResponseCallback), (object) this);
      }
      catch (WebException ex)
      {
        this.HandleException((Exception) ex);
      }
    }

    private void HandleException(Exception ex)
    {
      TimeSpan timeSpan = PWServerBase.CurrentTime - this.CreateTime;
      if (this.CanRetry)
      {
        this.Log("WebRequest Exception for URL [" + this.RequestURL + "] After [" + (object) timeSpan.TotalSeconds + "s] Retries Left [" + (object) this.RetriesLeft + (object) 1 + "] with Exception: " + ex.ToString(), false, ELoggingLevel.ELL_Warnings);
        this.WebRequest = (WebRequest) this.CreateDefaultRequest();
        this.SubmitServerQuery();
      }
      else
      {
        this.Log("WebRequest Exception for URL [" + this.RequestURL + "] After [" + (object) timeSpan.TotalSeconds + "s] with Exception: " + ex.ToString(), false, ELoggingLevel.ELL_Errors);
        this.ProcessServerResultDefault();
      }
    }

    private void WebResponseCallback(IAsyncResult async)
    {
      try
      {
        this.ResponseStream = this.WebRequest.EndGetResponse(async).GetResponseStream();
        this.ResponseStream.BeginRead(this.Buffer, 0, 1024, new AsyncCallback(this.WebReadCallBack), (object) this);
      }
      catch (WebException ex)
      {
        this.HandleException((Exception) ex);
      }
    }

    private void WebReadCallBack(IAsyncResult asyncResult)
    {
      int num = this.ResponseStream.EndRead(asyncResult);
      if (num > 0)
      {
        char[] chars = new char[1024];
        this.StreamDecode.GetChars(this.Buffer, 0, num, chars, 0);
        this.RequestData.Append(Encoding.ASCII.GetString(this.Buffer, 0, num));
        this.ResponseStream.BeginRead(this.Buffer, 0, 1024, new AsyncCallback(this.WebReadCallBack), (object) this);
      }
      else
      {
        if (this.RequestData.Length > 0)
        {
          byte[] bytes = Encoding.UTF8.GetBytes(this.RequestData.ToString());
          this.Log("Received Web Response [" + this.RequestData.ToString() + "]", false, ELoggingLevel.ELL_Verbose);
          try
          {
            if (this.ResultType != (Type) null)
              this.ResultObject = new XmlSerializer(this.ResultType).Deserialize((Stream) new MemoryStream(bytes));
            this.ProcessServerResultDefault();
          }
          catch (Exception ex)
          {
            if (this.ResultType != (Type) null)
              this.ResultObject = Activator.CreateInstance(this.ResultType);
            this.HandleException(ex);
          }
        }
        this.ResponseStream.Close();
      }
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (this.ResultType == (Type) null && this.RequestData.Length > 0)
        return true;
      return this.ResultType != (Type) null && this.ResultObject != null;
    }
  }
}
