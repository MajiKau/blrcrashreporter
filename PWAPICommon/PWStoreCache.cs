﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PWStoreCache
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries;
using PWAPICommon.Wrappers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Xml;
using System.Xml.Serialization;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class PWStoreCache : PWCacheBase
  {
    private ConcurrentDictionary<Guid, StoreItem> AllItems;
    private ConcurrentDictionary<int, ConcurrentDictionary<string, PWStoreCache.StoreTagInfo>> CachedStoreQueries;
    private ConcurrentDictionary<int, PWSqlItemQueryKeyedReq<StoreItem, int>> PendingRefreshes;
    private MessageSerializer Serializer;
    private XmlTinyAttribute[] IgnoreAttributes;

    public PWStoreCache(PWServerBase InServer, PWConnectionBase InConnection)
      : base(InServer, InConnection)
    {
      this.OwningServer = InServer;
      this.AllItems = new ConcurrentDictionary<Guid, StoreItem>();
      this.CachedStoreQueries = new ConcurrentDictionary<int, ConcurrentDictionary<string, PWStoreCache.StoreTagInfo>>();
      this.PendingRefreshes = new ConcurrentDictionary<int, PWSqlItemQueryKeyedReq<StoreItem, int>>();
      this.Serializer = new MessageSerializer();
      this.IgnoreAttributes = new XmlTinyAttribute[5]
      {
        new XmlTinyAttribute(typeof (StoreItem), "InitialQuantity"),
        new XmlTinyAttribute(typeof (StoreItem), "UserType"),
        new XmlTinyAttribute(typeof (StoreItem), "FriendlyName"),
        new XmlTinyAttribute(typeof (StoreItem), "ActivationData"),
        new XmlTinyAttribute(typeof (StoreItem), "VisibilityLevel")
      };
    }

    private void LoadItemTags(string ItemTagPath, out PWStoreTagItem[] StoreTags)
    {
      try
      {
        FileStream fileStream = new FileStream(ItemTagPath, FileMode.Open, FileAccess.Read);
        PWStoreInfo pwStoreInfo = (PWStoreInfo) new XmlSerializer(typeof (PWStoreInfo)).Deserialize(XmlReader.Create((Stream) fileStream));
        StoreTags = pwStoreInfo.StoreTags;
        fileStream.Close();
        this.Log("Item tags loaded successfully", ELoggingLevel.ELL_Informative);
      }
      catch (FileNotFoundException ex)
      {
        StoreTags = new PWStoreTagItem[0];
        this.Log("No item tags file found: " + ItemTagPath, ELoggingLevel.ELL_Warnings);
      }
      catch (Exception ex)
      {
        StoreTags = new PWStoreTagItem[0];
        this.Log("Failed to load item tags file: \n" + ex.ToString(), ELoggingLevel.ELL_Errors);
      }
    }

    public void ProcessEmbeddedOffers(PWStoreInfo StoreInfo, out TimeSpan ReadTimeSpan)
    {
      PWOfferCache resource = this.OwningServer.GetResource<PWOfferCache>();
      if (resource != null)
      {
        resource.ProcessEmbeddedOffers(StoreInfo, out ReadTimeSpan);
      }
      else
      {
        ReadTimeSpan = new TimeSpan(1, 0, 0);
        StoreInfo.OfferItems = new PWStoreOfferItem[0];
      }
    }

    public void GenerateMessageData(int UserType, string TagSet) => this.GenerateMessageData(UserType, TagSet, this.GetCachedItems(UserType, TagSet));

    public void GenerateMessageData(int UserType, string TagSet, List<StoreItem> ReadItems)
    {
      lock (this.CachedStoreQueries)
      {
        if (ReadItems == null)
          ReadItems = this.GetCachedItems(UserType, TagSet);
        if (ReadItems == null)
          return;
        PWStoreCache.StoreTagInfo storeTagInfo = new PWStoreCache.StoreTagInfo();
        storeTagInfo.Items = ReadItems;
        PWStoreInfo pwStoreInfo = new PWStoreInfo();
        string OutValue = "";
        if (this.OwningServer.GetConfigValue<string>("ItemTagPath", out OutValue) && !string.IsNullOrEmpty(OutValue))
          this.LoadItemTags(OutValue, out pwStoreInfo.StoreTags);
        pwStoreInfo.StoreItems = ReadItems.ToArray();
        TimeSpan ReadTimeSpan;
        this.ProcessEmbeddedOffers(pwStoreInfo, out ReadTimeSpan);
        storeTagInfo.ReadTime = PWServerBase.CurrentTime;
        storeTagInfo.NextReadTime = storeTagInfo.ReadTime + ReadTimeSpan;
        pwStoreInfo.NextReadTime = storeTagInfo.NextReadTime;
        this.NextReadTimeSpan = ReadTimeSpan;
        this.Log("Store Cache Refresh Time is " + ReadTimeSpan.ToString(), ELoggingLevel.ELL_Informative);
        MessageSerializer.Encode(EMessageType.EMT_QueryStoreUser, this.Serializer.Compress(this.Serializer.Serialize<PWStoreInfo>("T", pwStoreInfo, this.IgnoreAttributes)), out storeTagInfo.PreserializedMessage);
        ConcurrentDictionary<string, PWStoreCache.StoreTagInfo> concurrentDictionary = (ConcurrentDictionary<string, PWStoreCache.StoreTagInfo>) null;
        if (this.CachedStoreQueries.TryGetValue(UserType, out concurrentDictionary))
        {
          concurrentDictionary[TagSet] = storeTagInfo;
        }
        else
        {
          concurrentDictionary = new ConcurrentDictionary<string, PWStoreCache.StoreTagInfo>();
          if (!concurrentDictionary.TryAdd(TagSet, storeTagInfo) || !this.CachedStoreQueries.TryAdd(UserType, concurrentDictionary))
            this.Log("Failed to Apply Cached Store Items [" + UserType.ToString() + "\"" + TagSet + "\"]", ELoggingLevel.ELL_Warnings);
        }
        this.AllItems = new ConcurrentDictionary<Guid, StoreItem>((IEnumerable<KeyValuePair<Guid, StoreItem>>) ReadItems.ToDictionary<StoreItem, Guid>((Func<StoreItem, Guid>) (x => x.ItemGuid)));
      }
    }

    public override void MarkDirty()
    {
      this.Log("Store Cache Marked Dirty", ELoggingLevel.ELL_Informative);
      lock (this.CachedStoreQueries)
      {
        foreach (ConcurrentDictionary<string, PWStoreCache.StoreTagInfo> concurrentDictionary in (IEnumerable<ConcurrentDictionary<string, PWStoreCache.StoreTagInfo>>) this.CachedStoreQueries.Values)
        {
          foreach (PWStoreCache.StoreTagInfo storeTagInfo in (IEnumerable<PWStoreCache.StoreTagInfo>) concurrentDictionary.Values)
            storeTagInfo.NextReadTime = PWServerBase.CurrentTime;
        }
      }
    }

    protected override PWRequestBase CreateRefreshRequest()
    {
      PWSqlItemQueryKeyedReq<StoreItem, int> itemQueryKeyedReq1 = (PWSqlItemQueryKeyedReq<StoreItem, int>) null;
      if (!this.PendingRefreshes.TryGetValue(0, out itemQueryKeyedReq1))
      {
        PWSqlItemQueryKeyedReq<StoreItem, int> itemQueryKeyedReq2 = new PWSqlItemQueryKeyedReq<StoreItem, int>(this.LocalConnection);
        if (this.PendingRefreshes.TryAdd(0, itemQueryKeyedReq2))
        {
          this.Log("Store Cache out of date, Refreshing...", ELoggingLevel.ELL_Informative);
          itemQueryKeyedReq2.Key = 0;
          return (PWRequestBase) itemQueryKeyedReq2;
        }
      }
      return (PWRequestBase) null;
    }

    protected override bool ParseCacheRefreshResults(PWRequestBase Wrapper)
    {
      if (Wrapper is PWSqlItemQueryReqBase<StoreItem> itemQueryReqBase && itemQueryReqBase.Successful)
      {
        List<StoreItem> list = itemQueryReqBase.ResponseItems.Distinct<StoreItem>((IEqualityComparer<StoreItem>) new StoreItemComparer()).ToList<StoreItem>();
        switch (Wrapper)
        {
          case PWSqlItemQueryAllReq<StoreItem> _:
            this.GenerateMessageData(0, "", list);
            return true;
          case PWSqlItemQueryKeyedReq<StoreItem, int> itemQueryKeyedReq:
            this.GenerateMessageData(itemQueryKeyedReq.Key, "", list);
            this.PendingRefreshes.TryRemove(itemQueryKeyedReq.Key, out itemQueryKeyedReq);
            return true;
        }
      }
      return false;
    }

    public List<StoreItem> GetCachedItems() => this.GetCachedItems(0, "");

    protected void ConditionalAddPendingQuery(int UserType)
    {
      PWSqlItemQueryKeyedReq<StoreItem, int> itemQueryKeyedReq1 = (PWSqlItemQueryKeyedReq<StoreItem, int>) null;
      if (this.PendingRefreshes.TryGetValue(UserType, out itemQueryKeyedReq1))
        return;
      PWSqlItemQueryKeyedReq<StoreItem, int> itemQueryKeyedReq2 = new PWSqlItemQueryKeyedReq<StoreItem, int>(this.LocalConnection);
      if (!this.PendingRefreshes.TryAdd(UserType, itemQueryKeyedReq2))
        return;
      itemQueryKeyedReq2.Key = UserType;
      itemQueryKeyedReq2.ServerResultProcessor = new ProcessDelegate(this.ParseCacheRefreshResults);
      itemQueryKeyedReq2.SubmitServerQuery();
    }

    public List<StoreItem> GetCachedItems(int UserType, string TagSet)
    {
      List<StoreItem> OutItems = (List<StoreItem>) null;
      byte[] OutMessage = (byte[]) null;
      this.GetCachedItemsAndMessage(UserType, TagSet, out OutItems, out OutMessage);
      return OutItems;
    }

    public byte[] GetCachedMessage(int UserType, string TagSet)
    {
      List<StoreItem> OutItems = (List<StoreItem>) null;
      byte[] OutMessage = (byte[]) null;
      this.GetCachedItemsAndMessage(UserType, TagSet, out OutItems, out OutMessage);
      return OutMessage;
    }

    public void GetCachedItemsAndMessage(
      int UserType,
      string TagSet,
      out List<StoreItem> OutItems,
      out byte[] OutMessage)
    {
      ConcurrentDictionary<string, PWStoreCache.StoreTagInfo> concurrentDictionary = (ConcurrentDictionary<string, PWStoreCache.StoreTagInfo>) null;
      PWStoreCache.StoreTagInfo storeTagInfo = (PWStoreCache.StoreTagInfo) null;
      OutItems = (List<StoreItem>) null;
      OutMessage = (byte[]) null;
      if (this.CachedStoreQueries.TryGetValue(UserType, out concurrentDictionary) && concurrentDictionary.TryGetValue(TagSet, out storeTagInfo))
      {
        OutItems = storeTagInfo.Items;
        OutMessage = storeTagInfo.PreserializedMessage;
      }
      if (concurrentDictionary != null && storeTagInfo != null && !storeTagInfo.IsDirty)
        return;
      this.ConditionalAddPendingQuery(UserType);
    }

    public StoreItem GetCachedItem(Guid ForGuid)
    {
      StoreItem storeItem = (StoreItem) null;
      this.AllItems.TryGetValue(ForGuid, out storeItem);
      return storeItem;
    }

    public override Dictionary<string, string> CheckHealth()
    {
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      List<StoreItem> cachedItems = this.GetCachedItems();
      byte[] numArray = this.GetCachedMessage(0, "") ?? new byte[0];
      if (cachedItems != null)
      {
        dictionary["StoreCache:Status"] = "OK";
        dictionary["StoreCache:Count"] = cachedItems.Count.ToString();
        dictionary["StoreCache:Size"] = numArray.Length.ToString();
        dictionary["StoreCache:Age"] = this.Age.ToString();
      }
      else
        dictionary["StoreCache:Status"] = "PENDING";
      return dictionary;
    }

    private class StoreTagInfo
    {
      public DateTime ReadTime;
      public DateTime NextReadTime;
      public List<StoreItem> Items;
      public byte[] PreserializedMessage;

      public bool IsDirty => PWServerBase.CurrentTime > this.NextReadTime;
    }
  }
}
