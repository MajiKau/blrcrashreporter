﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Clients.GeoLocation
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Device.Location;
using System.Runtime.InteropServices;

namespace PWAPICommon.Clients
{
  [ComVisible(true)]
  public class GeoLocation
  {
    private GeoCoordinate Coord;
    public string RegionTag;
    public string Country;
    public string City;

    public GeoLocation(double InLat, double InLong)
    {
      this.Coord = new GeoCoordinate(InLat, InLong);
      this.RegionTag = "UNKNOWN";
      this.Country = "UNKNOWN";
      this.City = "UNKNOWN";
    }

    public static double ComputeDistance(GeoLocation A, GeoLocation B) => A.Coord.GetDistanceTo(B.Coord);
  }
}
