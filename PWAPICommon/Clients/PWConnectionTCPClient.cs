﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Clients.PWConnectionTCPClient
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Queries;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace PWAPICommon.Clients
{
  [ComVisible(true)]
  public class PWConnectionTCPClient : PWConnectionTCPBase
  {
    public ReceiveMessageCallback OnReceiveMessage;
    public ProcessDelegate DefaultClientResultProcessor;
    private List<PWRequestBase> RequestsAwaitingResponse;

    public PWConnectionTCPClient(PWServerBase InOwningServer, Socket InSocket)
      : base(InOwningServer, InSocket)
      => this.RequestsAwaitingResponse = new List<PWRequestBase>();

    public override void WaitForResponse(PWRequestBase InRequest)
    {
      if (this.RequestsAwaitingResponse.FindIndex((Predicate<PWRequestBase>) (x => x == InRequest)) != -1)
        return;
      this.RequestsAwaitingResponse.Add(InRequest);
    }

    protected override bool ReceiveValidMessage(PWRequestBase InRequest, byte[] InOriginalData)
    {
      InRequest.ClientResultProcessor = this.DefaultClientResultProcessor;
      for (int index = 0; index < this.RequestsAwaitingResponse.Count; ++index)
      {
        PWRequestBase pwRequestBase = this.RequestsAwaitingResponse[index];
        if (pwRequestBase.MatchesRequest(InRequest))
        {
          InRequest = pwRequestBase;
          InRequest.ParseClientQuery(InOriginalData);
          this.RequestsAwaitingResponse.RemoveAt(index);
          break;
        }
      }
      bool flag = InRequest.ProcessClientResultBase((PWRequestBase) new PWRequestWrapperObject(InRequest, (object) null));
      if (this.OnReceiveMessage != null)
      {
        int num = this.OnReceiveMessage(InRequest.MessageType, InRequest) ? 1 : 0;
      }
      return flag;
    }

    protected override bool ParseMessage(PWRequestBase InRequest, byte[] InMessage) => InRequest.ParseClientQuery(InMessage);

    public override bool SubmitQuery(PWRequestBase InQuery)
    {
      try
      {
        return InQuery.SubmitClientQuery();
      }
      catch (Exception ex)
      {
        this.Log("SubmitQuery Exception: " + ex.ToString(), false, ELoggingLevel.ELL_Errors);
      }
      return false;
    }

    public bool CancelQuery(PWRequestBase InQuery)
    {
      this.RequestsAwaitingResponse.RemoveAll((Predicate<PWRequestBase>) (x => x == InQuery));
      return true;
    }
  }
}
