﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Servers.PWHttpServer
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Config;
using PWAPICommon.Resources;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace PWAPICommon.Servers
{
  [ComVisible(true)]
  public class PWHttpServer : IPWResource
  {
    private HttpListener Listener;
    private IHttpResource DataProvider;
    private PWServerBase OwningServer;
    private string ServerName;
    private string URI;

    public PWHttpServer(string InServerName, PWServerBase InOwner, IHttpResource InProvider)
    {
      this.DataProvider = InProvider;
      this.OwningServer = InOwner;
      this.ServerName = InServerName;
      this.Listener = new HttpListener();
      string OutValue = "81";
      ConfigSettings.GetSetting("WebPort", ref OutValue, "81");
      int result;
      int.TryParse(OutValue, out result);
      this.URI = "http://localhost:" + result.ToString() + "/" + this.ServerName + "/";
      this.Listener.Prefixes.Add(this.URI);
      this.Log("Adding Http Listener on URI [" + this.URI + "]", ELoggingLevel.ELL_Informative);
    }

    private void Log(string InString, ELoggingLevel InLogLevel)
    {
      if (this.OwningServer == null)
        return;
      this.OwningServer.Log(InString, (PWConnectionBase) null, false, InLogLevel);
    }

    public void RequestReceived(IAsyncResult result)
    {
      try
      {
        ThreadPool.QueueUserWorkItem(new WaitCallback(this.ProcessRequest), (object) this.Listener.EndGetContext(result));
        this.GetReqest();
      }
      catch (Exception ex)
      {
        this.Log("HTTP Listener Exception: " + ex.ToString(), ELoggingLevel.ELL_Warnings);
      }
    }

    public void BrowseTo()
    {
      if (this.Listener == null || !this.Listener.IsListening || this.Listener.Prefixes.Count <= 0)
        return;
      Process.Start(this.URI);
    }

    public void StartResource()
    {
      this.Log("Http Listener Starting", ELoggingLevel.ELL_Informative);
      try
      {
        this.Listener.Start();
        this.GetReqest();
      }
      catch (Exception ex)
      {
        this.Log("Http Server Startup Exception: " + ex.ToString(), ELoggingLevel.ELL_Errors);
        this.Listener = (HttpListener) null;
      }
    }

    public void GetReqest()
    {
      if (this.Listener == null)
        return;
      this.Listener.BeginGetContext(new AsyncCallback(this.RequestReceived), (object) null);
    }

    public void StopResource()
    {
      this.Log("Http Listener Stopping", ELoggingLevel.ELL_Informative);
      if (this.Listener == null)
        return;
      this.Listener.Stop();
    }

    public Dictionary<string, string> CheckHealth() => new Dictionary<string, string>();

    public void ProcessRequest(object o)
    {
      HttpListenerContext httpListenerContext = o as HttpListenerContext;
      HttpListenerRequest request = httpListenerContext.Request;
      HttpListenerResponse response = httpListenerContext.Response;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.Append(this.PrintHeader());
      List<HttpData> webData = this.DataProvider.GetWebData();
      webData.Add(this.PrintAPILinks());
      stringBuilder.Append(this.PrintWebData(webData));
      stringBuilder.Append(this.PrintFooter());
      byte[] bytes = Encoding.UTF8.GetBytes(stringBuilder.ToString());
      response.ContentLength64 = (long) bytes.Length;
      Stream outputStream = response.OutputStream;
      outputStream.Write(bytes, 0, bytes.Length);
      outputStream.Close();
      response.Close();
    }

    public string PrintWebData(List<HttpData> InData)
    {
      string str1 = "<hr/><p><h2>Shortcuts</h2><br />";
      string str2 = "";
      foreach (HttpData httpData in InData)
      {
        str1 = str1 + "<a href=\"#" + httpData.SectionName + "\">" + httpData.SectionName + "</a><br />";
        str2 = str2 + "<a name=\"" + httpData.SectionName + "\"></a>";
        str2 = str2 + "<hr/><br /><p><h2>" + httpData.SectionName + "</h2><br />";
        str2 += httpData.SectionData;
      }
      return str1 + str2;
    }

    public string PrintHeader() => "" + "<HTML>" + "<HEAD>" + "<meta http-equiv=\"refresh\" content=\"60\">" + "<title>" + this.ServerName + "</title>" + "</HEAD>" + "<BODY>" + "<h1><b><center>" + this.ServerName + "</center></b></h1>";

    public string PrintFooter() => "" + "</BODY></HTML>";

    public HttpData PrintAPILinks()
    {
      HttpData httpData;
      httpData.SectionName = "Web Service Links";
      httpData.SectionData = "";
      string OutValue = "";
      if (ConfigSettings.GetSetting("WebServiceURL", ref OutValue))
      {
        string str = "" + "<a href=\"" + OutValue + "ItemServer.asmx\">Item Server</a><br /> " + "<a href=\"" + OutValue + "SocialServer.asmx\">Social Server</a><br /> " + "<a href=\"" + OutValue + "SuperServer.asmx\">Super Server</a></p><br /> ";
        httpData.SectionData = str;
      }
      return httpData;
    }
  }
}
