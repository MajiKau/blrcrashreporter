﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Servers.PWServerApp
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Common;
using PWAPICommon.Config;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading;
using System.Timers;
using System.Windows.Forms;

namespace PWAPICommon.Servers
{
  [ComVisible(true)]
  public class PWServerApp : IHttpResource
  {
    private string[] StartArgs;
    private EventLog eventLog;
    private PWServerBase.AddLogMessageCallback UILogCallback;
    private int NumDeadThreads;
    private SmtpClient MailClient;
    private ConcurrentDictionary<ELoggingLevel, PWServerApp.MailQueueItem> MailQueue;
    private Thread MailThread;
    private PerformanceCounter cpuCounter;
    private PerformanceCounter ramLeftCounter;
    private float RamTotalMB;
    private WeightedAverage CPUAverage;
    private System.Timers.Timer HealthMonitorTimer;
    private Dictionary<string, PWServerBase> ServerMap;
    private string AssemblyVersion;
    private System.Timers.Timer CPUUsageFreezeTimer;

    public int DeadThreadCount => this.NumDeadThreads;

    public float CPUUsagePercent
    {
      get
      {
        this.CPUAverage.AddSample(this.cpuCounter.NextValue());
        return this.CPUAverage.Average;
      }
    }

    public float RAMUsagePercent => (float) (100.0 * (((double) this.RamTotalMB - (double) this.RamAvailableMB) / (double) this.RamTotalMB));

    public string AssemblyString => this.AssemblyVersion;

    public static string AssemblyName => Assembly.GetExecutingAssembly().GetName().Version.ToString();

    private float RamAvailableMB => (float) ((double) this.ramLeftCounter.NextValue() / 1024.0 / 1024.0);

    public PWServerApp(string[] Args, PWServerBase.AddLogMessageCallback InCallback)
    {
      this.StartArgs = Args;
      this.UILogCallback = InCallback;
      AppDomain.CurrentDomain.UnhandledException += (UnhandledExceptionEventHandler) ((sender, e) => this.HandleException((Exception) e.ExceptionObject));
      Application.ThreadException += (ThreadExceptionEventHandler) ((sender, e) => this.HandleException(e.Exception));
      this.NumDeadThreads = 0;
      this.eventLog = new EventLog("Application");
      this.eventLog.Source = Assembly.GetEntryAssembly().GetName().Name;
      this.cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total", true);
      this.ramLeftCounter = new PerformanceCounter("Memory", "Available Bytes", true);
      this.CPUAverage = new WeightedAverage(10);
      this.MailQueue = new ConcurrentDictionary<ELoggingLevel, PWServerApp.MailQueueItem>();
      this.MailQueue[ELoggingLevel.ELL_Errors] = new PWServerApp.MailQueueItem(ELoggingLevel.ELL_Errors);
      this.MailQueue[ELoggingLevel.ELL_Critical] = new PWServerApp.MailQueueItem(ELoggingLevel.ELL_Critical);
      this.MailQueue[ELoggingLevel.ELL_Warnings] = new PWServerApp.MailQueueItem(ELoggingLevel.ELL_Warnings);
      ServicePointManager.DefaultConnectionLimit = 100;
      PWServerApp.MEMORYSTATUSEX lpBuffer = new PWServerApp.MEMORYSTATUSEX();
      PWServerApp.GlobalMemoryStatusEx(lpBuffer);
      this.ServerMap = new Dictionary<string, PWServerBase>();
      this.RamTotalMB = (float) ((double) lpBuffer.ullTotalPhys / 1024.0 / 1024.0);
      this.CPUUsageFreezeTimer = new System.Timers.Timer();
      this.CPUUsageFreezeTimer.Elapsed += new ElapsedEventHandler(this.OnCPUWarningFreezeExpired);
      this.CPUUsageFreezeTimer.Enabled = false;
      this.CPUUsageFreezeTimer.AutoReset = false;
      this.MailThread = new Thread(new ThreadStart(this.ProcessMail));
      this.AssemblyVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
    }

    public virtual void Start()
    {
      foreach (string startArg in this.StartArgs)
      {
        if (startArg.Contains("-cfg="))
        {
          string[] strArray = startArg.Split('=');
          if (strArray.Length == 2)
            this.LoadConfigFile(strArray[1]);
        }
      }
      this.MailThread.Start();
      this.HealthMonitorTimer = new System.Timers.Timer(15000.0);
      this.HealthMonitorTimer.Elapsed += new ElapsedEventHandler(this.OnCheckHealth);
      this.HealthMonitorTimer.Start();
    }

    public virtual void Stop()
    {
      this.MailThread.Abort();
      this.HealthMonitorTimer.Stop();
    }

    public void FreezeCPUWarning(float ForSeconds)
    {
      if ((double) ForSeconds * 1000.0 <= this.CPUUsageFreezeTimer.Interval)
        return;
      this.AddLogMessage("[System] Freezing CPU Usage Warnings for " + (object) ForSeconds + "s", ELoggingLevel.ELL_Informative);
      this.CPUUsageFreezeTimer.Interval = (double) ForSeconds * 1000.0;
      this.CPUUsageFreezeTimer.Start();
    }

    private void OnCPUWarningFreezeExpired(object sender, ElapsedEventArgs e) => this.AddLogMessage("[System] Un-Freezing CPU Usage Warnings", ELoggingLevel.ELL_Informative);

    public bool HasStartupFlag(string InMatch)
    {
      foreach (string startArg in this.StartArgs)
      {
        if (startArg.Contains(InMatch))
          return true;
      }
      return false;
    }

    public void AddServer(string ServerName, PWServerBase InServer) => this.ServerMap[ServerName] = InServer;

    private void OnCheckHealth(object Source, ElapsedEventArgs args)
    {
      float OutValue1;
      this.GetConfigValue<float>("CPUUsageWarnPct", out OutValue1, 95f);
      float OutValue2;
      this.GetConfigValue<float>("RAMUsageWarnPct", out OutValue2, 95f);
      if ((double) this.CPUUsagePercent > (double) OutValue1 && !this.CPUUsageFreezeTimer.Enabled)
        this.AddLogMessage("[System] CPU Usage above limit [" + (object) this.CPUUsagePercent + "%]", ELoggingLevel.ELL_Errors);
      if ((double) this.RAMUsagePercent <= (double) OutValue2)
        return;
      this.AddLogMessage("[System] RAM Usage above limit [" + (object) this.RAMUsagePercent + "%]", ELoggingLevel.ELL_Errors);
    }

    public override string ToString() => "" + " [PID:" + (object) Process.GetCurrentProcess().Id + "]" + " [CPU:" + this.CPUUsagePercent.ToString("000") + "%]" + " [RAM:" + this.RAMUsagePercent.ToString("000") + "%]" + " [PLAT:x86]" + " [CONF:RELEASE]" + " [ASM:" + this.AssemblyVersion + "]";

    private void ProcessMail()
    {
      Thread.CurrentThread.Name = "Mail Thread";
      while (true)
      {
        int OutValue1;
        this.GetConfigValue<int>("MailIntervalSeconds", out OutValue1, 120);
        TimeSpan timeSpan = new TimeSpan(0, 0, OutValue1);
        int OutValue2;
        this.GetConfigValue<int>("EarlyMailThresholdwarnings", out OutValue2);
        int OutValue3;
        this.GetConfigValue<int>("EarlyMailThresholdErrors", out OutValue3);
        int OutValue4;
        this.GetConfigValue<int>("EarlyMailThresholdCritical", out OutValue4);
        Dictionary<ELoggingLevel, int> dictionary = new Dictionary<ELoggingLevel, int>();
        dictionary[ELoggingLevel.ELL_Warnings] = OutValue2;
        dictionary[ELoggingLevel.ELL_Errors] = OutValue3;
        dictionary[ELoggingLevel.ELL_Critical] = OutValue4;
        bool OutValue5 = false;
        this.GetConfigValue<bool>("MailEnabled", out OutValue5, true);
        try
        {
          foreach (KeyValuePair<ELoggingLevel, PWServerApp.MailQueueItem> mail in this.MailQueue)
          {
            int num = dictionary[mail.Key];
            DateTime dateTime = mail.Value.LastSendTime + timeSpan;
            if (mail.Value.Items.Count > 0 && (mail.Value.Items.Count > num || PWServerBase.CurrentTime > dateTime))
            {
              List<string> MailItems = new List<string>(mail.Value.Items.Count);
              string result;
              while (mail.Value.Items.TryDequeue(out result))
                MailItems.Add(result);
              if (OutValue5)
                this.SendMailList(mail.Key, MailItems);
              mail.Value.LastSendTime = PWServerBase.CurrentTime;
            }
          }
        }
        catch (Exception ex)
        {
          this.AddLogMessage("[System] " + this.ToString() + " SendMail Error: " + ex.ToString(), ELoggingLevel.ELL_Warnings);
        }
        Thread.Sleep(15000);
      }
    }

    private bool SendMailList(ELoggingLevel InLogLevel, List<string> MailItems)
    {
      this.AddLogMessage("[System] Sending " + (object) MailItems.Count + " Mail Events", ELoggingLevel.ELL_Informative);
      string OutValue1;
      if (!this.GetConfigValue<string>("MailFrom", out OutValue1))
      {
        this.AddLogMessage("[System] Cannot Send Mail, Missing MailFrom config value", ELoggingLevel.ELL_Warnings);
        return false;
      }
      string OutValue2;
      if (!this.GetConfigValue<string>("MailAddr", out OutValue2))
      {
        this.AddLogMessage("[System] Cannot Send Mail, Missing MailAddr config value", ELoggingLevel.ELL_Warnings);
        return false;
      }
      int OutValue3;
      if (!this.GetConfigValue<int>("MailPort", out OutValue3))
      {
        this.AddLogMessage("[System] Cannot Send Mail, Missing MailPort config value", ELoggingLevel.ELL_Warnings);
        return false;
      }
      string OutValue4;
      if (!this.GetConfigValue<string>("MailTo", out OutValue4))
      {
        this.AddLogMessage("[System] Cannot Send Mail, Missing MailTo config value", ELoggingLevel.ELL_Warnings);
        return false;
      }
      string OutValue5;
      if (!this.GetConfigValue<string>("MailSubject", out OutValue5))
      {
        this.AddLogMessage("[System] Cannot Send Mail, Missing MailSubject config value", ELoggingLevel.ELL_Warnings);
        return false;
      }
      Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>();
      foreach (string mailItem in MailItems)
      {
        string key = mailItem;
        int num1 = mailItem.IndexOf("  at ");
        if (num1 > 0)
        {
          int num2 = -1;
          int num3 = -1;
          do
          {
            num3 = mailItem.IndexOf("]", num3 + 1);
            if (num3 > 0 && num3 < num1)
              num2 = num3;
          }
          while (num3 > 0 && num3 < num1);
          if (num2 > 0 && num2 < num1)
            key = mailItem.Substring(num2 + 1);
        }
        if (!dictionary.ContainsKey(key))
          dictionary[key] = new List<string>();
        dictionary[key].Add(mailItem);
      }
      string str1 = "" + "Assembly Version: " + this.AssemblyVersion + Environment.NewLine + "Number of Total Events: " + MailItems.Count.ToString() + Environment.NewLine + "Number of Unique Events: " + dictionary.Count.ToString() + Environment.NewLine;
      string InString = "";
      for (int index = 0; index < MailItems.Count; ++index)
        InString = InString + "Event " + (object) index + ": " + Environment.NewLine + MailItems[index] + Environment.NewLine;
      bool OutValue6 = false;
      this.GetConfigValue<bool>("MailSendAsAttachment", out OutValue6, true);
      MailMessage message = new MailMessage();
      if (OutValue6)
      {
        string str2 = "Events_" + PWServerBase.CurrentTime.ToString("yyMMddmmss");
        bool OutValue7 = false;
        this.GetConfigValue<bool>("MailCompress", out OutValue7, true);
        Attachment attachment;
        if (OutValue7)
        {
          MemoryStream memoryStream1 = new MemoryStream();
          GZipStream gzipStream = new GZipStream((Stream) memoryStream1, CompressionMode.Compress);
          byte[] buffer = new MessageSerializer().SerializeRaw(InString);
          gzipStream.Write(buffer, 0, InString.Length);
          gzipStream.Close();
          byte[] array = memoryStream1.ToArray();
          MemoryStream memoryStream2 = new MemoryStream(array);
          string name = str2 + ".zip";
          attachment = new Attachment((Stream) memoryStream2, name, "application/zip");
          attachment.ContentDisposition.FileName = name;
          int int32 = Convert.ToInt32((float) ((double) array.Length / (double) buffer.Length * 100.0));
          str1 = str1 + "Attachment compressed to " + (object) int32 + "%" + Environment.NewLine + Environment.NewLine;
        }
        else
        {
          MemoryStream memoryStream = new MemoryStream(new MessageSerializer().SerializeRaw(InString));
          string name = str2 + ".txt";
          attachment = new Attachment((Stream) memoryStream, name, "text/plain");
          attachment.ContentDisposition.FileName = name;
        }
        message.Attachments.Add(attachment);
        bool OutValue8 = false;
        this.GetConfigValue<bool>("MailListUniques", out OutValue8, true);
        if (OutValue8)
        {
          str1 = str1 + "Unique Event Listing:" + Environment.NewLine;
          int num = 0;
          foreach (KeyValuePair<string, List<string>> keyValuePair in dictionary)
            str1 = str1 + "Unique Event " + (object) num++ + ": Count: " + (object) keyValuePair.Value.Count + Environment.NewLine + keyValuePair.Value[0] + Environment.NewLine + Environment.NewLine;
        }
      }
      else
        str1 += InString;
      message.Body = str1;
      message.From = new MailAddress(OutValue1);
      message.To.Add(OutValue4);
      message.Subject = InLogLevel.GetDescription() + " " + OutValue5;
      message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess | DeliveryNotificationOptions.OnFailure | DeliveryNotificationOptions.Delay;
      if (this.MailClient == null)
      {
        this.MailClient = new SmtpClient(OutValue2, OutValue3);
        this.MailClient.SendCompleted += new SendCompletedEventHandler(this.SendCompletedCallback);
        this.MailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
      }
      this.MailClient.Send(message);
      this.AddLogMessage("[System] Mail Async Task Started", ELoggingLevel.ELL_Informative);
      return true;
    }

    public virtual List<HttpData> GetWebData() => new List<HttpData>();

    public static bool GetConfigValue<T>(
      string KeyName,
      out T OutValue,
      PWServerBase.AddLogMessageCallback LogCallback)
    {
      OutValue = default (T);
      try
      {
        string OutValue1 = "";
        ConfigSettings.GetSetting(KeyName, ref OutValue1);
        if (OutValue1 == null)
        {
          if (LogCallback != null)
            LogCallback("Could not find Configuration Variable [" + KeyName + "]", ELoggingLevel.ELL_Errors);
          return false;
        }
        TypeConverter converter = TypeDescriptor.GetConverter(typeof (T));
        if (converter != null)
        {
          OutValue = (T) converter.ConvertFromString(OutValue1);
          return true;
        }
        if (LogCallback != null)
          LogCallback("Could not find TypeConverter for type" + OutValue.GetType().ToString(), ELoggingLevel.ELL_Errors);
      }
      catch (Exception ex)
      {
        if (LogCallback != null)
          LogCallback("Exception when trying to load Configuration Variable [" + KeyName + "] Exception: " + ex.ToString(), ELoggingLevel.ELL_Errors);
      }
      return false;
    }

    public static bool GetConfigValue<T>(
      string KeyName,
      out T OutValue,
      T DefaultValue,
      PWServerBase.AddLogMessageCallback LogCallback)
    {
      if (PWServerApp.GetConfigValue<T>(KeyName, out OutValue, LogCallback))
        return true;
      OutValue = DefaultValue;
      return false;
    }

    public bool GetConfigValue<T>(string KeyName, out T OutValue)
    {
      OutValue = default (T);
      return PWServerApp.GetConfigValue<T>(KeyName, out OutValue, this.UILogCallback);
    }

    public bool GetConfigValue<T>(string KeyName, out T OutValue, T DefaultValue)
    {
      if (PWServerApp.GetConfigValue<T>(KeyName, out OutValue, this.UILogCallback))
        return true;
      OutValue = DefaultValue;
      return false;
    }

    public static string StackTrace(int framesToSkip) => Environment.NewLine + "Stack Trace:" + new System.Diagnostics.StackTrace().ToString() + Environment.NewLine;

    public static string StackTrace() => PWServerApp.StackTrace(1);

    public void AddLogMessage(string InLogText, ELoggingLevel InLogLevel)
    {
      Trace.WriteLine(InLogText);
      try
      {
        this.eventLog.WriteEntry(InLogText, PWServerBase.GetEventLogType(InLogLevel));
      }
      catch (SecurityException ex)
      {
      }
      if (InLogLevel == ELoggingLevel.ELL_Errors || InLogLevel == ELoggingLevel.ELL_Critical)
      {
        bool OutValue = false;
        this.GetConfigValue<bool>("MailStackTraces", out OutValue);
        if (OutValue)
          InLogText += PWServerApp.StackTrace(2);
        this.SendMail(InLogText, InLogLevel);
      }
      if (this.UILogCallback == null)
        return;
      this.UILogCallback(InLogText, InLogLevel);
    }

    public void HandleException(Exception ex)
    {
      this.AddLogMessage("[System] Unhandled Exception, Freezing Dead Thread: " + ex.ToString(), ELoggingLevel.ELL_Critical);
      Interlocked.Increment(ref this.NumDeadThreads);
      Thread.CurrentThread.IsBackground = true;
      Thread.CurrentThread.Priority = ThreadPriority.Lowest;
      Thread.CurrentThread.Name = "Dead thread: " + ex.ToString();
      while (true)
        Thread.Sleep(TimeSpan.FromHours(1.0));
    }

    protected void SendMail(string MailString, ELoggingLevel InLogLevel) => this.MailQueue[InLogLevel].Items.Enqueue(MailString);

    private void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
    {
      if (e.Error != null)
        this.AddLogMessage("[System] Mail Send Failed: " + e.Error.ToString(), ELoggingLevel.ELL_Warnings);
      else if (e.Cancelled)
        this.AddLogMessage("[System] Mail Send Cancelled: ", ELoggingLevel.ELL_Warnings);
      else
        this.AddLogMessage("[System] Mail Send Succeeded: ", ELoggingLevel.ELL_Informative);
    }

    public void LoadConfigFile(string PathName)
    {
      this.AddLogMessage("[System] Loading User-specified config file: " + PathName, ELoggingLevel.ELL_Informative);
      ConfigSettings.Load(PathName);
    }

    [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool GlobalMemoryStatusEx([In, Out] PWServerApp.MEMORYSTATUSEX lpBuffer);

    private class MailQueueItem
    {
      public ELoggingLevel LoggingLevel;
      public DateTime LastSendTime;
      public ConcurrentQueue<string> Items;

      public MailQueueItem(ELoggingLevel InLogLevel)
      {
        this.LoggingLevel = InLogLevel;
        this.Items = new ConcurrentQueue<string>();
        this.LastSendTime = PWServerBase.CurrentTime;
      }
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public class MEMORYSTATUSEX
    {
      public uint dwLength;
      public uint dwMemoryLoad;
      public ulong ullTotalPhys;
      public ulong ullAvailPhys;
      public ulong ullTotalPageFile;
      public ulong ullAvailPageFile;
      public ulong ullTotalVirtual;
      public ulong ullAvailVirtual;
      public ulong ullAvailExtendedVirtual;

      public MEMORYSTATUSEX() => this.dwLength = (uint) Marshal.SizeOf(typeof (PWServerApp.MEMORYSTATUSEX));
    }
  }
}
