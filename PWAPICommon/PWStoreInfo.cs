﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PWStoreInfo
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Common;
using System;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class PWStoreInfo
  {
    [XmlAttribute("RT")]
    public DateTime NextReadTime;
    [XmlElement("ST")]
    public PWStoreTagItem[] StoreTags;
    [XmlElement("SO")]
    public PWStoreOfferItem[] OfferItems;
    [XmlElement("SI")]
    public StoreItem[] StoreItems;
  }
}
