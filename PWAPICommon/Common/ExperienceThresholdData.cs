﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.ExperienceThresholdData
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class ExperienceThresholdData : OfferDataBase<ExperienceThresholdData>
  {
    public int Level;
    public int XPReq;
    public string MailSubject;
    public string MailBody;

    [XmlElement("Duration")]
    [DefaultValue(5)]
    public byte DurationByte
    {
      get => (byte) this.Duration;
      set => this.Duration = (EPurchaseLength) value;
    }

    public override bool Matches(ExperienceThresholdData Other) => Other != null && this.Level == Other.Level;
  }
}
