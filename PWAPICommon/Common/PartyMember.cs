﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PartyMember
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PartyMember : PWItemBase
  {
    [XmlAttribute("OW")]
    public long OwnerID;
    [XmlAttribute("FID")]
    public long MemberID;
    [XmlAttribute("FS")]
    public byte State;
    [XmlElement("PI")]
    public PWPlayerInfo PlayerInfo;
    [XmlIgnore]
    public SocialLoginData LoginInfo;

    public PartyMember() => this.PlayerInfo = new PWPlayerInfo();

    public override bool ParseFromArray(object[] InArray) => InArray.Length == 5 && this.ValidatedAssign(ref this.OwnerID, InArray[0]) && (this.ValidatedAssign(ref this.MemberID, InArray[1]) && this.ValidatedAssign<byte>(ref this.State, InArray[2]));
  }
}
