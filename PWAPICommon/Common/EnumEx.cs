﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.EnumEx
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public static class EnumEx
  {
    public static T GetValueFromDescription<T>(string description)
    {
      Type type = typeof (T);
      if (!type.IsEnum)
        throw new InvalidOperationException();
      foreach (FieldInfo field in type.GetFields())
      {
        if (Attribute.GetCustomAttribute((MemberInfo) field, typeof (DescriptionAttribute)) is DescriptionAttribute customAttribute)
        {
          if (customAttribute.Description == description)
            return (T) field.GetValue((object) null);
        }
        else if (field.Name == description)
          return (T) field.GetValue((object) null);
      }
      throw new ArgumentException("Not found.", nameof (description));
    }

    public static string GetDescription(this Enum value) => Attribute.GetCustomAttribute((MemberInfo) value.GetType().GetField(value.ToString()), typeof (DescriptionAttribute)) is DescriptionAttribute customAttribute ? customAttribute.Description : value.ToString();
  }
}
