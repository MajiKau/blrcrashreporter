﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.MessageSerializer
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using ComponentAce.Compression.Libs.zlib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class MessageSerializer
  {
    private Encoding CharacterEncoding;
    private uint CompressSize;

    public MessageSerializer()
      : this(Encoding.UTF8)
    {
    }

    public MessageSerializer(Encoding MessageEncoding)
      : this(MessageEncoding, 100U)
    {
    }

    public MessageSerializer(Encoding MessageEncoding, uint MessageCompressSize)
    {
      this.CharacterEncoding = MessageEncoding;
      this.CompressSize = MessageCompressSize;
    }

    public byte[] Serialize(string InPreamble) => this.CharacterEncoding.GetBytes(InPreamble.ToCharArray());

    public byte[] Serialize(string InPreamble, byte[] Data)
    {
      byte[] numArray1 = this.Serialize(InPreamble != null ? InPreamble + ":" : "");
      byte[] numArray2 = new byte[numArray1.Length + Data.Length];
      Array.Copy((Array) numArray1, (Array) numArray2, numArray1.Length);
      Array.Copy((Array) Data, 0, (Array) numArray2, numArray1.Length, Data.Length);
      return numArray2;
    }

    public byte[] SerializeObject<T>(T Data) => this.CharacterEncoding.GetBytes(this.SerializeObjectToString<T>(Data).ToCharArray());

    public byte[] SerializeObject<T>(T Data, Type[] ExtraTypes) => this.CharacterEncoding.GetBytes(this.SerializeObjectToString<T>(Data, ExtraTypes).ToCharArray());

    public string SerializeObjectToString<T>(T Data)
    {
      Type[] ExtraTypes = (Type[]) null;
      return this.SerializeObjectToString<T>(Data, ExtraTypes);
    }

    public string SerializeObjectToString<T>(T Data, Type[] ExtraTypes)
    {
      MemoryStream memoryStream = new MemoryStream();
      XmlWriter.Create((Stream) memoryStream);
      (ExtraTypes == null ? XmlSerializerCache.Create(typeof (T)) : XmlSerializerCache.Create(typeof (T), ExtraTypes)).Serialize((Stream) memoryStream, (object) Data);
      return this.CharacterEncoding.GetString(memoryStream.ToArray());
    }

    public T DeSerializeStringToObject<T>(string Data)
    {
      Type[] ExtraTypes = (Type[]) null;
      return this.DeSerializeStringToObject<T>(Data, ExtraTypes);
    }

    public T DeSerializeStringToObject<T>(string Data, Type[] ExtraTypes)
    {
      XmlReader xmlReader = XmlReader.Create((Stream) new MemoryStream(this.SerializeRaw(Data ?? "")));
      return (T) (ExtraTypes == null ? XmlSerializerCache.Create(typeof (T)) : XmlSerializerCache.Create(typeof (T), ExtraTypes)).Deserialize(xmlReader);
    }

    public string SerializeObjectToString<T>(T Data, XmlTinyAttribute[] IgnoreAttributes)
    {
      if (IgnoreAttributes == null || IgnoreAttributes.Length <= 0)
        return this.SerializeObjectToString<T>(Data);
      MemoryStream memoryStream = new MemoryStream();
      XmlWriter.Create((Stream) memoryStream);
      XmlSerializerCache.Create(typeof (T), IgnoreAttributes).Serialize((Stream) memoryStream, (object) Data);
      return this.CharacterEncoding.GetString(memoryStream.ToArray());
    }

    public byte[] Serialize<T>(string InPreamble, T Data) => this.CharacterEncoding.GetBytes(((InPreamble != null ? InPreamble + ":" : "") + this.SerializeObjectToString<T>(Data)).ToCharArray());

    public byte[] Serialize<T>(string InPreamble, T Data, Type[] ExtraTypes) => this.CharacterEncoding.GetBytes(((InPreamble != null ? InPreamble + ":" : "") + this.SerializeObjectToString<T>(Data, ExtraTypes)).ToCharArray());

    public byte[] SerializeRaw(string InString) => this.CharacterEncoding.GetBytes(InString.ToCharArray());

    public byte[] Serialize<T>(string InPreamble, T Data, XmlTinyAttribute[] IgnoreAttributes) => this.CharacterEncoding.GetBytes(((InPreamble != null ? InPreamble + ":" : "") + this.SerializeObjectToString<T>(Data, IgnoreAttributes)).ToCharArray());

    public string[] Deserialize(byte[] BinaryData)
    {
      byte[] numArray = BinaryData ?? new byte[0];
      if (numArray.Length > 6 && numArray[0] == (byte) 90 && numArray[1] == (byte) 88)
        numArray = this.Decompress(numArray);
      string str = this.CharacterEncoding.GetString(numArray, 0, BinaryData.Length);
      int length = 0;
      while (length < str.Length)
        ++length;
      return str.Substring(0, length).Split(':');
    }

    public string DeserializeToString(byte[] BinaryData)
    {
      byte[] bytes = BinaryData ?? new byte[0];
      string str = this.CharacterEncoding.GetString(bytes, 0, bytes.Length);
      int length = 0;
      while (length < str.Length)
        ++length;
      return str.Substring(0, length);
    }

    public string[] DeserializeToStringArray(byte[] InData) => this.CharacterEncoding.GetString(InData ?? new byte[0]).Split(':');

    public object[] Deserialize(byte[] BinaryData, Type XmlDataType, Type[] ExtraTypes)
    {
      byte[] numArray = BinaryData ?? new byte[0];
      if (numArray.Length > 6 && numArray[0] == (byte) 90 && numArray[1] == (byte) 88)
        numArray = this.Decompress(numArray);
      string str = this.CharacterEncoding.GetString(numArray);
      int num = 0;
      while (num < str.Length && str[num] != '<')
        ++num;
      object[] array = ((IEnumerable<object>) str.Substring(0, num).Split(':')).ToArray<object>();
      if (array.Length > 0 && num < str.Length && str[num] == '<')
      {
        XmlTextReader xmlTextReader = new XmlTextReader((TextReader) new StringReader(str.Substring(num)));
        XmlSerializer xmlSerializer = ExtraTypes == null ? XmlSerializerCache.Create(XmlDataType) : XmlSerializerCache.Create(XmlDataType, ExtraTypes);
        try
        {
          array[array.Length - 1] = !xmlSerializer.CanDeserialize((XmlReader) xmlTextReader) ? (object) null : xmlSerializer.Deserialize((XmlReader) xmlTextReader);
        }
        catch (Exception ex)
        {
          array[array.Length - 1] = (object) null;
        }
      }
      return array ?? new object[0];
    }

    public object[] Deserialize(byte[] BinaryData, Type XmlDataType)
    {
      Type[] ExtraTypes = (Type[]) null;
      return this.Deserialize(BinaryData, XmlDataType, ExtraTypes);
    }

    public byte[] Compress(byte[] InData)
    {
      if (InData == null || (long) InData.Length <= (long) this.CompressSize)
        return InData;
      MemoryStream memoryStream = new MemoryStream();
      ZOutputStream zoutputStream = new ZOutputStream((Stream) memoryStream, -1);
      zoutputStream.Write(InData, 0, InData.Length);
      zoutputStream.Flush();
      zoutputStream.Close();
      int position = (int) memoryStream.Position;
      if (position == 0 || position >= InData.Length)
        return InData;
      byte[] buffer = new byte[position + 6];
      buffer[0] = (byte) 90;
      buffer[1] = (byte) 88;
      Array.Copy((Array) BitConverter.GetBytes(InData.Length), 0, (Array) buffer, 2, 4);
      memoryStream.Position = 0L;
      memoryStream.Read(buffer, 6, position);
      memoryStream.Close();
      return buffer;
    }

    public byte[] Decompress(byte[] CompressedData)
    {
      byte[] buffer = (byte[]) null;
      if (CompressedData != null && CompressedData.Length >= 6)
      {
        int int32 = BitConverter.ToInt32(CompressedData, 2);
        if (int32 >= 0)
        {
          buffer = new byte[int32];
          MemoryStream memoryStream = new MemoryStream(buffer);
          ZOutputStream zoutputStream = new ZOutputStream((Stream) memoryStream);
          try
          {
            zoutputStream.Write(CompressedData, 6, CompressedData.Length - 6);
          }
          catch (ZStreamException ex)
          {
            buffer = (byte[]) null;
          }
          zoutputStream.Close();
          memoryStream.Close();
        }
      }
      return buffer;
    }

    public static void Encode(EMessageType MessageType, byte[] BinaryData, out byte[] FinalBlob)
    {
      if (BinaryData == null || BinaryData.Length == 0)
      {
        BinaryData = new byte[1];
        BinaryData[0] = (byte) 1;
      }
      byte num1 = XXTEA.DoEncrypt(ref BinaryData);
      byte[] InOutData = new byte[BinaryData.Length + 4];
      Array.Copy((Array) BinaryData, 0, (Array) InOutData, 4, BinaryData.Length);
      InOutData[0] = (byte) 60;
      InOutData[1] = num1;
      InOutData[2] = (byte) MessageType;
      InOutData[3] = (byte) 62;
      int num2 = (int) XXTEA.DoEncrypt(ref InOutData);
      FinalBlob = new byte[InOutData.Length + 4];
      Array.Copy((Array) InOutData, 0, (Array) FinalBlob, 2, InOutData.Length);
      FinalBlob[0] = (byte) 60;
      FinalBlob[1] = (byte) 60;
      FinalBlob[FinalBlob.Length - 1] = (byte) 62;
      FinalBlob[FinalBlob.Length - 2] = (byte) 62;
    }

    public string HashStringMD5(string InString)
    {
      byte[] hash = MD5.Create().ComputeHash(this.CharacterEncoding.GetBytes(InString));
      StringBuilder stringBuilder = new StringBuilder();
      for (int index = 0; index < hash.Length; ++index)
        stringBuilder.Append(hash[index].ToString("X2"));
      return stringBuilder.ToString();
    }
  }
}
