﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWBiDictionary`2
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Collections.Generic;

namespace PWAPICommon.Common
{
  internal class PWBiDictionary<TFirst, TSecond>
  {
    private IDictionary<TFirst, IList<TSecond>> firstToSecond = (IDictionary<TFirst, IList<TSecond>>) new Dictionary<TFirst, IList<TSecond>>();
    private IDictionary<TSecond, IList<TFirst>> secondToFirst = (IDictionary<TSecond, IList<TFirst>>) new Dictionary<TSecond, IList<TFirst>>();
    private static IList<TFirst> EmptyFirstList = (IList<TFirst>) new TFirst[0];
    private static IList<TSecond> EmptySecondList = (IList<TSecond>) new TSecond[0];

    public int Count => this.firstToSecond.Count;

    public void Add(TFirst first, TSecond second)
    {
      IList<TSecond> secondList;
      if (!this.firstToSecond.TryGetValue(first, out secondList))
      {
        secondList = (IList<TSecond>) new List<TSecond>();
        this.firstToSecond[first] = secondList;
      }
      IList<TFirst> firstList;
      if (!this.secondToFirst.TryGetValue(second, out firstList))
      {
        firstList = (IList<TFirst>) new List<TFirst>();
        this.secondToFirst[second] = firstList;
      }
      secondList.Add(second);
      firstList.Add(first);
    }

    public IList<TSecond> this[TFirst first] => this.GetByFirst(first);

    public IList<TFirst> this[TSecond second] => this.GetBySecond(second);

    public IList<TSecond> GetByFirst(TFirst first)
    {
      IList<TSecond> secondList;
      return !this.firstToSecond.TryGetValue(first, out secondList) ? PWBiDictionary<TFirst, TSecond>.EmptySecondList : (IList<TSecond>) new List<TSecond>((IEnumerable<TSecond>) secondList);
    }

    public IList<TFirst> GetBySecond(TSecond second)
    {
      IList<TFirst> firstList;
      return !this.secondToFirst.TryGetValue(second, out firstList) ? PWBiDictionary<TFirst, TSecond>.EmptyFirstList : (IList<TFirst>) new List<TFirst>((IEnumerable<TFirst>) firstList);
    }
  }
}
