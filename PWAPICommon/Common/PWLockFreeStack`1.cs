﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWLockFreeStack`1
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;
using System.Threading;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PWLockFreeStack<T>
  {
    private PWSingleLinkNode<T> head;
    private int PoolSize;

    public int NumElements => this.PoolSize;

    public PWLockFreeStack() => this.head = new PWSingleLinkNode<T>();

    public bool Contains(T item)
    {
      for (PWSingleLinkNode<T> pwSingleLinkNode = this.head; pwSingleLinkNode != null; pwSingleLinkNode = pwSingleLinkNode.Next)
      {
        if ((object) pwSingleLinkNode.Item != null && pwSingleLinkNode.Item.Equals((object) item))
          return true;
      }
      return false;
    }

    public void Push(T item)
    {
      PWSingleLinkNode<T> newValue = new PWSingleLinkNode<T>();
      newValue.Item = item;
      do
      {
        newValue.Next = this.head.Next;
      }
      while (!SyncMethods.CAS<PWSingleLinkNode<T>>(ref this.head.Next, newValue.Next, newValue));
      Interlocked.Increment(ref this.PoolSize);
    }

    public bool Pop(out T item)
    {
      PWSingleLinkNode<T> next;
      do
      {
        next = this.head.Next;
        if (next == null)
        {
          item = default (T);
          return false;
        }
      }
      while (!SyncMethods.CAS<PWSingleLinkNode<T>>(ref this.head.Next, next, next.Next));
      item = next.Item;
      Interlocked.Decrement(ref this.PoolSize);
      return true;
    }

    public T Pop()
    {
      T obj;
      this.Pop(out obj);
      return obj;
    }
  }
}
