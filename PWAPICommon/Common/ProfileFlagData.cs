﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.ProfileFlagData
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Queries.Player;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class ProfileFlagData : OfferDataBase<ProfileFlagData>
  {
    public ProfileFlag ProfileTrigger;
    public bool bSendToMail;
    public MailOfferData MailData;

    [XmlElement("Duration")]
    [DefaultValue(EPurchaseLength.EPL_Perm)]
    public EPurchaseLength DurationEnum
    {
      get => this.Duration;
      set => this.Duration = value;
    }

    public override bool Matches(ProfileFlagData Other) => Other != null && this.ProfileTrigger == Other.ProfileTrigger;
  }
}
