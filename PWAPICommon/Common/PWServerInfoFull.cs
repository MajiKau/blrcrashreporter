﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWServerInfoFull
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PWServerInfoFull
  {
    public PWServerInfoReq BaseInfo;
    public int GamePort;
    public string PublicIP;
    public bool bKeepAlive;
    public int GMPort;
    public int ServerID;
    public int GameRegion;
    public string PBHome;
    public bool bLogToFile;
    public string LogToFileDir;
    [XmlIgnore]
    public ConcurrentDictionary<string, string> ServerProperties;
    public List<string> StartingServerProperties;

    public EGameServerType GameServerType
    {
      get
      {
        if (this.bKeepAlive)
          return EGameServerType.EGST_Fixed;
        if (this.BaseInfo.bArbitrated)
          return EGameServerType.EGST_Ranked;
        return this.BaseInfo.HasPassword ? EGameServerType.EGST_Private : EGameServerType.EGST_Custom;
      }
    }

    public PWServerInfoFull()
    {
      this.BaseInfo = new PWServerInfoReq();
      this.GamePort = 7777;
      this.GMPort = 7777;
      this.PublicIP = "";
      this.GameRegion = -1;
      this.bKeepAlive = false;
      this.PBHome = "";
      this.bLogToFile = false;
      this.ServerProperties = new ConcurrentDictionary<string, string>();
      this.StartingServerProperties = new List<string>();
    }

    public string GetCreateString()
    {
      string str1 = " ";
      string str2 = (!this.BaseInfo.bArbitrated ? (!(this.BaseInfo.Playlist == "") ? str1 + "FoxEntry?Playlist=" + this.BaseInfo.Playlist : str1 + this.BaseInfo.MapName + "?Game=" + this.BaseInfo.GameType) : str1 + "FoxEntry?MapName=" + this.BaseInfo.MapName) + "?ServerName=\"" + this.BaseInfo.ServerName.Replace("?", "_") + "\"" + "?OwnerName=\"" + this.BaseInfo.OwnerName.Replace("?", "_") + "\"";
      if (this.BaseInfo.Password != "")
        str2 = str2 + "?GamePassword=\"" + this.BaseInfo.Password + "\"";
      string str3 = str2 + "?MaxPlayers=" + (object) this.BaseInfo.MaxPlayers + "?MaxSpectators=" + (object) this.BaseInfo.MaxSpectators + "?Port=" + (object) this.GamePort + "?remotedebugport=" + (object) this.GMPort + "?AdvertisedIP=" + this.PublicIP + "?RequestID=" + (object) this.BaseInfo.RequestId + "?Region=" + (object) this.GameRegion;
      if (this.bLogToFile)
      {
        str3 += "?LOGTOFILE=1";
        if (this.LogToFileDir != "")
          str3 = str3 + "?LOGTOFILEDIR=\"" + this.LogToFileDir + "\"";
      }
      if (this.BaseInfo.bArbitrated)
      {
        string str4 = str3 + "?Ranked" + "?SeasonID=" + this.BaseInfo.SeasonID.ToString() + "?MatchID=" + this.BaseInfo.MatchID.ToString() + "?A=";
        foreach (long arbitratedTeamAplayer in this.BaseInfo.ArbitratedTeamAPlayers)
          str4 = str4 + arbitratedTeamAplayer.ToString() + ",";
        if (str4.EndsWith(","))
          str4 = str4.Remove(str4.Length - 1);
        str3 = str4 + "?B=";
        foreach (long arbitratedTeamBplayer in this.BaseInfo.ArbitratedTeamBPlayers)
          str3 = str3 + arbitratedTeamBplayer.ToString() + ",";
        if (str3.EndsWith(","))
          str3 = str3.Remove(str3.Length - 1);
      }
      if (this.BaseInfo.bPrivate && !this.BaseInfo.bArbitrated)
        str3 = str3 + "?PrivateMatchOwner=" + (object) this.BaseInfo.CreatorId;
      if (this.BaseInfo.PlaylistBuildInfo.Length == 4 && this.BaseInfo.PlaylistBuildInfo[0] != 0)
        str3 = str3 + "?PlaylistInfo1=" + (object) this.BaseInfo.PlaylistBuildInfo[0] + "?PlaylistInfo2=" + (object) this.BaseInfo.PlaylistBuildInfo[1] + "?PlaylistInfo3=" + (object) this.BaseInfo.PlaylistBuildInfo[2] + "?PlaylistInfo4=" + (object) this.BaseInfo.PlaylistBuildInfo[3];
      if (this.StartingServerProperties != null)
      {
        foreach (string startingServerProperty in this.StartingServerProperties)
          str3 = str3 + "?" + startingServerProperty;
      }
      string str5;
      if (this.BaseInfo.bPrivate)
        str5 = str3 + " -Log=\"Private_" + (object) this.BaseInfo.CreatorId + "_" + PWServerBase.CurrentTime.ToString("YYMMddhhmm") + ".log\"";
      else
        str5 = str3 + " -Log=\"" + this.BaseInfo.ServerName.Replace(" ", "_") + ".log\"";
      if (this.PBHome != "")
        str5 = str5 + " PBHOMEPATH=\"" + this.PBHome + "\"";
      return str5;
    }

    public string GetConnectString() => this.PublicIP + ":" + this.BaseInfo.Password + ":" + (object) this.GamePort + ":" + this.BaseInfo.MaxPlayers.ToString();
  }
}
