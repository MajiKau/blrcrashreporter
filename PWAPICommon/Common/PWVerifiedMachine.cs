﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWVerifiedMachine
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PWVerifiedMachine : 
    PWSqlItemBase<PWVerifiedMachine>,
    IPWSqlItemKeyed<PWMachineID>,
    IPWSqlItemKeyed<long>
  {
    public Guid UniqueID;
    public long UserID;
    public PWMachineID MachineID;
    public string ChallengeCode;
    public bool bVerified;
    public DateTime ExpirationDate;

    public PWVerifiedMachine() => this.MachineID = new PWMachineID();

    public override bool ParseFromArray(object[] InArray) => InArray != null && InArray.Length == 6 && (this.ValidatedAssign(ref this.UniqueID, InArray[0]) && this.ValidatedAssign(ref this.UserID, InArray[1])) && (this.ValidatedAssign(ref this.MachineID, InArray[2]) && this.ValidatedAssign(ref this.ChallengeCode, InArray[3]) && (this.ValidatedAssign<bool>(ref this.bVerified, InArray[4]) && this.ValidatedAssign(ref this.ExpirationDate, InArray[5])));

    public static PWVerifiedMachine GenerateFor(PWMachineID InMachine, long InUser) => new PWVerifiedMachine()
    {
      UniqueID = Guid.NewGuid(),
      bVerified = false,
      ExpirationDate = PWServerBase.CurrentTime + new TimeSpan(24, 0, 0),
      ChallengeCode = NameGenerator.Generate(6),
      MachineID = InMachine,
      UserID = InUser
    };

    public override SqlCommand GetCommand(ESqlItemOp SqlOp, ref string LogOutput)
    {
      SqlCommand command;
      if (SqlOp == ESqlItemOp.ESIO_Add)
      {
        command = new SqlCommand("SP_VerifiedMachines_Add");
        command.CommandType = CommandType.StoredProcedure;
        command.AddParameter<byte[]>("@UniqueID", SqlDbType.VarBinary, this.UniqueID.ToByteArray());
        command.AddParameter<long>("@UserID", SqlDbType.BigInt, this.UserID);
        command.AddParameter<string>("@MachineID", SqlDbType.VarChar, this.MachineID.ToString());
        command.AddParameter<string>("@ChallengeCode", SqlDbType.VarChar, this.ChallengeCode);
        command.AddParameter<bool>("@Verified", SqlDbType.Bit, this.bVerified);
        command.AddParameter<DateTime>("@Expiration", SqlDbType.DateTime, this.ExpirationDate);
      }
      else
        command = base.GetCommand(SqlOp, ref LogOutput);
      return command;
    }

    public SqlCommand GetCommand(ESqlItemOp SqlOp, PWMachineID Key, ref string LogOutput)
    {
      SqlCommand command = (SqlCommand) null;
      if (SqlOp == ESqlItemOp.ESIO_QueryKeyed)
      {
        command = new SqlCommand("SP_VerifiedMachines_Select_By_MachineID");
        command.CommandType = CommandType.StoredProcedure;
        command.AddParameter<string>("@UserID", SqlDbType.BigInt, Key.ToString());
      }
      return command;
    }

    public SqlCommand GetCommand(ESqlItemOp SqlOp, long Key, ref string LogOutput)
    {
      SqlCommand command = (SqlCommand) null;
      if (SqlOp == ESqlItemOp.ESIO_QueryKeyed)
      {
        command = new SqlCommand("SP_VerifiedMachines_Select_By_UserID");
        command.CommandType = CommandType.StoredProcedure;
        command.AddParameter<long>("@UserID", SqlDbType.BigInt, Key);
      }
      return command;
    }
  }
}
