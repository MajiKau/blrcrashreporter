﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWSqlItemBase`1
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Wrappers;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public abstract class PWSqlItemBase<T> : PWItemBase, IPWSqlItem where T : PWSqlItemBase<T>, new()
  {
    public PWSqlItemBase()
    {
    }

    public PWSqlItemBase(object[] InValues)
      : base(InValues)
    {
    }

    public virtual SqlCommand GetCommand(ESqlItemOp SqlOp, ref string LogOutput) => (SqlCommand) null;

    public virtual EMessageType GetMessageType(ESqlItemOp SqlOp) => EMessageType.EMT_Invalid;

    public virtual bool ParseServerResult(PWSqlItemReqBase<T> SqlReq) => true;
  }
}
