﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.MasterServerInfo
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Queries.Matchmaking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class MasterServerInfo
  {
    private PWConnectionBase connection;
    private string externalIP;
    private string regionTag;
    private List<PWServerInfoRunning> RunningServers;
    private MasterServerStats Stats;
    private int serverID;
    private PWMachineID machineId;
    private ReaderWriterLockSlim ServerLock;

    public PWConnectionBase Connection
    {
      get => this.connection;
      set => this.connection = value;
    }

    public int CurrentPopulation => this.Stats.CurPopulation;

    public int MaxPopulation => this.Stats.MaxPopulation;

    public PWMachineID MachineID => this.machineId;

    public string RegionTag => this.regionTag;

    public int RankedSlotsAvailable => this.Stats.NumRankedMatchSlots - this.Stats.NumRankedMatches;

    public int PrivateSlotsAvailable => this.Stats.NumPrivateMatchSlots - this.Stats.NumPrivateMatches;

    public int PremiumSlotsAvailable => this.Stats.NumPremiumMatchSlots - this.Stats.NumPremiumMatches;

    public int GenericSlotsAvailable => this.Stats.NumGenericMatchSlots - this.Stats.NumGenericMatches;

    public int TotalSlotsAvailable => this.RankedSlotsAvailable + this.PrivateSlotsAvailable + this.PremiumSlotsAvailable + this.GenericSlotsAvailable;

    public int GetSlotsAvailableFor(EGameServerType InType)
    {
      switch (InType)
      {
        case EGameServerType.EGST_Custom:
          return 0;
        case EGameServerType.EGST_Fixed:
          return 0;
        case EGameServerType.EGST_Ranked:
          return this.RankedSlotsAvailable;
        case EGameServerType.EGST_Private:
          return this.PrivateSlotsAvailable;
        case EGameServerType.EGST_Premium:
          return this.PremiumSlotsAvailable;
        default:
          return 0;
      }
    }

    public int ServerID => this.serverID;

    public float Rating => (float) (0.0 + (this.Stats.MaxTemplates > this.Stats.CurTemplates ? (double) (this.Stats.MaxTemplates - this.Stats.CurTemplates) * 5.0 : -5000.0)) + (float) (10.0 - (double) this.Stats.CPULoad / 10.0) + (float) (10.0 - (double) this.Stats.RamLoad / 10.0);

    public int NumTotalServers => this.RunningServers.Count;

    public int NumRankedServers
    {
      get
      {
        this.ServerLock.EnterReadLock();
        int num = this.RunningServers.Count<PWServerInfoRunning>((Func<PWServerInfoRunning, bool>) (x => x.BaseInfo.BaseInfo.bArbitrated));
        this.ServerLock.ExitReadLock();
        return num;
      }
    }

    public int NumPrivateServers
    {
      get
      {
        this.ServerLock.EnterReadLock();
        int num = this.RunningServers.Count<PWServerInfoRunning>((Func<PWServerInfoRunning, bool>) (x => x.BaseInfo.BaseInfo.bPrivate));
        this.ServerLock.ExitReadLock();
        return num;
      }
    }

    public int NumPublicServers
    {
      get
      {
        this.ServerLock.EnterReadLock();
        int num = this.RunningServers.Count<PWServerInfoRunning>((Func<PWServerInfoRunning, bool>) (x => !x.BaseInfo.BaseInfo.bPrivate && !x.BaseInfo.BaseInfo.bArbitrated));
        this.ServerLock.ExitReadLock();
        return num;
      }
    }

    public IEnumerable<PWServerInfoRunning> PublicServers
    {
      get
      {
        this.ServerLock.EnterReadLock();
        IEnumerable<PWServerInfoRunning> serverInfoRunnings = this.RunningServers.Where<PWServerInfoRunning>((Func<PWServerInfoRunning, bool>) (x => !x.BaseInfo.BaseInfo.bArbitrated));
        this.ServerLock.ExitReadLock();
        return serverInfoRunnings;
      }
    }

    public MasterServerInfo(
      PWConnectionBase InConnection,
      PWMachineID ForMachine,
      int InMasterID,
      string InExternalIP,
      string InRegionTag)
    {
      this.externalIP = InExternalIP;
      this.regionTag = InRegionTag;
      this.connection = InConnection;
      this.machineId = ForMachine;
      this.RunningServers = new List<PWServerInfoRunning>();
      this.serverID = InMasterID;
      this.ServerLock = new ReaderWriterLockSlim();
    }

    public override string ToString() => "SID: " + (object) this.serverID + " CNT: " + this.RunningServers.Count.ToString() + " MID:" + this.MachineID.ToString() + " RAT:" + this.Rating.ToString();

    public PWServerInfoRunning FindServer(PWConnectionBase InConnection)
    {
      this.ServerLock.EnterReadLock();
      PWServerInfoRunning serverInfoRunning = this.RunningServers.Find((Predicate<PWServerInfoRunning>) (x => x.GameConnection == InConnection));
      this.ServerLock.ExitReadLock();
      return serverInfoRunning;
    }

    public void AddServer(PWServerInfoRunning NewServer)
    {
      this.ServerLock.EnterWriteLock();
      this.RunningServers.Add(NewServer);
      this.ServerLock.ExitWriteLock();
    }

    public bool RemoveServer(PWConnectionBase InConnection)
    {
      this.ServerLock.EnterWriteLock();
      bool flag = this.RunningServers.RemoveAll((Predicate<PWServerInfoRunning>) (x => x.GameConnection == InConnection)) > 0;
      this.ServerLock.ExitWriteLock();
      return flag;
    }

    public bool RemoveServer(PWServerInfoRunning InServer)
    {
      this.ServerLock.EnterWriteLock();
      bool flag = this.RunningServers.Remove(InServer);
      this.ServerLock.ExitWriteLock();
      return flag;
    }

    public bool RemoveServer(PWServerInfoFull InServerInfo)
    {
      this.ServerLock.EnterWriteLock();
      bool flag = this.RunningServers.RemoveAll((Predicate<PWServerInfoRunning>) (x => x.BaseInfo.ServerID == InServerInfo.ServerID)) > 0;
      this.ServerLock.ExitWriteLock();
      return flag;
    }

    public IEnumerable<string> PrintServers()
    {
      List<string> stringList = new List<string>(this.NumTotalServers);
      this.ServerLock.EnterReadLock();
      foreach (PWServerInfoRunning runningServer in this.RunningServers)
        stringList.Add(runningServer.ToString());
      this.ServerLock.ExitReadLock();
      return (IEnumerable<string>) stringList;
    }

    public void UpdateStats(MasterServerStats NewStats) => this.Stats = NewStats;
  }
}
