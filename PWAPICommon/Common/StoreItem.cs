﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.StoreItem
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Wrappers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Runtime.InteropServices;
using System.Xml;
using System.Xml.Serialization;

namespace PWAPICommon.Common
{
  [XmlRoot("StoreItem", Namespace = "")]
  [ComVisible(true)]
  public class StoreItem : PWSqlItemBase<StoreItem>, IPWSqlItemKeyed<int>, ICloneable
  {
    [XmlAttribute("IG")]
    public Guid ItemGuid;
    [XmlAttribute("UT")]
    public int UserType;
    [XmlAttribute("ID")]
    public int ItemId;
    [XmlAttribute("IT")]
    public string ItemTags;
    [XmlAttribute("FN")]
    public string FriendlyName;
    [XmlAttribute("IQ")]
    [DefaultValue(-1)]
    public int InitialQuantity;
    [DefaultValue(-1)]
    [XmlAttribute("QL")]
    public int QuantityLeft;
    [XmlAttribute("POD")]
    public DateTime PreOrderDate;
    [XmlAttribute("LD")]
    public DateTime LiveDate;
    [XmlAttribute("XD")]
    public DateTime ExpirationDate;
    [XmlAttribute("CD")]
    public DateTime CreationDate;
    [XmlIgnore]
    public DateTime ModifiedDate;
    [DefaultValue(-1)]
    [XmlAttribute("OGP")]
    public int OneDayPrice;
    [DefaultValue(-1)]
    [XmlAttribute("TGP")]
    public int ThreeDayPrice;
    [DefaultValue(-1)]
    [XmlAttribute("SGP")]
    public int SevenDayPrice;
    [DefaultValue(-1)]
    [XmlAttribute("TTGP")]
    public int ThirtyDayPrice;
    [XmlAttribute("DGP")]
    [DefaultValue(-1)]
    public int NinetyDayPrice;
    [DefaultValue(-1)]
    [XmlAttribute("PMP")]
    public int PermanentPrice;
    [DefaultValue(-1)]
    [XmlAttribute("OZP")]
    public int OneDayZenPrice;
    [XmlAttribute("TZP")]
    [DefaultValue(-1)]
    public int ThreeDayZenPrice;
    [XmlAttribute("SZP")]
    [DefaultValue(-1)]
    public int SevenDayZenPrice;
    [DefaultValue(-1)]
    [XmlAttribute("TTZP")]
    public int ThirtyDayZenPrice;
    [DefaultValue(-1)]
    [XmlAttribute("NZP")]
    public int NinetyDayZenPrice;
    [XmlAttribute("PZP")]
    [DefaultValue(-1)]
    public int PermanentZenPrice;
    [XmlAttribute("MO")]
    [DefaultValue(-1)]
    public int MaxOwned;
    [XmlAttribute("AT")]
    [DefaultValue(0)]
    public byte ActivationType;
    [DefaultValue("<Root />")]
    [XmlAttribute("AD")]
    public string ActivationData;
    [DefaultValue(false)]
    [XmlAttribute("HD")]
    public bool Hidden;
    [XmlAttribute("VL")]
    [DefaultValue(0)]
    public byte VisibilityLevel;
    [XmlAttribute("DC")]
    [DefaultValue(1f)]
    public double Discount;
    [DefaultValue(-1)]
    [XmlAttribute("SI")]
    public int SortIndex;

    [XmlIgnore]
    public bool ConsumeOnActivation
    {
      get
      {
        switch (this.ActivationType)
        {
          case 2:
          case 3:
          case 4:
            return true;
          default:
            return false;
        }
      }
    }

    public StoreItem(object[] InArray)
      : base(InArray)
    {
    }

    public StoreItem()
    {
      this.PreOrderDate = PWServerBase.CurrentTime;
      this.LiveDate = PWServerBase.CurrentTime;
      this.ExpirationDate = PWServerBase.CurrentTime;
      this.CreationDate = PWServerBase.CurrentTime;
      this.ModifiedDate = PWServerBase.CurrentTime;
      this.OneDayPrice = -1;
      this.ThreeDayPrice = -1;
      this.SevenDayPrice = -1;
      this.ThirtyDayPrice = -1;
      this.NinetyDayPrice = -1;
      this.PermanentPrice = -1;
      this.OneDayZenPrice = -1;
      this.ThreeDayZenPrice = -1;
      this.SevenDayZenPrice = -1;
      this.ThirtyDayZenPrice = -1;
      this.NinetyDayZenPrice = -1;
      this.PermanentZenPrice = -1;
      this.InitialQuantity = -1;
      this.QuantityLeft = -1;
      this.Discount = 1.0;
      this.SortIndex = -1;
      this.ItemGuid = Guid.NewGuid();
      this.ItemId = -1;
      this.ItemTags = "";
      this.FriendlyName = "";
      this.MaxOwned = -1;
      this.ActivationType = (byte) 0;
      this.ActivationData = "<Root />";
      this.VisibilityLevel = (byte) 0;
    }

    public object Clone() => (object) new StoreItem()
    {
      UserType = this.UserType,
      PreOrderDate = this.PreOrderDate,
      LiveDate = this.LiveDate,
      ExpirationDate = this.ExpirationDate,
      CreationDate = this.CreationDate,
      OneDayPrice = this.OneDayPrice,
      ThreeDayPrice = this.ThreeDayPrice,
      SevenDayPrice = this.SevenDayPrice,
      ThirtyDayPrice = this.ThirtyDayPrice,
      NinetyDayPrice = this.NinetyDayPrice,
      PermanentPrice = this.PermanentPrice,
      OneDayZenPrice = this.OneDayZenPrice,
      ThreeDayZenPrice = this.ThreeDayZenPrice,
      SevenDayZenPrice = this.SevenDayZenPrice,
      ThirtyDayZenPrice = this.ThirtyDayZenPrice,
      NinetyDayZenPrice = this.NinetyDayZenPrice,
      PermanentZenPrice = this.PermanentZenPrice,
      InitialQuantity = this.InitialQuantity,
      QuantityLeft = this.QuantityLeft,
      Discount = this.Discount,
      ItemGuid = this.ItemGuid,
      ItemId = this.ItemId,
      ItemTags = (string) this.ItemTags.Clone(),
      FriendlyName = (string) this.FriendlyName.Clone(),
      MaxOwned = this.MaxOwned,
      ActivationType = this.ActivationType,
      ActivationData = this.ActivationData,
      VisibilityLevel = this.VisibilityLevel,
      Hidden = this.Hidden,
      SortIndex = this.SortIndex
    };

    public int GetPrice(EPurchaseLength InLength, ECurrencyType Currency)
    {
      switch (Currency)
      {
        case ECurrencyType.EC_GP:
          switch (InLength)
          {
            case EPurchaseLength.EPL_OneDay:
              return this.OneDayPrice;
            case EPurchaseLength.EPL_ThreeDay:
              return this.ThreeDayPrice;
            case EPurchaseLength.EPL_SevenDay:
              return this.SevenDayPrice;
            case EPurchaseLength.EPL_ThirtyDay:
              return this.ThirtyDayPrice;
            case EPurchaseLength.EPL_NinetyDay:
              return this.NinetyDayPrice;
            case EPurchaseLength.EPL_Perm:
              return this.PermanentPrice;
          }
          break;
        case ECurrencyType.EC_ZP:
          switch (InLength)
          {
            case EPurchaseLength.EPL_OneDay:
              return this.OneDayZenPrice;
            case EPurchaseLength.EPL_ThreeDay:
              return this.ThreeDayZenPrice;
            case EPurchaseLength.EPL_SevenDay:
              return this.SevenDayZenPrice;
            case EPurchaseLength.EPL_ThirtyDay:
              return this.ThirtyDayZenPrice;
            case EPurchaseLength.EPL_NinetyDay:
              return this.NinetyDayZenPrice;
            case EPurchaseLength.EPL_Perm:
              return this.PermanentZenPrice;
          }
          break;
      }
      return -1;
    }

    public static DateTime GetExpirationDate(EPurchaseLength InLength)
    {
      DateTime currentTime = PWServerBase.CurrentTime;
      switch (InLength)
      {
        case EPurchaseLength.EPL_OneDay:
          return currentTime.AddDays(1.0);
        case EPurchaseLength.EPL_ThreeDay:
          return currentTime.AddDays(3.0);
        case EPurchaseLength.EPL_SevenDay:
          return currentTime.AddDays(7.0);
        case EPurchaseLength.EPL_ThirtyDay:
          return currentTime.AddDays(30.0);
        case EPurchaseLength.EPL_NinetyDay:
          return currentTime.AddDays(90.0);
        case EPurchaseLength.EPL_Perm:
          return currentTime.AddYears(100);
        default:
          return currentTime;
      }
    }

    public override bool Equals(object rhs)
    {
      if (!(rhs.GetType() == this.GetType()))
        return false;
      StoreItem storeItem = (StoreItem) rhs;
      return this.ItemGuid == storeItem.ItemGuid && this.UserType == storeItem.UserType && (this.ItemId == storeItem.ItemId && this.QuantityLeft == storeItem.QuantityLeft) && (this.InitialQuantity == storeItem.InitialQuantity && this.ItemTags == storeItem.ItemTags && (this.LiveDate == storeItem.LiveDate && this.PreOrderDate == storeItem.PreOrderDate)) && (this.ExpirationDate == storeItem.ExpirationDate && this.OneDayPrice == storeItem.OneDayPrice && (this.ThreeDayPrice == storeItem.ThreeDayPrice && this.SevenDayPrice == storeItem.SevenDayPrice) && (this.ThirtyDayPrice == storeItem.ThirtyDayPrice && this.NinetyDayPrice == storeItem.NinetyDayPrice && (this.PermanentPrice == storeItem.PermanentPrice && this.OneDayZenPrice == storeItem.OneDayZenPrice))) && (this.ThreeDayZenPrice == storeItem.ThreeDayZenPrice && this.SevenDayZenPrice == storeItem.SevenDayZenPrice && (this.ThirtyDayZenPrice == storeItem.ThirtyDayZenPrice && this.PermanentZenPrice == storeItem.PermanentZenPrice) && (this.FriendlyName == storeItem.FriendlyName && this.MaxOwned == storeItem.MaxOwned && (this.ActivationData == storeItem.ActivationData && (int) this.ActivationType == (int) storeItem.ActivationType)) && ((int) this.VisibilityLevel == (int) storeItem.VisibilityLevel && this.SortIndex == storeItem.SortIndex));
    }

    public override int GetHashCode() => base.GetHashCode();

    public static bool operator ==(StoreItem a, StoreItem b)
    {
      if (object.ReferenceEquals((object) a, (object) b))
        return true;
      return (object) a != null && (object) b != null && a.Equals((object) b);
    }

    public static bool operator !=(StoreItem a, StoreItem b) => !(a == b);

    public override bool ParseFromArray(object[] InArray)
    {
      try
      {
        this.ValidatedAssign(ref this.ItemGuid, InArray[0]);
        this.ValidatedAssign<int>(ref this.UserType, InArray[1]);
        this.ValidatedAssign<int>(ref this.ItemId, InArray[2]);
        this.ValidatedAssign(ref this.ItemTags, InArray[3]);
        this.ValidatedAssign<int>(ref this.QuantityLeft, InArray[4]);
        this.ValidatedAssign(ref this.PreOrderDate, InArray[5]);
        this.ValidatedAssign(ref this.LiveDate, InArray[6]);
        this.ValidatedAssign(ref this.ExpirationDate, InArray[7]);
        this.ValidatedAssign<int>(ref this.OneDayPrice, InArray[8]);
        this.ValidatedAssign<int>(ref this.ThreeDayPrice, InArray[9]);
        this.ValidatedAssign<int>(ref this.SevenDayPrice, InArray[10]);
        this.ValidatedAssign<int>(ref this.ThirtyDayPrice, InArray[11]);
        this.ValidatedAssign<int>(ref this.NinetyDayPrice, InArray[12]);
        this.ValidatedAssign<int>(ref this.PermanentPrice, InArray[13]);
        this.ValidatedAssign<int>(ref this.OneDayZenPrice, InArray[14]);
        this.ValidatedAssign<int>(ref this.ThreeDayZenPrice, InArray[15]);
        this.ValidatedAssign<int>(ref this.SevenDayZenPrice, InArray[16]);
        this.ValidatedAssign<int>(ref this.ThirtyDayZenPrice, InArray[17]);
        this.ValidatedAssign<int>(ref this.NinetyDayZenPrice, InArray[18]);
        this.ValidatedAssign<int>(ref this.PermanentZenPrice, InArray[19]);
        this.ValidatedAssign(ref this.FriendlyName, InArray[20]);
        this.ValidatedAssign<int>(ref this.MaxOwned, InArray[21]);
        this.ValidatedAssign<byte>(ref this.ActivationType, InArray[22]);
        this.ValidatedAssign(ref this.ActivationData, InArray[23]);
        this.ValidatedAssign<byte>(ref this.VisibilityLevel, InArray[24]);
        this.ValidatedAssign(ref this.CreationDate, InArray[25]);
        this.ValidatedAssign(ref this.ModifiedDate, InArray[26]);
        return true;
      }
      catch (Exception ex)
      {
        return false;
      }
    }

    public EPurchaseResult ValidatePurchase(
      long UserId,
      PWInventory Inventory,
      EPurchaseLength Duration,
      ECurrencyType Currency,
      int Quantity)
    {
      if (Inventory == null || Inventory.Items == null)
        return EPurchaseResult.EPR_UnknownError;
      if (this.PreOrderDate > PWServerBase.CurrentTime || this.ExpirationDate < PWServerBase.CurrentTime)
        return EPurchaseResult.EPL_NoLongerAvailable;
      if (this.InitialQuantity > 0 && this.QuantityLeft < Quantity)
        return EPurchaseResult.EPL_QuantityNotAvailable;
      int num = -1;
      switch (Currency)
      {
        case ECurrencyType.EC_GP:
        case ECurrencyType.EC_ZP:
          num = this.GetPrice(Duration, Currency);
          break;
      }
      if (num < 0)
        return EPurchaseResult.EPR_InvalidLength;
      return UserId == Inventory.UserId && Inventory.CapReached ? EPurchaseResult.EPR_InventoryCapped : EPurchaseResult.EPR_Success;
    }

    public EPurchaseResult ValdiateRenewal(
      PWInventory Inventory,
      EPurchaseLength Duration,
      ECurrencyType Currency)
    {
      return this.ValidatePurchase(0L, Inventory, Duration, Currency, 1);
    }

    public EPurchaseResult ValidateActivate(PWInventory Inventory)
    {
      if (Inventory == null || Inventory.Items == null)
        return EPurchaseResult.EPR_UnknownError;
      return this.MaxOwned >= 1 && this.MaxOwned <= Inventory.Items.FindAll((Predicate<PWInventoryItem>) (x => x.ItemId == this.ItemId && x.Activated && x.ExpirationDate > PWServerBase.CurrentTime)).Count ? EPurchaseResult.EPR_MaxOwned : EPurchaseResult.EPR_Success;
    }

    public override string ToString() => this.UserType.ToString() + "," + (object) this.ItemId + "," + this.FriendlyName;

    public bool IsValidForPurchase(EPurchaseLength InLength, ECurrencyType Currency) => !(PWServerBase.CurrentTime < this.PreOrderDate) && !(PWServerBase.CurrentTime > this.ExpirationDate) && this.GetPrice(InLength, Currency) != -1 && (this.InitialQuantity <= 0 || this.QuantityLeft > 0);

    public int CalculateCostFor(EPurchaseLength InLength, ECurrencyType Currency, int count)
    {
      int price = this.GetPrice(InLength, Currency);
      return price == -1 ? -1 : (int) ((double) price * this.Discount * (double) count);
    }

    public PWInventoryItem GeneratePurchaseItem(
      long UserID,
      EPurchaseLength PurchaseLength,
      int Count)
    {
      return new PWInventoryItem()
      {
        ItemId = this.ItemId,
        UserId = UserID,
        StoreItemId = this.ItemGuid,
        ExpirationDate = StoreItem.GetExpirationDate(PurchaseLength),
        UsesLeft = Count,
        PurchaseDuration = (byte) PurchaseLength,
        PurchaseDate = PWServerBase.CurrentTime,
        Activated = false
      };
    }

    public bool GenerateActivationRequests(
      long InUserID,
      List<PWInventoryItem> UserInventory,
      PWInventoryItem TargetItem,
      List<StoreItem> AllStoreItems,
      ref List<PWInventoryItem> NewItems,
      ref List<PWInventoryItem> DeleteItems,
      ref List<PWInventoryItem> ChangedItems)
    {
      bool flag = false;
      if (TargetItem.ItemId == this.ItemId)
      {
        List<BaseItem> activationData = this.ParseActivationData(TargetItem, UserInventory, AllStoreItems, ref NewItems, ref DeleteItems, ref ChangedItems);
        if (activationData != null && activationData.Count > 0)
        {
          using (List<BaseItem>.Enumerator enumerator = activationData.GetEnumerator())
          {
label_11:
            while (enumerator.MoveNext())
            {
              BaseItem current = enumerator.Current;
              PWInventoryItem TargetItem1 = new PWInventoryItem();
              TargetItem1.ItemId = current.ID;
              TargetItem1.UserId = InUserID;
              TargetItem1.StoreItemId = this.ItemGuid;
              TargetItem1.UsesLeft = Math.Max(current.Quantity, 1);
              TargetItem1.PurchaseDuration = (byte) current.Duration;
              TargetItem1.PurchaseDate = PWServerBase.CurrentTime;
              TargetItem1.ExpirationDate = StoreItem.GetExpirationDate(current.Duration != -1 ? (EPurchaseLength) current.Duration : (EPurchaseLength) TargetItem.PurchaseDuration);
              if (current.bActivated)
              {
                StoreItem parentStoreItem = TargetItem1.FindParentStoreItem(AllStoreItems);
                if (parentStoreItem != (StoreItem) null && (parentStoreItem.ActivationType == (byte) 3 || parentStoreItem.ActivationType == (byte) 4))
                {
                  int num = 0;
                  while (true)
                  {
                    if (num < TargetItem1.UsesLeft && parentStoreItem.GenerateActivationRequests(InUserID, UserInventory, TargetItem1, AllStoreItems, ref NewItems, ref DeleteItems, ref ChangedItems))
                      ++num;
                    else
                      goto label_11;
                  }
                }
                else
                {
                  TargetItem1.Activated = current.bActivated;
                  TargetItem1.ActivationDate = PWServerBase.CurrentTime;
                }
              }
              NewItems.Add(TargetItem1);
            }
          }
          flag = true;
        }
      }
      return flag;
    }

    private List<BaseItem> ParseActivationData(
      PWInventoryItem TargetItem,
      List<PWInventoryItem> AllInventory,
      List<StoreItem> StoreItems,
      ref List<PWInventoryItem> NewItems,
      ref List<PWInventoryItem> DeleteItems,
      ref List<PWInventoryItem> ChangedItems)
    {
      XmlSerializer xmlSerializer1 = XmlSerializerCache.Create(typeof (RandomItem[]));
      XmlSerializer xmlSerializer2 = XmlSerializerCache.Create(typeof (BaseItem[]));
      XmlSerializer xmlSerializer3 = XmlSerializerCache.Create(typeof (PWPackItem));
      XmlSerializer xmlSerializer4 = XmlSerializerCache.Create(typeof (PWLockboxItem));
      List<BaseItem> baseItemList = new List<BaseItem>();
      XmlReader xmlReader = (XmlReader) new XmlTextReader((TextReader) new StringReader(this.ActivationData));
      if (xmlSerializer1.CanDeserialize(xmlReader))
      {
        RandomItem[] randomItemArray = (RandomItem[]) xmlSerializer1.Deserialize(xmlReader);
        baseItemList.Add(RandomItem.ChooseRandomItem((IEnumerable<RandomItem>) randomItemArray));
      }
      else if (xmlSerializer2.CanDeserialize(xmlReader))
      {
        BaseItem[] baseItemArray = (BaseItem[]) xmlSerializer2.Deserialize(xmlReader);
        baseItemList.AddRange((IEnumerable<BaseItem>) baseItemArray);
      }
      else if (xmlSerializer3.CanDeserialize(xmlReader))
        baseItemList = ((PWPackItem) xmlSerializer3.Deserialize(xmlReader)).GenerateItems();
      else if (xmlSerializer4.CanDeserialize(xmlReader))
        baseItemList = ((PWLockboxItem) xmlSerializer4.Deserialize(xmlReader)).GenerateItemsFor(AllInventory, ref DeleteItems, ref ChangedItems);
      return baseItemList;
    }

    public int GetTokenActivationQuantity()
    {
      XmlSerializer xmlSerializer = new XmlSerializer(typeof (PWTokenItem));
      XmlReader xmlReader = (XmlReader) new XmlTextReader((TextReader) new StringReader(this.ActivationData));
      return xmlSerializer.CanDeserialize(xmlReader) ? ((PWTokenItem) xmlSerializer.Deserialize(xmlReader)).Amount : -1;
    }

    public override SqlCommand GetCommand(ESqlItemOp SqlOp, ref string LogOutput)
    {
      SqlCommand command;
      switch (SqlOp)
      {
        case ESqlItemOp.ESIO_Add:
          command = new SqlCommand("SP_Store_Item_Add");
          command.CommandType = CommandType.StoredProcedure;
          command.AddParameter<int>("@ItemID", SqlDbType.Int, this.ItemId);
          command.AddParameter<byte[]>("@InstanceID", SqlDbType.VarBinary, this.ItemGuid.ToByteArray());
          command.AddParameter<int>("@UserType", SqlDbType.Int, this.UserType);
          command.AddParameter<string>("@ItemTags", SqlDbType.VarChar, this.ItemTags);
          command.AddParameter<int>("@QuantityLeft", SqlDbType.Int, this.InitialQuantity);
          command.AddParameter<DateTime>("@PreOrderDate", SqlDbType.DateTime, this.PreOrderDate);
          command.AddParameter<DateTime>("@LiveDate", SqlDbType.DateTime, this.LiveDate);
          command.AddParameter<DateTime>("@ExpirationDate", SqlDbType.DateTime, this.ExpirationDate);
          command.AddParameter<DateTime>("@CreationDate", SqlDbType.DateTime, this.CreationDate);
          command.AddParameter<DateTime>("@ModifiedDate", SqlDbType.DateTime, this.ModifiedDate);
          command.AddParameter<int>("@OneDayPrice", SqlDbType.Int, this.OneDayPrice);
          command.AddParameter<int>("@ThreeDayPrice", SqlDbType.Int, this.ThreeDayPrice);
          command.AddParameter<int>("@SevenDayPrice", SqlDbType.Int, this.SevenDayPrice);
          command.AddParameter<int>("@ThirtyDayPrice", SqlDbType.Int, this.ThirtyDayPrice);
          command.AddParameter<int>("@NinetyDayPrice", SqlDbType.Int, this.NinetyDayPrice);
          command.AddParameter<int>("@PermanentPrice", SqlDbType.Int, this.PermanentPrice);
          command.AddParameter<int>("@OneDayZenPrice", SqlDbType.Int, this.OneDayZenPrice);
          command.AddParameter<int>("@ThreeDayZenPrice", SqlDbType.Int, this.ThreeDayZenPrice);
          command.AddParameter<int>("@SevenDayZenPrice", SqlDbType.Int, this.SevenDayZenPrice);
          command.AddParameter<int>("@ThirtyDayZenPrice", SqlDbType.Int, this.ThirtyDayZenPrice);
          command.AddParameter<int>("@NinetyDayZenPrice", SqlDbType.Int, this.NinetyDayZenPrice);
          command.AddParameter<int>("@PermanetZenPrice", SqlDbType.Int, this.PermanentZenPrice);
          command.AddParameter<string>("@FriendlyName", SqlDbType.VarChar, this.FriendlyName);
          command.AddParameter<int>("@MaxOwned", SqlDbType.Int, this.MaxOwned);
          command.AddParameter<byte>("@ActivationType", SqlDbType.TinyInt, this.ActivationType);
          command.AddParameter<SqlXml>("@ActivationData", SqlDbType.Xml, new SqlXml((XmlReader) new XmlTextReader((TextReader) new StringReader(this.ActivationData))));
          command.AddParameter<byte>("@Hidden", SqlDbType.TinyInt, this.VisibilityLevel);
          LogOutput = "Adding Store Item [" + this.ItemId.ToString() + " ," + this.ItemGuid.ToString() + "]";
          break;
        case ESqlItemOp.ESIO_Update:
          command = new SqlCommand("SP_Store_Item_Update");
          command.CommandType = CommandType.StoredProcedure;
          command.AddParameter<int>("@ItemID", SqlDbType.Int, this.ItemId);
          command.AddParameter<byte[]>("@InstanceID", SqlDbType.VarBinary, this.ItemGuid.ToByteArray());
          command.AddParameter<int>("@UserType", SqlDbType.Int, this.UserType);
          command.AddParameter<string>("@ItemTags", SqlDbType.VarChar, this.ItemTags);
          command.AddParameter<int>("@QuantityLeft", SqlDbType.Int, this.InitialQuantity);
          command.AddParameter<DateTime>("@PreOrderDate", SqlDbType.DateTime, this.PreOrderDate);
          command.AddParameter<DateTime>("@LiveDate", SqlDbType.DateTime, this.LiveDate);
          command.AddParameter<DateTime>("@ExpirationDate", SqlDbType.DateTime, this.ExpirationDate);
          command.AddParameter<DateTime>("@CreationDate", SqlDbType.DateTime, this.CreationDate);
          command.AddParameter<DateTime>("@ModifiedDate", SqlDbType.DateTime, this.ModifiedDate);
          command.AddParameter<int>("@OneDayPrice", SqlDbType.Int, this.OneDayPrice);
          command.AddParameter<int>("@ThreeDayPrice", SqlDbType.Int, this.ThreeDayPrice);
          command.AddParameter<int>("@SevenDayPrice", SqlDbType.Int, this.SevenDayPrice);
          command.AddParameter<int>("@ThirtyDayPrice", SqlDbType.Int, this.ThirtyDayPrice);
          command.AddParameter<int>("@NinetyDayPrice", SqlDbType.Int, this.NinetyDayPrice);
          command.AddParameter<int>("@PermanentPrice", SqlDbType.Int, this.PermanentPrice);
          command.AddParameter<int>("@OneDayZenPrice", SqlDbType.Int, this.OneDayZenPrice);
          command.AddParameter<int>("@ThreeDayZenPrice", SqlDbType.Int, this.ThreeDayZenPrice);
          command.AddParameter<int>("@SevenDayZenPrice", SqlDbType.Int, this.SevenDayZenPrice);
          command.AddParameter<int>("@ThirtyDayZenPrice", SqlDbType.Int, this.ThirtyDayZenPrice);
          command.AddParameter<int>("@NinetyDayZenPrice", SqlDbType.Int, this.NinetyDayZenPrice);
          command.AddParameter<int>("@PermanetZenPrice", SqlDbType.Int, this.PermanentZenPrice);
          command.AddParameter<string>("@FriendlyName", SqlDbType.VarChar, this.FriendlyName);
          command.AddParameter<int>("@MaxOwned", SqlDbType.Int, this.MaxOwned);
          command.AddParameter<byte>("@ActivationType", SqlDbType.TinyInt, this.ActivationType);
          command.AddParameter<SqlXml>("@ActivationData", SqlDbType.Xml, new SqlXml((XmlReader) new XmlTextReader((TextReader) new StringReader(this.ActivationData))));
          command.AddParameter<byte>("@Hidden", SqlDbType.TinyInt, this.VisibilityLevel);
          LogOutput = "Updating Store Item [" + this.ItemId.ToString() + " ," + this.ItemGuid.ToString() + "]";
          break;
        case ESqlItemOp.ESIO_Remove:
          command = new SqlCommand("SP_Store_Item_Remove");
          command.CommandType = CommandType.StoredProcedure;
          command.AddParameter<byte[]>("@InstanceID", SqlDbType.VarBinary, this.ItemGuid.ToByteArray());
          LogOutput = "Removing Store Item [" + this.ItemGuid.ToString() + "]";
          break;
        case ESqlItemOp.ESIO_QueryAll:
          command = new SqlCommand("SP_Store_Item_Select_All");
          command.CommandType = CommandType.StoredProcedure;
          LogOutput = "Querying Store Items [All]";
          break;
        default:
          command = base.GetCommand(SqlOp, ref LogOutput);
          break;
      }
      return command;
    }

    public override bool ParseServerResult(PWSqlItemReqBase<StoreItem> SqlReq)
    {
      if (SqlReq is PWSqlItemQueryReqBase<StoreItem> itemQueryReqBase && itemQueryReqBase.ResponseItems != null)
      {
        foreach (StoreItem responseItem in itemQueryReqBase.ResponseItems)
          responseItem.Hidden = !PWRegionMap.IsVisible(itemQueryReqBase.Connection.OwningServer.ServerRegionName, (EServerLevel) responseItem.VisibilityLevel);
      }
      return base.ParseServerResult(SqlReq);
    }

    public SqlCommand GetCommand(ESqlItemOp SqlOp, int Key, ref string LogOutput)
    {
      SqlCommand command;
      if (SqlOp == ESqlItemOp.ESIO_QueryKeyed)
      {
        command = new SqlCommand("SP_Store_Item_Select_By_Usertype");
        command.CommandType = CommandType.StoredProcedure;
        command.AddParameter<int>("@UserType", SqlDbType.Int, Key);
        LogOutput = "Querying Store Items [" + Key.ToString() + "]";
      }
      else
        command = base.GetCommand(SqlOp, ref LogOutput);
      return command;
    }

    public enum ActivationTypes
    {
      AT_None,
      AT_Apply,
      AT_InputString,
      AT_Consume,
      AT_Stackable,
    }
  }
}
