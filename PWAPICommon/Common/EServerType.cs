﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.EServerType
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.ComponentModel;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public enum EServerType
  {
    [Description("Auth")] EST_Auth,
    [Description("Item")] EST_Item,
    [Description("Super")] EST_Super,
    [Description("Social")] EST_Social,
    [Description("Voip")] EST_Voice,
    [Description("Crash")] EST_Report,
  }
}
