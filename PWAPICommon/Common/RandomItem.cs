﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.RandomItem
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class RandomItem : BaseItem
  {
    [DefaultValue(1)]
    [XmlAttribute("Weight")]
    public int Weight;

    public RandomItem() => this.Weight = 1;

    public static BaseItem ChooseRandomItem(IEnumerable<RandomItem> InItems)
    {
      int Max = 0;
      foreach (RandomItem inItem in InItems)
        Max += inItem.Weight;
      int num = PWRandom.Next(Max);
      foreach (RandomItem inItem in InItems)
      {
        if (num < inItem.Weight)
          return (BaseItem) inItem;
        num -= inItem.Weight;
      }
      return (BaseItem) null;
    }

    public override string ToString() => "[" + (object) this.ID + "," + (object) this.Weight + "," + (object) this.bActivated + "," + (object) this.Quantity + "," + (object) this.Duration + "]";
  }
}
