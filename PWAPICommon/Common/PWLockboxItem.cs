﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWLockboxItem
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PWLockboxItem
  {
    public List<PWLockboxItem.KeyData> KeyItems;
    public EKeyOp KeyOp;
    public PWPackItem PackItem;

    public PWLockboxItem()
    {
      this.KeyItems = new List<PWLockboxItem.KeyData>();
      this.PackItem = new PWPackItem();
      this.KeyOp = EKeyOp.EKO_Or;
    }

    public bool ContainsKey(Guid g) => this.KeyItems.FindIndex((Predicate<PWLockboxItem.KeyData>) (x => x.KeyID == g)) != -1;

    public List<BaseItem> GenerateItemsFor(
      List<PWInventoryItem> AllInventory,
      ref List<PWInventoryItem> DeleteItems,
      ref List<PWInventoryItem> ChangedItems)
    {
      if (this.KeyItems.Count <= 0)
        return this.PackItem.GenerateItems();
      this.KeyItems.Sort(new Comparison<PWLockboxItem.KeyData>(PWLockboxItem.KeyData.CompareKeyData));
      using (List<PWLockboxItem.KeyData>.Enumerator enumerator = this.KeyItems.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          PWLockboxItem.KeyData key = enumerator.Current;
          PWInventoryItem pwInventoryItem = AllInventory.Find((Predicate<PWInventoryItem>) (x => x.StoreItemId == key.KeyID));
          if (pwInventoryItem != null)
          {
            if (pwInventoryItem.UsesLeft > 1)
            {
              --pwInventoryItem.UsesLeft;
              ChangedItems.Add(pwInventoryItem);
            }
            else
              DeleteItems.Add(pwInventoryItem);
            return this.PackItem.GenerateItems();
          }
        }
      }
      return (List<BaseItem>) null;
    }

    public class KeyData
    {
      public Guid KeyID;
      public int KeyPriority;

      public static int CompareKeyData(PWLockboxItem.KeyData a, PWLockboxItem.KeyData b) => a.KeyPriority - b.KeyPriority;
    }
  }
}
