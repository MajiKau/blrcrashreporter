﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Wrappers.PWSqlItemReqBase`1
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Wrappers
{
  [ComVisible(true)]
  public abstract class PWSqlItemReqBase<T> : PWSQLReq where T : PWSqlItemBase<T>, new()
  {
    protected static readonly T DefaultObject = new T();

    public abstract ESqlItemOp SqlOp { get; }

    public PWSqlItemReqBase()
      : this((PWConnectionBase) null)
    {
    }

    public PWSqlItemReqBase(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => PWSqlItemReqBase<T>.DefaultObject.GetMessageType(this.SqlOp);

    protected abstract SqlCommand GetCommand(ref string LogOutput);

    public override bool SubmitServerQuery()
    {
      string empty = string.Empty;
      this.RequestQuery = this.GetCommand(ref empty);
      if (!string.IsNullOrEmpty(empty))
        this.OwningServer.Log(empty, (PWConnectionBase) null, false, ELoggingLevel.ELL_Verbose);
      return base.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => base.ParseServerResult(ForRequest);

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
