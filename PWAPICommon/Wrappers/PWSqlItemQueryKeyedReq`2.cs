﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Wrappers.PWSqlItemQueryKeyedReq`2
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Wrappers
{
  [ComVisible(true)]
  public class PWSqlItemQueryKeyedReq<T, U> : PWSqlItemQueryReqBase<T> where T : PWSqlItemBase<T>, IPWSqlItemKeyed<U>, new()
  {
    public U Key;

    public override ESqlItemOp SqlOp => ESqlItemOp.ESIO_QueryKeyed;

    public PWSqlItemQueryKeyedReq()
      : this((PWConnectionBase) null)
    {
    }

    public PWSqlItemQueryKeyedReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override SqlCommand GetCommand(ref string LogOutput) => PWSqlItemReqBase<T>.DefaultObject.GetCommand(this.SqlOp, this.Key, ref LogOutput);
  }
}
