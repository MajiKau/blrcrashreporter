﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.ELoginCacheResponse
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;

namespace PWAPICommon
{
  [ComVisible(true)]
  public enum ELoginCacheResponse
  {
    ELCR_OK,
    ELCR_NotPending,
    ELCR_Duplicate,
    ELCR_Blacklisted,
    ELCR_Whitelisted,
    ELCR_Failed,
  }
}
