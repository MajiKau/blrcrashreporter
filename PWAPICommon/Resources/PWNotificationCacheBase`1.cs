﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Resources.PWNotificationCacheBase`1
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.General;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Resources
{
  [ComVisible(true)]
  public abstract class PWNotificationCacheBase<T> : 
    IPWNotificationCache<T>,
    IPWNotificationCache,
    IPWResource
  {
    private string ResourceName;
    private PWServerBase OwningServer;
    private ConcurrentDictionary<long, PWNotificationCacheBase<T>.PWNotificationClientBase> ActiveClients;
    private TimeSpan PushInterval;
    private PWNotificationCacheBase<T>.SpawnReqDelegate ReqSpawner = (PWNotificationCacheBase<T>.SpawnReqDelegate) ((Connection, Items) => (PWNotificationReq<T>) null);
    private PWNotificationCacheBase<T>.DispatchReqDelegate ReqDispatcher = (PWNotificationCacheBase<T>.DispatchReqDelegate) (NotificationReq => false);

    public PWNotificationCacheBase(
      string InResourceName,
      PWServerBase InOwningServer,
      TimeSpan InPushInterval)
    {
      this.ResourceName = InResourceName;
      this.OwningServer = InOwningServer;
      this.PushInterval = InPushInterval;
      this.ActiveClients = new ConcurrentDictionary<long, PWNotificationCacheBase<T>.PWNotificationClientBase>();
    }

    public virtual void StartResource()
    {
    }

    public virtual void StopResource() => this.PurgeClients();

    public virtual Dictionary<string, string> CheckHealth() => new Dictionary<string, string>()
    {
      [this.ResourceName + ":Pending"] = this.ActiveClients.Count.ToString()
    };

    public void NotifyConnected(PWConnectionBase Connection)
    {
      if (Connection == null || Connection.UserID == PWServerBase.EmptyUserID)
        return;
      this.ConnectClient(this.SpawnClient(Connection, this.ReqSpawner));
    }

    public void NotifyDisconnected(PWConnectionBase Connection)
    {
      if (Connection == null || Connection.UserID == PWServerBase.EmptyUserID)
        return;
      PWNotificationCacheBase<T>.PWNotificationClientBase Client = (PWNotificationCacheBase<T>.PWNotificationClientBase) null;
      if (!this.ActiveClients.TryRemove(Connection.UserID, out Client))
        return;
      this.DisconnectClient(Client);
    }

    public bool Enqueue(PWNotificationReq<T> NotificationReq)
    {
      if (NotificationReq == null)
        return false;
      PWNotificationCacheBase<T>.PWNotificationClientBase notificationClientBase1 = (PWNotificationCacheBase<T>.PWNotificationClientBase) null;
      if (NotificationReq.UserID == PWServerBase.GlobalUserID)
      {
        foreach (PWNotificationCacheBase<T>.PWNotificationClientBase notificationClientBase2 in (IEnumerable<PWNotificationCacheBase<T>.PWNotificationClientBase>) this.ActiveClients.Values)
          notificationClientBase2.Enqueue(NotificationReq);
        return true;
      }
      if (!this.ActiveClients.TryGetValue(NotificationReq.UserID, out notificationClientBase1))
        return this.ReqDispatcher(NotificationReq);
      notificationClientBase1.Enqueue(NotificationReq);
      return true;
    }

    public void SetNotificationReqType<U>() where U : PWNotificationReq<T>, new() => this.ReqSpawner = (PWNotificationCacheBase<T>.SpawnReqDelegate) ((Connection, Items) =>
    {
      PWNotificationReq<T> pwNotificationReq = (PWNotificationReq<T>) null;
      if (Connection != null && Items != null && Items.Length > 0)
      {
        pwNotificationReq = (PWNotificationReq<T>) new U();
        pwNotificationReq.Connection = Connection;
        pwNotificationReq.UserID = Connection.UserID;
        pwNotificationReq.Items = Items;
      }
      return pwNotificationReq;
    });

    public void SetPresenceType<U>() where U : PWPresenceInfo => this.ReqDispatcher = (PWNotificationCacheBase<T>.DispatchReqDelegate) (NotificationReq =>
    {
      PWPresenceCache<U> resource = this.OwningServer.GetResource<PWPresenceCache<U>>();
      return resource != null && resource.SendExternalQuery<PWNotificationReq<T>>(NotificationReq.UserID, NotificationReq);
    });

    protected abstract PWNotificationCacheBase<T>.PWNotificationClientBase SpawnClient(
      PWConnectionBase InConnection,
      PWNotificationCacheBase<T>.SpawnReqDelegate InSpawner);

    private void ConnectClient(
      PWNotificationCacheBase<T>.PWNotificationClientBase Client)
    {
      this.Log("Added Notification Data for User " + (object) Client.UserID, (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      this.ActiveClients[Client.UserID] = Client;
    }

    private void DisconnectClient(
      PWNotificationCacheBase<T>.PWNotificationClientBase Client)
    {
      this.Log("Removed Notification Data for User " + (object) Client.UserID, (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      Client.Stop();
    }

    private void PurgeClients()
    {
      foreach (PWNotificationCacheBase<T>.PWNotificationClientBase Client in (IEnumerable<PWNotificationCacheBase<T>.PWNotificationClientBase>) this.ActiveClients.Values)
        this.DisconnectClient(Client);
      this.ActiveClients.Clear();
    }

    protected void Log(string Text, bool bLogToRemoteServer, ELoggingLevel LogLevel) => this.Log(Text, (PWConnectionBase) null, bLogToRemoteServer, LogLevel);

    protected void Log(
      string Text,
      PWConnectionBase Connection,
      bool bLogToRemoteServer,
      ELoggingLevel LogLevel)
    {
      if (this.OwningServer == null)
        return;
      this.OwningServer.Log(Text, Connection, bLogToRemoteServer, LogLevel);
    }

    protected delegate PWNotificationReq<T> SpawnReqDelegate(
      PWConnectionBase Connection,
      T[] Items);

    protected delegate bool DispatchReqDelegate(PWNotificationReq<T> NotificationReq);

    protected abstract class PWNotificationClientBase
    {
      protected PWConnectionBase Connection;
      private PWAsyncTask PushTask;

      public long UserID => this.Connection == null ? PWServerBase.EmptyUserID : this.Connection.UserID;

      public bool Connected => this.Connection != null;

      public PWNotificationClientBase(
        PWConnectionBase InConnection,
        PWNotificationCacheBase<T>.SpawnReqDelegate InSpawner)
      {
        PWNotificationCacheBase<T>.PWNotificationClientBase notificationClientBase = this;
        this.PushTask = new PWAsyncTask((Action) (() => InSpawner(notificationClientBase.Connection, notificationClientBase.Dequeue())?.SubmitServerQuery()));
        this.Connection = InConnection;
      }

      public void Stop()
      {
        lock (this)
        {
          this.PushTask.Stop();
          this.PushTask = (PWAsyncTask) null;
          this.Connection = (PWConnectionBase) null;
        }
      }

      protected void SendAsync()
      {
        lock (this)
        {
          if (this.PushTask == null)
            return;
          this.PushTask.Start();
        }
      }

      public abstract void Enqueue(PWNotificationReq<T> NotificationReq);

      public abstract T[] Dequeue();
    }

    protected abstract class PWNotificationClientBase<U> : 
      PWNotificationCacheBase<T>.PWNotificationClientBase
    {
      private PWBatchQueue<T, U> BatchQueue;

      public PWNotificationClientBase(
        PWConnectionBase InConnection,
        PWNotificationCacheBase<T>.SpawnReqDelegate InSpawner,
        PWBatchQueue<T, U> InBatchQueue)
        : base(InConnection, InSpawner)
      {
        this.BatchQueue = InBatchQueue;
      }

      public override void Enqueue(PWNotificationReq<T> NotificationReq)
      {
        this.BatchQueue.Enqueue((IEnumerable<T>) NotificationReq.Items);
        this.SendAsync();
      }

      public override T[] Dequeue() => this.BatchQueue.Dequeue();
    }
  }
}
