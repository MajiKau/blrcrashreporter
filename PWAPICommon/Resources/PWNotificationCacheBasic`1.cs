﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Resources.PWNotificationCacheBasic`1
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Resources
{
  [ComVisible(true)]
  public class PWNotificationCacheBasic<T> : PWNotificationCacheBase<T>
  {
    public PWNotificationCacheBasic(
      string InResourceName,
      PWServerBase InOwningServer,
      TimeSpan InPushInterval)
      : base(InResourceName, InOwningServer, InPushInterval)
    {
    }

    protected override PWNotificationCacheBase<T>.PWNotificationClientBase SpawnClient(
      PWConnectionBase InConnection,
      PWNotificationCacheBase<T>.SpawnReqDelegate InSpawner)
    {
      return (PWNotificationCacheBase<T>.PWNotificationClientBase) new PWNotificationCacheBasic<T>.PWNotificationClient(InConnection, InSpawner);
    }

    protected class PWNotificationClient : PWNotificationCacheBase<T>.PWNotificationClientBase<T>
    {
      public PWNotificationClient(
        PWConnectionBase InConnection,
        PWNotificationCacheBase<T>.SpawnReqDelegate InSpawner)
        : base(InConnection, InSpawner, (PWBatchQueue<T, T>) new PWBatchQueue<T>())
      {
      }
    }
  }
}
