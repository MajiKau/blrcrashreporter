﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Resources.PWNotificationCacheTemporal`1
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace PWAPICommon.Resources
{
  [ComVisible(true)]
  public class PWNotificationCacheTemporal<T> : PWNotificationCacheBase<T> where T : class, IComparable<T>
  {
    public PWNotificationCacheTemporal(
      string InResourceName,
      PWServerBase InOwningServer,
      TimeSpan InPushInterval)
      : base(InResourceName, InOwningServer, InPushInterval)
    {
    }

    protected override PWNotificationCacheBase<T>.PWNotificationClientBase SpawnClient(
      PWConnectionBase InConnection,
      PWNotificationCacheBase<T>.SpawnReqDelegate InSpawner)
    {
      return (PWNotificationCacheBase<T>.PWNotificationClientBase) new PWNotificationCacheTemporal<T>.PWNotificationClient(InConnection, InSpawner);
    }

    protected class PWNotification : IComparable<PWNotificationCacheTemporal<T>.PWNotification>
    {
      private T Item;
      private DateTime TimeStamp;

      public T UpdatedItem => this.Item;

      public int CompareTo(
        PWNotificationCacheTemporal<T>.PWNotification Other)
      {
        return this.UpdatedItem.CompareTo(Other.UpdatedItem);
      }

      public void Coalesce(T NewItem, DateTime NewTimeStamp)
      {
        if (!(this.TimeStamp <= NewTimeStamp))
          return;
        this.Item = NewItem;
        this.TimeStamp = NewTimeStamp;
      }

      public void Coalesce(
        PWNotificationCacheTemporal<T>.PWNotification NewNotification)
      {
        if (!(this.TimeStamp <= NewNotification.TimeStamp))
          return;
        this.Item = NewNotification.Item;
        this.TimeStamp = NewNotification.TimeStamp;
      }
    }

    protected class PWNotificationQueue : 
      PWBatchQueue<T, PWNotificationCacheTemporal<T>.PWNotification>
    {
      protected override PWNotificationCacheTemporal<T>.PWNotification Generate(
        T Item)
      {
        PWNotificationCacheTemporal<T>.PWNotification pwNotification = new PWNotificationCacheTemporal<T>.PWNotification();
        pwNotification.Coalesce(Item, PWServerBase.CurrentTime);
        return pwNotification;
      }

      protected override void Coalesce(
        PWNotificationCacheTemporal<T>.PWNotification BatchItem,
        List<PWNotificationCacheTemporal<T>.PWNotification> CoalescedItems)
      {
        PWNotificationCacheTemporal<T>.PWNotification pwNotification = CoalescedItems.SingleOrDefault<PWNotificationCacheTemporal<T>.PWNotification>((Func<PWNotificationCacheTemporal<T>.PWNotification, bool>) (x => x.CompareTo(BatchItem) == 0));
        if (pwNotification == null)
          CoalescedItems.Add(BatchItem);
        else
          pwNotification.Coalesce(BatchItem);
      }

      protected override T[] FormatOutput(
        IEnumerable<PWNotificationCacheTemporal<T>.PWNotification> CoalescedItems)
      {
        return CoalescedItems.Select<PWNotificationCacheTemporal<T>.PWNotification, T>((Func<PWNotificationCacheTemporal<T>.PWNotification, T>) (x => x.UpdatedItem)).ToArray<T>();
      }
    }

    protected class PWNotificationClient : 
      PWNotificationCacheBase<T>.PWNotificationClientBase<PWNotificationCacheTemporal<T>.PWNotification>
    {
      public PWNotificationClient(
        PWConnectionBase InConnection,
        PWNotificationCacheBase<T>.SpawnReqDelegate InSpawner)
        : base(InConnection, InSpawner, (PWBatchQueue<T, PWNotificationCacheTemporal<T>.PWNotification>) new PWNotificationCacheTemporal<T>.PWNotificationQueue())
      {
      }
    }
  }
}
