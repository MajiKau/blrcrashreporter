﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Caches.PWPremiumMatchCache
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries;
using PWAPICommon.Wrappers;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Caches
{
  [ComVisible(true)]
  public class PWPremiumMatchCache : PWCacheBase
  {
    public List<PremiumMatchItem> CachedMatches;

    public PWPremiumMatchCache(PWServerBase InServer, PWConnectionBase InConnection)
      : base(InServer, InConnection)
      => this.CachedMatches = new List<PremiumMatchItem>(100);

    protected override PWRequestBase CreateRefreshRequest() => (PWRequestBase) new PWSqlItemQueryAllReq<PremiumMatchItem>(this.LocalConnection);

    protected override bool ParseCacheRefreshResults(PWRequestBase Wrapper)
    {
      PWSqlItemQueryAllReq<PremiumMatchItem> sqlItemQueryAllReq = Wrapper as PWSqlItemQueryAllReq<PremiumMatchItem>;
      if (sqlItemQueryAllReq.ResponseItems != null)
        this.CachedMatches = sqlItemQueryAllReq.ResponseItems;
      return this.CachedMatches != null;
    }

    public bool RemoveExpiredMatch(PremiumMatchItem InMatch)
    {
      this.CachedMatches.Remove(InMatch);
      PWSqlItemRemoveReq<PremiumMatchItem> sqlItemRemoveReq = new PWSqlItemRemoveReq<PremiumMatchItem>(this.LocalConnection);
      sqlItemRemoveReq.Item = InMatch;
      return sqlItemRemoveReq.SubmitServerQuery();
    }

    public override Dictionary<string, string> CheckHealth() => new Dictionary<string, string>()
    {
      ["PMCache:Status"] = "OK",
      ["PMCache:Count"] = this.CachedMatches.Count.ToString()
    };
  }
}
