﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Caches.PWPlayerItemCache`1
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Queries;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Timers;

namespace PWAPICommon.Caches
{
  [ComVisible(true)]
  public class PWPlayerItemCache<T> : PWCacheBase
  {
    private ConcurrentDictionary<long, CachedItem<T>> CachedItems;
    private TimeSpan CacheLifetime;
    private string CacheName;
    private Timer PurgeTimer;
    private TimeSpan PurgeTime;

    public PWPlayerItemCache(string InName, PWServerBase InServer, TimeSpan InCacheTime)
      : base(InServer, (PWConnectionBase) null)
    {
      this.CacheName = InName;
      this.CachedItems = new ConcurrentDictionary<long, CachedItem<T>>();
      this.PurgeTimer = new Timer(60000.0);
      this.PurgeTimer.Elapsed += new ElapsedEventHandler(this.OnPurgeTimer);
      this.PurgeTimer.Start();
      this.CacheLifetime = InCacheTime;
      this.PurgeTime = InCacheTime.Add(InCacheTime);
    }

    public T GetCachedItem(long ForUserID)
    {
      CachedItem<T> cachedItem = (CachedItem<T>) null;
      return this.CachedItems.TryGetValue(ForUserID, out cachedItem) ? cachedItem.Item : default (T);
    }

    public bool ContainsKey(long ForUserID) => this.CachedItems.ContainsKey(ForUserID);

    public bool ContainsValue(T InValue) => this.CachedItems.Values.Contains(new CachedItem<T>(this.CacheLifetime, InValue));

    public void SetCachedItem(long ForUserID, T InValue)
    {
      if (this.CachedItems.ContainsKey(ForUserID))
      {
        this.CachedItems[ForUserID].Item = InValue;
      }
      else
      {
        CachedItem<T> cachedItem = new CachedItem<T>(this.CacheLifetime, InValue);
        this.CachedItems.TryAdd(ForUserID, cachedItem);
      }
    }

    public override void MarkDirty()
    {
      base.MarkDirty();
      this.CachedItems.Clear();
    }

    public void MarkDirtyFor(long ForUserID)
    {
      if (!this.CachedItems.ContainsKey(ForUserID))
        return;
      this.CachedItems[ForUserID].Dirty = true;
    }

    protected override bool ParseCacheRefreshResults(PWRequestBase Wrapper) => throw new NotImplementedException();

    protected override PWRequestBase CreateRefreshRequest() => (PWRequestBase) null;

    public override Dictionary<string, string> CheckHealth() => new Dictionary<string, string>()
    {
      [this.CacheName + ":Status"] = "OK",
      [this.CacheName + ":Count"] = this.CachedItems.Count.ToString()
    };

    private void OnPurgeTimer(object Source, ElapsedEventArgs args) => this.CachedItems.RemoveAll<long, CachedItem<T>>((Func<CachedItem<T>, bool>) (x => x.Age > this.PurgeTime));
  }
}
