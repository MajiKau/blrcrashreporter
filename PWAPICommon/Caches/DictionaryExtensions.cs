﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Caches.DictionaryExtensions
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace PWAPICommon.Caches
{
  [ComVisible(true)]
  public static class DictionaryExtensions
  {
    public static void RemoveAll<TKey, TValue>(
      this IDictionary<TKey, TValue> dic,
      Func<TValue, bool> predicate)
    {
      foreach (TKey key in dic.Keys.Where<TKey>((Func<TKey, bool>) (k => predicate(dic[k]))).ToList<TKey>())
        dic.Remove(key);
    }

    public static void RemoveAll<TKey, TValue>(
      this ConcurrentDictionary<TKey, TValue> dic,
      Func<TValue, bool> predicate)
    {
      foreach (TKey key in dic.Keys.Where<TKey>((Func<TKey, bool>) (k => predicate(dic[k]))).ToList<TKey>())
      {
        TValue obj = default (TValue);
        dic.TryRemove(key, out obj);
      }
    }

    public static string PrintHttpTable<TKey, TValue>(this IDictionary<TKey, TValue> dic)
    {
      string str = "" + "<table border=\"0\">" + "<tr><td><b>Key</b></td><td><b>Value</b></td></tr>";
      foreach (KeyValuePair<TKey, TValue> keyValuePair in (IEnumerable<KeyValuePair<TKey, TValue>>) dic)
        str = str + "<tr><td>" + keyValuePair.Key.ToString() + "</td><td>" + keyValuePair.Value.ToString() + "</td></tr>";
      return str + "</table></p>";
    }
  }
}
