﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Caches.PWFileCache
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Resources;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Caches
{
  [ComVisible(true)]
  public class PWFileCache : IPWResource
  {
    private PWServerBase OwningServer;
    private ConcurrentDictionary<string, CachedItem<string>> CachedFiles;
    private TimeSpan FileCacheTime;

    public PWFileCache(PWServerBase InOwningServer)
    {
      this.OwningServer = InOwningServer;
      this.CachedFiles = new ConcurrentDictionary<string, CachedItem<string>>();
      this.FileCacheTime = new TimeSpan(0, 30, 0);
    }

    public string GetFile(string InFileName)
    {
      CachedItem<string> cachedItem = (CachedItem<string>) null;
      return this.CachedFiles.TryGetValue(InFileName, out cachedItem) && !cachedItem.Dirty ? cachedItem.Item : (string) null;
    }

    public void MarkDirty()
    {
      foreach (KeyValuePair<string, CachedItem<string>> cachedFile in this.CachedFiles)
        cachedFile.Value.Dirty = true;
    }

    public bool SetFile(string InFileName, string InFileData)
    {
      this.CachedFiles[InFileName] = new CachedItem<string>(this.FileCacheTime, InFileData);
      return true;
    }

    public virtual void StartResource()
    {
    }

    public virtual void StopResource()
    {
    }

    public Dictionary<string, string> CheckHealth()
    {
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      foreach (KeyValuePair<string, CachedItem<string>> cachedFile in this.CachedFiles)
      {
        dictionary["FileCache:" + cachedFile.Key + ":AGE"] = cachedFile.Value.Age.ToString();
        dictionary["FileCache:" + cachedFile.Key + ":SIZE"] = cachedFile.Value.Item != null ? cachedFile.Value.Item.Length.ToString() : "0";
      }
      return dictionary;
    }
  }
}
