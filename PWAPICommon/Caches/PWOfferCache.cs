﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Caches.PWOfferCache
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries;
using PWAPICommon.Queries.Mail;
using PWAPICommon.Queries.Offers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace PWAPICommon.Caches
{
  [ComVisible(true)]
  public class PWOfferCache : PWCacheBase
  {
    private List<PWOfferItem> CachedOffers;
    private List<PWMailItem> CachedNews;
    private static readonly EOfferType[] ClientOfferTypes = new EOfferType[6]
    {
      EOfferType.EOT_Discount,
      EOfferType.EOT_Featured,
      EOfferType.EOT_Popular,
      EOfferType.EOT_AARAdvertise,
      EOfferType.EOT_DealOfTheDay,
      EOfferType.EOT_EventItem
    };

    protected TimeSpan DefaultReadTimeSpan => new TimeSpan(1, 0, 0);

    public PWOfferCache(PWServerBase InServer, PWConnectionBase InConnection)
      : base(InServer, InConnection)
    {
    }

    protected override PWRequestBase CreateRefreshRequest()
    {
      this.Log("Offer Cache out of date, Refreshing...", ELoggingLevel.ELL_Informative);
      return (PWRequestBase) new PWOfferQueryReq();
    }

    public void GenerateMessageData(int UserType, string TagSet)
    {
      PWStoreCache resource = this.OwningServer.GetResource<PWStoreCache>();
      if (resource == null)
        return;
      TimeSpan NextReadTimeSpan;
      this.ProcessCachedOffers((Action<PWOfferItem>) null, (IEnumerable<PWOfferItem>) this.GetCachedItems(), out NextReadTimeSpan);
      this.NextReadTimeSpan = NextReadTimeSpan;
      resource.GenerateMessageData(UserType, TagSet);
      this.Log("Offer Cache Refresh Time is " + NextReadTimeSpan.ToString(), ELoggingLevel.ELL_Informative);
    }

    protected bool HasOfferStarted(PWOfferItem Offer, out TimeSpan TimeLeft)
    {
      TimeLeft = Offer.StartDate.Subtract(PWServerBase.CurrentTime);
      return Offer.StartDate < PWServerBase.CurrentTime;
    }

    protected bool HasOfferEnded(PWOfferItem Offer, out TimeSpan TimeLeft)
    {
      TimeLeft = Offer.EndDate.Subtract(PWServerBase.CurrentTime);
      return Offer.EndDate < PWServerBase.CurrentTime;
    }

    public void ProcessEmbeddedOffers(PWStoreInfo StoreInfo, out TimeSpan NextReadTimeSpan)
    {
      NextReadTimeSpan = this.DefaultReadTimeSpan;
      if (StoreInfo == null || StoreInfo.StoreItems == null)
        return;
      IEnumerable<PWStoreOfferItem> ClientOffers;
      this.ProcessCachedOffers((Action<PWOfferItem>) (Offer =>
      {
        switch (Offer.OfferTypeEnum)
        {
          case EOfferType.EOT_Discount:
          case EOfferType.EOT_DealOfTheDay:
            StoreItem storeItem = ((IEnumerable<StoreItem>) StoreInfo.StoreItems).FirstOrDefault<StoreItem>((Func<StoreItem, bool>) (x => x.ItemGuid == Offer.TargetItem));
            if (!(storeItem != (StoreItem) null))
              break;
            storeItem.Discount = Offer.Discount;
            break;
        }
      }), (IEnumerable<PWOfferItem>) this.GetCachedItems(), out ClientOffers, out NextReadTimeSpan);
      StoreInfo.OfferItems = ClientOffers.ToArray<PWStoreOfferItem>();
    }

    private void ProcessNewsOffers()
    {
      this.CachedNews = new List<PWMailItem>();
      this.ProcessCachedOffers((Func<PWOfferItem, bool>) (x => x.OfferTypeEnum == EOfferType.EOT_NewsItem), (Action<PWOfferItem>) (Offer =>
      {
        if (Offer.OfferTypeEnum != EOfferType.EOT_NewsItem)
          return;
        MailOfferData mailDataFrom = this.GetMailDataFrom(Offer);
        if (mailDataFrom != null)
          this.CachedNews.Add(this.GenerateMailItem(PWServerBase.GlobalUserID, PWServerBase.GlobalUserID, "", Offer.TargetItem, mailDataFrom, Offer.StartDate));
        else
          this.Log("Missing Offer Data [" + Offer.OfferTypeEnum.ToString() + ", " + Offer.UniqueID.ToString() + "]", ELoggingLevel.ELL_Errors);
      }), (IEnumerable<PWOfferItem>) this.GetCachedItems(), out TimeSpan _);
      new PWMailNotificationReq(this.LocalConnection, this.CachedNews).SubmitServerQuery();
    }

    private void ProcessCachedOffers(
      Action<PWOfferItem> Processor,
      IEnumerable<PWOfferItem> Offers,
      out TimeSpan NextReadTimeSpan)
    {
      this.ProcessCachedOffers((Func<PWOfferItem, bool>) null, Processor, Offers, out NextReadTimeSpan);
    }

    private void ProcessCachedOffers(
      Func<PWOfferItem, bool> Selector,
      Action<PWOfferItem> Processor,
      IEnumerable<PWOfferItem> Offers,
      out TimeSpan NextReadTimeSpan)
    {
      NextReadTimeSpan = this.DefaultReadTimeSpan;
      foreach (PWOfferItem Offer in (Offers ?? (IEnumerable<PWOfferItem>) new List<PWOfferItem>()).Where<PWOfferItem>(Selector ?? (Func<PWOfferItem, bool>) (x => true)))
      {
        TimeSpan TimeLeft;
        if (!this.HasOfferEnded(Offer, out TimeLeft))
        {
          if (TimeLeft < NextReadTimeSpan)
            NextReadTimeSpan = TimeLeft;
          if (!this.HasOfferStarted(Offer, out TimeLeft))
          {
            if (TimeLeft < NextReadTimeSpan)
              NextReadTimeSpan = TimeLeft;
          }
          else if (Processor != null)
            Processor(Offer);
        }
      }
    }

    private void ProcessCachedOffers(
      Action<PWOfferItem> Processor,
      IEnumerable<PWOfferItem> Offers,
      out IEnumerable<PWStoreOfferItem> ClientOffers,
      out TimeSpan NextReadTimeSpan)
    {
      this.ProcessCachedOffers((Func<PWOfferItem, bool>) null, Processor, Offers, out ClientOffers, out NextReadTimeSpan);
    }

    private void ProcessCachedOffers(
      Func<PWOfferItem, bool> Selector,
      Action<PWOfferItem> Processor,
      IEnumerable<PWOfferItem> Offers,
      out IEnumerable<PWStoreOfferItem> ClientOffers,
      out TimeSpan NextReadTimeSpan)
    {
      List<PWStoreOfferItem> pwStoreOfferItemList = new List<PWStoreOfferItem>();
      NextReadTimeSpan = this.DefaultReadTimeSpan;
      using (IEnumerator<PWOfferItem> enumerator = (Offers ?? (IEnumerable<PWOfferItem>) new List<PWOfferItem>()).Where<PWOfferItem>(Selector ?? (Func<PWOfferItem, bool>) (x => true)).GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          PWOfferItem Offer = enumerator.Current;
          TimeSpan TimeLeft;
          if (!this.HasOfferEnded(Offer, out TimeLeft))
          {
            if (TimeLeft < NextReadTimeSpan)
              NextReadTimeSpan = TimeLeft;
            if (!this.HasOfferStarted(Offer, out TimeLeft))
            {
              if (TimeLeft < NextReadTimeSpan)
                NextReadTimeSpan = TimeLeft;
            }
            else if (Processor != null)
              Processor(Offer);
            if (((IEnumerable<EOfferType>) PWOfferCache.ClientOfferTypes).Any<EOfferType>((Func<EOfferType, bool>) (x => x == Offer.OfferTypeEnum)))
              pwStoreOfferItemList.Add(new PWStoreOfferItem(Offer));
          }
        }
      }
      ClientOffers = (IEnumerable<PWStoreOfferItem>) pwStoreOfferItemList;
    }

    public List<PWOfferItem> GetCachedItems()
    {
      this.ConditionalRefreshCache();
      return this.CachedOffers;
    }

    public List<PWMailItem> GetCachedNewsItems()
    {
      this.ConditionalRefreshCache();
      return this.CachedNews;
    }

    protected override bool ParseCacheRefreshResults(PWRequestBase Wrapper)
    {
      this.CachedOffers = (Wrapper as PWOfferQueryReq).Offers;
      this.ProcessNewsOffers();
      this.GenerateMessageData(0, "");
      return true;
    }

    private void SendStoreItemToInventory(
      long UserID,
      PWConnectionBase UserConnection,
      Guid StoreItemGuid,
      EPurchaseLength Duration,
      int Quantity,
      bool bActivate)
    {
      new PWGiftItemReq(UserConnection != null ? UserConnection : this.LocalConnection)
      {
        UserID = UserID,
        bUserConnection = (UserConnection != null),
        PurchaseLength = Duration,
        Count = Math.Max(Quantity, 1),
        bAutoActivate = bActivate,
        StoreItemGuid = StoreItemGuid
      }.SubmitServerQuery();
    }

    private void SendStoreItemToInventory(
      long UserID,
      Guid StoreItemGuid,
      EPurchaseLength Duration,
      int Quantity,
      bool bAutoActivate)
    {
      this.SendStoreItemToInventory(UserID, (PWConnectionBase) null, StoreItemGuid, Duration, Quantity, bAutoActivate);
    }

    private void SendStoreItemToMail(
      long UserID,
      PWConnectionBase UserConnection,
      Guid StoreItemGuid,
      string MailSubject,
      string MailBody)
    {
      this.SendStoreItemToMail(UserID, UserConnection, StoreItemGuid, new MailOfferData()
      {
        MailSubject = MailSubject,
        MailBody = new string[1]{ MailBody }
      });
    }

    private void SendStoreItemToMail(
      long UserID,
      PWConnectionBase UserConnection,
      Guid StoreItemGuid,
      MailOfferData MailData)
    {
      PWMailItem mailItem = this.GenerateMailItem(UserID, PWServerBase.AdminUserID, PWServerBase.AdminName, StoreItemGuid, MailData, PWServerBase.CurrentTime);
      if (mailItem == null)
        return;
      mailItem.GPAmount = 0;
      mailItem.Duration = (byte) MailData.Duration;
      PWMailCreateReq pwMailCreateReq = new PWMailCreateReq(UserConnection != null ? UserConnection : this.LocalConnection);
      pwMailCreateReq.Item = mailItem;
      pwMailCreateReq.ItemsToSend.Add(StoreItemGuid);
      pwMailCreateReq.SubmitServerQuery();
    }

    private PWMailItem GenerateMailItem(
      long RecipientID,
      long SenderID,
      string SenderName,
      Guid StoreItemGuid,
      string MailSubject,
      string MailBody,
      DateTime TimeSent)
    {
      return new PWMailItem()
      {
        SenderID = SenderID,
        SenderName = SenderName,
        RecipientID = RecipientID,
        Subject = MailSubject,
        Message = MailBody,
        TimeSent = TimeSent,
        ItemA = StoreItemGuid
      };
    }

    private PWMailItem GenerateMailItem(
      long RecipientID,
      long SenderID,
      string SenderName,
      Guid StoreItemGuid,
      MailOfferData MailData,
      DateTime TimeSent)
    {
      return MailData != null ? this.GenerateMailItem(RecipientID, SenderID, SenderName, StoreItemGuid, MailData.MailSubject, string.Join(",", MailData.MailBody ?? new string[0]), TimeSent) : (PWMailItem) null;
    }

    private IList<PWOfferItem> SelectOffers(
      byte OfferType,
      OfferDataBase OfferData,
      IList<PWOfferItem> AllOffers)
    {
      IList<PWOfferItem> pwOfferItemList = (IList<PWOfferItem>) null;
      if (OfferData != null && AllOffers != null)
      {
        pwOfferItemList = (IList<PWOfferItem>) new List<PWOfferItem>(AllOffers.Count);
        if (OfferType == (byte) 0)
        {
          foreach (PWOfferItem allOffer in (IEnumerable<PWOfferItem>) AllOffers)
          {
            if (allOffer.EndDate > PWServerBase.CurrentTime && allOffer.StartDate < PWServerBase.CurrentTime)
            {
              OfferDataBase customData = allOffer.GetCustomData<OfferDataBase>();
              if (customData != null && customData.Matches((object) OfferData))
                pwOfferItemList.Add(allOffer);
            }
          }
        }
        else
        {
          foreach (PWOfferItem allOffer in (IEnumerable<PWOfferItem>) AllOffers)
          {
            if ((int) allOffer.OfferType == (int) OfferType && allOffer.EndDate > PWServerBase.CurrentTime && allOffer.StartDate < PWServerBase.CurrentTime)
            {
              OfferDataBase customData = allOffer.GetCustomData<OfferDataBase>();
              if (customData != null && customData.Matches((object) OfferData))
                pwOfferItemList.Add(allOffer);
            }
          }
        }
      }
      return pwOfferItemList;
    }

    public bool AwardMatchingOffers(
      long TargetUser,
      OfferDataBase InputData,
      PWConnectionBase UserConnection)
    {
      return this.AwardMatchingOffers(TargetUser, EOfferType.EOT_None, InputData, UserConnection);
    }

    public bool AwardMatchingOffers(
      long TargetUser,
      EOfferType OfferType,
      OfferDataBase InputData,
      PWConnectionBase UserConnection)
    {
      foreach (PWOfferItem Offer in (IEnumerable<PWOfferItem>) (this.SelectOffers((byte) OfferType, InputData, (IList<PWOfferItem>) this.CachedOffers) ?? (IList<PWOfferItem>) new PWOfferItem[0]))
      {
        MailOfferData mailDataFrom = this.GetMailDataFrom(Offer);
        if (mailDataFrom != null)
        {
          this.SendStoreItemToMail(TargetUser, UserConnection, Offer.TargetItem, mailDataFrom);
        }
        else
        {
          OfferDataBase customData = Offer.GetCustomData<OfferDataBase>();
          this.SendStoreItemToInventory(TargetUser, UserConnection, Offer.TargetItem, customData.Duration, customData.Quantity, customData.bAutoActivate);
        }
      }
      return true;
    }

    public MailOfferData GetMailDataFrom(PWOfferItem Offer)
    {
      MailOfferData mailOfferData = (MailOfferData) null;
      ProfileFlagData customData1 = Offer.GetCustomData<ProfileFlagData>();
      if (customData1 != null && customData1.bSendToMail)
      {
        mailOfferData = new MailOfferData();
        mailOfferData.MailSubject = customData1.MailData.MailSubject;
        mailOfferData.MailBody = customData1.MailData.MailBody;
        mailOfferData.Duration = customData1.Duration;
      }
      ExperienceThresholdData customData2 = Offer.GetCustomData<ExperienceThresholdData>();
      if (customData2 != null)
      {
        mailOfferData = new MailOfferData();
        mailOfferData.MailSubject = customData2.MailSubject;
        mailOfferData.MailBody = new string[1]
        {
          customData2.MailBody
        };
        mailOfferData.Duration = customData2.Duration;
      }
      MailOfferData customData3 = Offer.GetCustomData<MailOfferData>();
      if (customData3 != null)
        mailOfferData = customData3;
      return mailOfferData;
    }

    public override Dictionary<string, string> CheckHealth() => new Dictionary<string, string>();
  }
}
