﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Party.PartyDisbandReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;

namespace PWAPICommon.Queries.Party
{
  internal class PartyDisbandReq : PWSocialReqBase
  {
    public PartyDisbandReq()
      : base((PWConnectionBase) null)
    {
    }

    public PartyDisbandReq(PWConnectionBase InClient)
      : base(InClient)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_PartyDisbanded;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.SocialServer != null)
      {
        SocialLoginData clientData = this.ClientData;
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => true;

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
