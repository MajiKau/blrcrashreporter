﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Party.PartyInviteRespondReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Party
{
  [ComVisible(true)]
  public class PartyInviteRespondReq : PWSocialReqBase
  {
    public bool bAccept;
    private EPartyError ErrorCode;

    public PartyInviteRespondReq()
      : base((PWConnectionBase) null)
    {
    }

    public PartyInviteRespondReq(PWConnectionBase InClient)
      : base(InClient)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_PartyInviteResponse;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      int result;
      if (strArray.Length != 1 || !int.TryParse(strArray[0], out result))
        return false;
      this.bAccept = Convert.ToBoolean(result);
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.ErrorCode = EPartyError.EPE_UnknownError;
      if (this.SocialServer != null && this.ClientData != null)
        this.ErrorCode = !this.bAccept ? this.SocialServer.DenyPartyInvite(this.ClientData) : this.SocialServer.AcceptPartyInvite(this.ClientData);
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.ErrorCode == EPartyError.EPE_Ok;

    protected override bool ProcessServerResult()
    {
      this.SendMessage(this.CompressMessage(this.SerializeMessage(Convert.ToByte((object) this.ErrorCode).ToString())));
      if (this.ErrorCode == EPartyError.EPE_Ok)
        this.SocialServer.PushUpdatedPlayerToParty(this.ClientData);
      return true;
    }
  }
}
