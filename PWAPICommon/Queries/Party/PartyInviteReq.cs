﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Party.PartyInviteReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Party
{
  [ComVisible(true)]
  public class PartyInviteReq : PWSocialReqBase
  {
    private long InviteId;
    private EPartyError ErrorType;
    private bool bPartyCreate;

    public PartyInviteReq()
      : base((PWConnectionBase) null)
    {
    }

    public PartyInviteReq(PWConnectionBase InClient)
      : base(InClient)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_InviteToParty;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 1 && long.TryParse(strArray[0], out this.InviteId) && this.InviteId > 0L;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.ErrorType = EPartyError.EPE_UnknownError;
      if (this.SocialServer != null && this.ClientData != null)
      {
        this.bPartyCreate = this.ClientData.Party == null;
        this.ErrorType = !this.bPartyCreate ? this.SocialServer.InvitePendingPartyMember(this.ClientData, this.InviteId) : this.SocialServer.CreateParty(this.ClientData, this.InviteId);
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.ErrorType == EPartyError.EPE_Ok;

    protected override bool ProcessServerResult()
    {
      SocialLoginData loginData1 = this.SocialServer.GetLoginData(this.Connection);
      SocialLoginData loginData2 = this.SocialServer.GetLoginData(this.InviteId);
      this.SendMessage(this.CompressMessage(this.SerializeMessage(Convert.ToByte((object) this.ErrorType).ToString())));
      if (this.ErrorType == EPartyError.EPE_Ok)
      {
        if (this.bPartyCreate)
          this.PartyCreated();
        else
          this.NewMemberAdded();
        if (loginData1 != null && loginData1.PlayerInfo != null & loginData2 != null)
          this.SocialServer.SendMessageToClient(EMessageType.EMT_PartyInviteReceive, "T:" + loginData1.PlayerInfo.UserName, loginData2.Connection);
      }
      return true;
    }

    private void PartyCreated()
    {
      foreach (PartyMember member in this.ClientData.Party.MemberList)
        this.PushPartyInfoToPlayer(member.LoginInfo.Connection);
    }

    private void NewMemberAdded()
    {
      List<PartyMember> memberList = this.ClientData.Party.MemberList;
      PartyMember NewMember = memberList[memberList.Count - 1];
      this.PushPartyInfoToPlayer(NewMember.LoginInfo.Connection);
      this.PushUpdatedPlayerToParty(NewMember);
    }

    private void PushPartyInfoToPlayer(PWConnectionBase InConnection)
    {
      List<PartyMember> memberList = this.ClientData.Party.MemberList;
      new PartyListUpdateReq(InConnection)
      {
        MembersToUpdate = memberList
      }.SubmitServerQuery();
    }

    private void PushUpdatedPlayerToParty(PartyMember NewMember)
    {
      List<PartyMember> memberList = this.ClientData.Party.MemberList;
      for (int index = 0; index < memberList.Count - 1; ++index)
        new PartyListUpdateReq(memberList[index].LoginInfo.Connection)
        {
          MembersToUpdate = {
            NewMember
          }
        }.SubmitServerQuery();
    }
  }
}
