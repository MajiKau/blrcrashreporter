﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Mail.PWMailMarkRead
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Wrappers;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Mail
{
  [ComVisible(true)]
  public class PWMailMarkRead : PWMailReqBase
  {
    private PWSqlItemUpdateReq<PWMailItem> MailUpdate;
    private PWMailQueryUserReq UserMail;
    private PWMailItem FoundItem;

    public PWMailMarkRead()
      : base((PWConnectionBase) null)
    {
    }

    public PWMailMarkRead(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_MailMarkRead;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      this.Item = new PWMailItem();
      return strArray.Length == 2 && long.TryParse(strArray[0], out this.Item.RecipientID) && Guid.TryParse(strArray[1], out this.Item.UniqueID);
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.UserMail == null)
      {
        this.UserMail = new PWMailQueryUserReq(this.Connection);
        this.UserMail.UserID = this.Item.RecipientID;
        this.UserMail.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.UserMail.SubmitServerQuery();
      }
      if (this.UserMail.Successful && this.FoundItem == null)
      {
        this.FoundItem = this.UserMail.Items.Find((Predicate<PWMailItem>) (x => x.UniqueID == this.Item.UniqueID));
        if (this.FoundItem != null && !this.FoundItem.Read)
        {
          this.FoundItem.Read = true;
          this.MailUpdate = new PWSqlItemUpdateReq<PWMailItem>(this.Connection);
          this.MailUpdate.Item = this.FoundItem;
          this.MailUpdate.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
          return this.MailUpdate.SubmitServerQuery();
        }
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.MailUpdate != null && this.MailUpdate.Successful;

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
