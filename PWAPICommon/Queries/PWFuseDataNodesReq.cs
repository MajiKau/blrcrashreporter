﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.PWFuseDataNodesReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.Currency;
using PWAPICommon.Queries.Inventory;
using PWAPICommon.Queries.Store;
using PWAPICommon.Wrappers;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries
{
  [ComVisible(true)]
  public class PWFuseDataNodesReq : FuseInventoryHandlerReqBase
  {
    private const int DataNodeIDOffsetStart = 2000;
    private const int DataNodeNumQualityLevels = 5;
    private const int DatanodeTypeOffsets = 10;
    private const int DataNodeNumDurabilityLevels = 5;
    private PWStoreQueryUserReq StoreReq;
    private PWSqlItemAddReq<PWInventoryItem> InvAddReq;
    private StoreItem StoreItemToAdd;
    private PWInventoryItem ItemToAdd;
    private StoreItem ReinforcementStoreItem;
    private Guid ReinforcementItemGuid;
    private PWInventoryItem BestDataNode;
    private int DataNodeAValue;
    private int DataNodeBValue;
    private int ReinforcementExtraChance;
    private int NumNodesToBeDeleted;

    public PWFuseDataNodesReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWFuseDataNodesReq(PWConnectionBase InClient)
      : base(InClient)
    {
    }

    public PWFuseDataNodesReq(
      PWConnectionBase InConnection,
      long UserId,
      PWInventoryItem InvItemA,
      PWInventoryItem InvItemB,
      string AdditionalFusionData)
      : base(InConnection, UserId, InvItemA, InvItemB, AdditionalFusionData)
    {
      Guid.TryParse(AdditionalFusionData, out this.ReinforcementItemGuid);
      this.Log("Begin fusion with InvItemA " + InvItemA.InstanceId.ToString() + " and InvItemB " + InvItemB.InstanceId.ToString(), false, ELoggingLevel.ELL_Informative);
    }

    public override bool ParseServerQuery(byte[] InMessage) => true;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.StoreReq = new PWStoreQueryUserReq(this.Connection);
      this.StoreReq.UserId = this.ForUserID;
      this.StoreReq.UserType = (byte) 0;
      this.StoreReq.Filter = "";
      this.StoreReq.ServerResultProcessor = new ProcessDelegate(this.OnStoreDataRead);
      this.Log("Fuse data node. Reading store data", false, ELoggingLevel.ELL_Informative);
      return this.StoreReq.SubmitServerQuery();
    }

    private bool OnStoreDataRead(PWRequestBase ForRequest)
    {
      this.Log("Fuse data node. Store data read", false, ELoggingLevel.ELL_Informative);
      this.ReinforcementExtraChance = 0;
      this.ReinforcementStoreItem = this.StoreReq.Items.Find((Predicate<StoreItem>) (x => x.ItemGuid == this.ReinforcementItemGuid));
      if (this.ReinforcementStoreItem != (StoreItem) null)
      {
        this.ReinforcementExtraChance = this.ReinforcementStoreItem.MaxOwned;
        int price1 = this.ReinforcementStoreItem.GetPrice(EPurchaseLength.EPL_Perm, ECurrencyType.EC_GP);
        int price2 = this.ReinforcementStoreItem.GetPrice(EPurchaseLength.EPL_Perm, ECurrencyType.EC_ZP);
        if (price1 > 0)
        {
          PWTransactionAddReq transactionAddReq = new PWTransactionAddReq(this.Connection);
          transactionAddReq.Item = new PWTransaction(this.ForUserID, ETransactionType.ETT_PurchaseGP, this.ReinforcementItemGuid);
          transactionAddReq.SubmitServerQuery();
          PWGPRemoveReq pwgpRemoveReq = new PWGPRemoveReq(this.Connection);
          pwgpRemoveReq.UserId = this.ForUserID;
          pwgpRemoveReq.Amount = price1;
          pwgpRemoveReq.ServerResultProcessor = new ProcessDelegate(this.OnFundsRemoved);
          return pwgpRemoveReq.SubmitServerQuery();
        }
        if (price2 > 0)
        {
          PWTransactionAddReq transactionAddReq = new PWTransactionAddReq(this.Connection);
          transactionAddReq.Item = new PWTransaction(this.ForUserID, ETransactionType.ETT_PurchaseZP, this.ReinforcementItemGuid);
          transactionAddReq.SubmitServerQuery();
          PWZPRemoveReq pwzpRemoveReq = new PWZPRemoveReq(this.Connection);
          pwzpRemoveReq.userid = this.ForUserID;
          pwzpRemoveReq.amount = price2;
          pwzpRemoveReq.count = 1;
          pwzpRemoveReq.server = this.Connection.OwningServer.PWServerID;
          pwzpRemoveReq.game = this.Connection.OwningServer.PWGameID;
          pwzpRemoveReq.itemid = this.ReinforcementStoreItem.ItemId;
          pwzpRemoveReq.uniqueid = "";
          pwzpRemoveReq.charid = 0;
          pwzpRemoveReq.ServerResultProcessor = new ProcessDelegate(this.OnFundsRemoved);
          return pwzpRemoveReq.SubmitServerQuery();
        }
        this.OnFundsQueried((PWRequestBase) null);
        return true;
      }
      this.Log("Failed to find fuse reinforcement store item for guid " + this.ReinforcementItemGuid.ToString(), false, ELoggingLevel.ELL_Verbose);
      return false;
    }

    private bool OnTransactionFinished(PWRequestBase ForRequest) => true;

    private bool OnFundsRemoved(PWRequestBase ForRequest)
    {
      this.Log("Fuse data node. funds removed. Now gonna re-query funds", false, ELoggingLevel.ELL_Informative);
      PWQueryAllFundReq pwQueryAllFundReq = new PWQueryAllFundReq(this.Connection, this.ForUserID);
      pwQueryAllFundReq.ServerResultProcessor = new ProcessDelegate(this.OnFundsQueried);
      pwQueryAllFundReq.ForceProcessResult = true;
      return pwQueryAllFundReq.SubmitServerQuery();
    }

    private bool OnFundsQueried(PWRequestBase ForRequest)
    {
      this.Log("Funds have been re-queried starting fusion now", false, ELoggingLevel.ELL_Informative);
      this.TryNodeFusion(this.ReinforcementExtraChance);
      return true;
    }

    private void TryNodeFusion(int ReinforcementChance)
    {
      int num1 = ReinforcementChance + this.GetFusionSuccessChance();
      int num2 = PWRandom.Next(100);
      this.BestDataNode = this.DataNodeAValue <= this.DataNodeBValue ? (this.DataNodeBValue <= this.DataNodeAValue ? (PWRandom.Next(2) == 0 ? this.ItemA : this.ItemB) : this.ItemB) : this.ItemA;
      if (num1 >= num2)
        this.FuseSuccess();
      else
        this.FuseFailure();
    }

    private void FuseSuccess()
    {
      this.NumNodesToBeDeleted = 2;
      this.Log("Fusedatanode SUCCESS", false, ELoggingLevel.ELL_Informative);
      int num1 = this.BestDataNode == this.ItemA ? this.DataNodeAValue + 1 : this.DataNodeBValue + 1;
      int num2 = num1 / 5;
      int num3 = num2 > 4 ? 4 : num2;
      int num4 = num1 - num3 * 5;
      int num5 = num4 > 4 ? 4 : num4;
      EPurchaseLength PurchaseLength = (EPurchaseLength) num5;
      int nodeType = this.GetNodeType(this.BestDataNode.ItemId);
      int FinalNodeId = 2000 + nodeType * 10 + num3;
      this.Log("New Fused node type is " + nodeType.ToString() + " and quality is " + num3.ToString() + " and durability is " + num5.ToString() + " and item id is " + FinalNodeId.ToString(), false, ELoggingLevel.ELL_Informative);
      this.StoreItemToAdd = this.StoreReq.Items.Find((Predicate<StoreItem>) (x => x.ItemId == FinalNodeId));
      this.Log("nodefuse charid=" + (object) this.ForUserID + " serverid=" + (object) this.Connection.OwningServer.PWServerID + " success=true reinforcement=" + (object) this.ReinforcementExtraChance + "% noderemovedid=" + (object) this.ItemA.ItemId + " noderemoveddur=" + (object) this.ItemA.PurchaseDuration + " noderemovedid=" + (object) this.ItemB.ItemId + " noderemoveddur=" + (object) this.ItemB.PurchaseDuration + " nodeaddedid=" + (object) FinalNodeId + " nodeaddeddur=" + (object) num5, true, ELoggingLevel.ELL_Verbose);
      if (this.StoreItemToAdd != (StoreItem) null)
      {
        this.ItemToAdd = this.StoreItemToAdd.GeneratePurchaseItem(this.ForUserID, PurchaseLength, 1);
        if (this.ItemToAdd != null)
        {
          this.InvAddReq = new PWSqlItemAddReq<PWInventoryItem>(this.Connection);
          this.InvAddReq.ServerResultProcessor = new ProcessDelegate(this.OnNewNodeAdded);
          this.InvAddReq.Item = this.ItemToAdd;
          this.AddChangedInventoryItem(this.ItemToAdd);
          this.ResultingInventoryGuid = this.ItemToAdd.InstanceId;
          this.InvAddReq.SubmitServerQuery();
          return;
        }
        this.Log("Failed to create Fused inventory item of id " + FinalNodeId.ToString(), false, ELoggingLevel.ELL_Informative);
      }
      else
        this.Log("Failed find store Fused item of id " + FinalNodeId.ToString(), false, ELoggingLevel.ELL_Informative);
      this.RemoveWeakerNode();
    }

    private bool OnNewNodeAdded(PWRequestBase ForRequest)
    {
      this.Log("New datanode successfully added", false, ELoggingLevel.ELL_Informative);
      this.RemoveDataNode(this.ItemA);
      return true;
    }

    private void FuseFailure()
    {
      this.Log("Fusedatanode FAIL", false, ELoggingLevel.ELL_Informative);
      this.ResultingInventoryGuid = this.BestDataNode == this.ItemA ? this.ItemB.InstanceId : this.ItemA.InstanceId;
      this.NumNodesToBeDeleted = 1;
      this.Log("nodefuse charid=" + (object) this.ForUserID + " serverid=" + (object) this.Connection.OwningServer.PWServerID + " success=false reinforcement=" + (object) this.ReinforcementExtraChance + "% noderemovedid=" + (object) (this.BestDataNode == this.ItemA ? this.ItemB.ItemId : this.ItemA.ItemId) + " noderemoveddur=" + (object) (this.BestDataNode == this.ItemA ? (int) this.ItemB.PurchaseDuration : (int) this.ItemA.PurchaseDuration), true, ELoggingLevel.ELL_Verbose);
      this.RemoveWeakerNode();
    }

    private void RemoveWeakerNode()
    {
      PWInventoryItem InvItemToRemove = this.BestDataNode == this.ItemA ? this.ItemB : this.ItemA;
      this.Log("Removing the weaker node. It is " + InvItemToRemove.InstanceId.ToString(), false, ELoggingLevel.ELL_Informative);
      this.RemoveDataNode(InvItemToRemove);
    }

    private void RemoveDataNode(PWInventoryItem InvItemToRemove)
    {
      PWInvDestroyReq pwInvDestroyReq = new PWInvDestroyReq(this.Connection);
      pwInvDestroyReq.ItemToRemove = InvItemToRemove;
      pwInvDestroyReq.ServerResultProcessor = new ProcessDelegate(this.OnDataNodeDeleted);
      pwInvDestroyReq.SubmitServerQuery();
    }

    private bool OnDataNodeDeleted(PWRequestBase ForRequest)
    {
      --this.NumNodesToBeDeleted;
      if (ForRequest is PWInvDestroyReq pwInvDestroyReq && pwInvDestroyReq.Successful)
      {
        this.Log("DataNode deleted", false, ELoggingLevel.ELL_Informative);
        this.AddChangedInventoryItem(pwInvDestroyReq.ItemToRemove);
      }
      if (this.NumNodesToBeDeleted > 0)
      {
        this.RemoveDataNode(this.ItemB);
        return true;
      }
      this.Log("Pushing changed notifications", false, ELoggingLevel.ELL_Informative);
      this.PushInventoryChanges();
      return true;
    }

    private int GetFusionSuccessChance()
    {
      int nodeQuality1 = this.GetNodeQuality(this.ItemA.ItemId);
      int nodeQuality2 = this.GetNodeQuality(this.ItemB.ItemId);
      this.DataNodeAValue = nodeQuality1 * 5 + (int) this.ItemA.PurchaseDuration;
      this.DataNodeBValue = nodeQuality2 * 5 + (int) this.ItemB.PurchaseDuration;
      return 50 - Math.Abs(this.DataNodeAValue - this.DataNodeBValue) * 10;
    }

    private int GetNodeQuality(int ItemID) => (ItemID - 2000) % 10;

    private int GetNodeType(int ItemID) => (ItemID - 2000) / 10;

    protected override EMessageType GetMessageType() => EMessageType.EMT_FuseInventory;

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.ResultingInventoryGuid != Guid.Empty;

    protected override bool ProcessServerResult()
    {
      this.Log("Processing server result for datanode fuse and the result guid is " + this.ResultingInventoryGuid.ToString(), false, ELoggingLevel.ELL_Informative);
      return this.SendDefaultMessage();
    }
  }
}
