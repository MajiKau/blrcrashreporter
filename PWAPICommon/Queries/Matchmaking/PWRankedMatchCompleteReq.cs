﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWRankedMatchCompleteReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.General;
using PWAPICommon.Queries.Offers;
using System;
using System.Collections.Generic;

namespace PWAPICommon.Queries.Matchmaking
{
  internal class PWRankedMatchCompleteReq : PWRequestBase
  {
    public IList<KeyValuePair<PWConnectionBase, long>> Players;
    private PWOfferQueryReq Offers;
    private PWOfferItem ChosenOffer;
    private PWCombinedReq Gifts;

    public PWRankedMatchCompleteReq(PWConnectionBase InConnection)
      : base(InConnection)
      => this.Players = (IList<KeyValuePair<PWConnectionBase, long>>) new List<KeyValuePair<PWConnectionBase, long>>(16);

    protected override EMessageType GetMessageType() => throw new NotImplementedException();

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.Offers == null)
      {
        this.Offers = new PWOfferQueryReq(this.Connection);
        this.Offers.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.Offers.SubmitServerQuery();
      }
      if (this.Offers.Successful && this.Offers.Offers != null)
      {
        this.ChosenOffer = this.Offers.Offers.Find((Predicate<PWOfferItem>) (x => x.OfferType == (byte) 4));
        if (this.ChosenOffer != null)
        {
          this.Gifts = new PWCombinedReq(this.Connection);
          this.Gifts.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
          foreach (KeyValuePair<PWConnectionBase, long> player in (IEnumerable<KeyValuePair<PWConnectionBase, long>>) this.Players)
            this.Gifts.AddSubRequest((PWRequestBase) new PWGiftItemReq(player.Key)
            {
              UserID = player.Value,
              StoreItemGuid = this.ChosenOffer.TargetItem,
              PurchaseLength = EPurchaseLength.EPL_Perm
            });
          return this.Gifts.SubmitServerQuery();
        }
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.Gifts != null && this.Gifts.Successful;
  }
}
