﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWMasterServerUpdateReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWMasterServerUpdateReq : PWRequestBase
  {
    public MasterServerStats InStats;
    public PWMachineID MachineId;

    public PWMasterServerUpdateReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWMasterServerUpdateReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseClientQuery(byte[] InMessage) => false;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      PWMasterServerUpdateReq masterServerUpdateReq = this.DeserializeSelf<PWMasterServerUpdateReq>(InMessage);
      if (masterServerUpdateReq == null)
        return false;
      this.InStats = masterServerUpdateReq.InStats;
      this.MachineId = masterServerUpdateReq.MachineId;
      return true;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_MasterServerUpdate;

    public override bool SubmitClientQuery()
    {
      if (this.Connection == null)
        return false;
      base.SubmitClientQuery();
      return this.SendMessage(this.SerializeSelf());
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWSuperServer resource = this.Connection.OwningServer.GetResource<PWSuperServer>();
      return resource != null && resource.UpdateMasterServer(this);
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      return this.ProcessServerResultDefault();
    }

    protected override bool ProcessServerResult() => true;

    protected override bool ProcessClientResult(PWRequestBase ForRequest) => true;
  }
}
