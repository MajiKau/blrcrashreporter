﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.ERankedJoinResponse
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public enum ERankedJoinResponse
  {
    ERJR_Success,
    ERJR_AlreadyInQueue,
    ERJR_InvalidUser,
    ERJR_InvalidIP,
    ERJR_InvalidTeamSize,
    ERJR_NoActiveSeason,
    ERJR_InternalDBErrorParam,
    ERJR_InternalDBErrorConn,
    ERJR_UnknownError,
  }
}
