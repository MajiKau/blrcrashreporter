﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWRegisterMasterServerReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWRegisterMasterServerReq : PWRequestBase
  {
    public PWMachineID MachineId;
    public int ServerID;
    public string ExternalIP;
    public string RegionTag;
    public float LatOverride;
    public float LongOverride;

    public PWRegisterMasterServerReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWRegisterMasterServerReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseClientQuery(byte[] InMessage)
    {
      this.MachineId = this.Connection.OwningServer.HardwareID;
      return true;
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      PWRegisterMasterServerReq registerMasterServerReq = this.DeserializeSelf<PWRegisterMasterServerReq>(InMessage);
      if (registerMasterServerReq == null)
        return false;
      this.MachineId = registerMasterServerReq.MachineId;
      this.ExternalIP = registerMasterServerReq.ExternalIP;
      this.RegionTag = registerMasterServerReq.RegionTag;
      this.LatOverride = registerMasterServerReq.LatOverride;
      this.LongOverride = registerMasterServerReq.LongOverride;
      return true;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_MasterServerRegister;

    public override bool SubmitClientQuery()
    {
      base.SubmitClientQuery();
      this.MachineId = this.Connection.OwningServer.HardwareID;
      return this.SendMessage(this.SerializeSelf());
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWSuperServer resource = this.Connection.OwningServer.GetResource<PWSuperServer>();
      return resource != null && resource.RegisterMasterServer(this, out this.ServerID);
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      return this.ProcessServerResultDefault();
    }

    protected override bool ProcessServerResult() => this.SendMessage(this.SerializeMessage(this.ServerID.ToString()));
  }
}
