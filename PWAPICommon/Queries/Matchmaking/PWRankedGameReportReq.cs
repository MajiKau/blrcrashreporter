﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWRankedGameReportReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Resources;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWRankedGameReportReq : PWRequestBase
  {
    private MatchReport InReport;
    private MatchReportResp OutResponse;

    public PWRankedGameReportReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWRankedGameReportReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_WriteStats;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      bool flag1 = false;
      foreach (LogFormatData logFormatData in this.DeserializeToLogData(InMessage))
      {
        switch (logFormatData.logToken)
        {
          case "match-complete":
            flag1 = this.ProcMatchComplete(logFormatData.propertyList);
            this.Log("match-complete Result: " + (flag1 ? "Yes" : "False"), false, ELoggingLevel.ELL_Verbose);
            break;
          case "final-data":
            flag1 = this.ProcFinalData(logFormatData.propertyList);
            this.Log("finaldata Result: " + (flag1 ? "Yes" : "False"), false, ELoggingLevel.ELL_Verbose);
            break;
          case "user-stats":
            flag1 = this.ProcUserStats(logFormatData.propertyList);
            this.Log("user-stats Result: " + (flag1 ? "Yes" : "False"), false, ELoggingLevel.ELL_Verbose);
            break;
        }
        if (!flag1)
          break;
      }
      bool flag2 = (this.InReport.MatchData.Winner == "blue" || this.InReport.MatchData.Winner == "red") && this.InReport.MatchData.Entries != null && this.InReport.MatchData.Entries.Count > 0;
      if (this.InReport.MatchID <= 0)
        this.Log("Invalid MatchID when reporting Ranked Match Stats " + (object) this.InReport.MatchID, false, ELoggingLevel.ELL_Errors);
      if (!flag2)
        this.Log("Failed to parse match report, data: " + this.Serializer.DeserializeToString(InMessage), false, ELoggingLevel.ELL_Errors);
      return flag2;
    }

    public IEnumerable<LogFormatData> DeserializeToLogData(byte[] InData)
    {
      string str1 = this.Serializer.DeserializeToString(InData);
      this.Log("Match Report String [" + (object) InData + "]", false, ELoggingLevel.ELL_Informative);
      string[] strArray1 = str1.Split(new char[1]{ '\n' }, StringSplitOptions.RemoveEmptyEntries);
      List<LogFormatData> logFormatDataList = new List<LogFormatData>(strArray1.Length);
      foreach (string str2 in strArray1)
      {
        char[] separator1 = new char[1]{ ' ' };
        string[] strArray2 = str2.Split(separator1, StringSplitOptions.RemoveEmptyEntries);
        LogFormatData logFormatData = new LogFormatData();
        logFormatData.propertyList = new List<KeyValuePair<string, string>>();
        foreach (string str3 in strArray2)
        {
          char[] separator2 = new char[1]{ '=' };
          string[] strArray3 = str3.Split(separator2, StringSplitOptions.RemoveEmptyEntries);
          if (strArray3.Length == 1)
          {
            this.Log("Adding Token " + strArray3[0], false, ELoggingLevel.ELL_Verbose);
            logFormatData.logToken = strArray3[0];
          }
          else if (strArray3.Length == 2)
          {
            this.Log("Adding Key " + strArray3[0] + " Value " + strArray3[1], false, ELoggingLevel.ELL_Verbose);
            logFormatData.propertyList.Add(new KeyValuePair<string, string>(strArray3[0], strArray3[1]));
          }
        }
        logFormatDataList.Add(logFormatData);
      }
      return (IEnumerable<LogFormatData>) logFormatDataList;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("Match " + (object) this.InReport.MatchID + " reporting stats", false, ELoggingLevel.ELL_Informative);
      string OutValue;
      if (!this.OwningServer.GetConfigValue<string>("MatchURL", out OutValue))
        return this.ProcessServerResultDefault();
      PWWebReq pwWebReq = new PWWebReq(this.Connection, OutValue, false, EWebRequestType.EWRT_POST, (object) this.InReport, typeof (MatchReportResp));
      pwWebReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      this.Connection.OwningServer.GetResource<PWRankedManager>()?.NotifyReportedStats(this.Connection, this);
      return pwWebReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      this.OutResponse = (MatchReportResp) ((PWWebReq) ForRequest).Result;
      return this.OutResponse.Status == "success";
    }

    protected override bool ProcessServerResult() => this.SendDefaultMessage();

    private bool ProcMatchComplete(List<KeyValuePair<string, string>> pairs)
    {
      if (pairs != null)
      {
        foreach (KeyValuePair<string, string> pair in pairs)
        {
          if (pair.Key.ToLower() == "matchid")
          {
            this.InReport.MatchID = Convert.ToInt32(pair.Value);
            return true;
          }
        }
      }
      return false;
    }

    private bool ProcFinalData(List<KeyValuePair<string, string>> pairs)
    {
      if (pairs != null)
      {
        foreach (KeyValuePair<string, string> pair in pairs)
        {
          if (pair.Key.ToLower() == "winner")
          {
            this.InReport.MatchData.Winner = pair.Value;
            return true;
          }
        }
      }
      return false;
    }

    private bool ProcUserStats(List<KeyValuePair<string, string>> pairs)
    {
      bool flag = false;
      if (pairs.Count == 4)
      {
        PlayerEntry playerEntry = new PlayerEntry();
        foreach (KeyValuePair<string, string> pair in pairs)
        {
          switch (pair.Key.ToLower())
          {
            case "userid":
              playerEntry.UserID = (long) Convert.ToInt32(pair.Value);
              continue;
            case "kills":
              playerEntry.Kills = Convert.ToInt32(pair.Value);
              continue;
            case "assists":
              playerEntry.Assists = Convert.ToInt32(pair.Value);
              continue;
            case "deaths":
              playerEntry.Deaths = Convert.ToInt32(pair.Value);
              continue;
            default:
              continue;
          }
        }
        if (playerEntry.UserID != 0L)
        {
          if (this.InReport.MatchData.Entries == null)
            this.InReport.MatchData.Entries = new List<PlayerEntry>();
          this.InReport.MatchData.Entries.Add(playerEntry);
          flag = true;
        }
      }
      return flag;
    }
  }
}
