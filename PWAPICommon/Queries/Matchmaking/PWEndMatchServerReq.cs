﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWEndMatchServerReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.General;
using PWAPICommon.Queries.Store;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWEndMatchServerReq : PWRequestBase
  {
    public List<long> VictoriousUserIds = new List<long>();
    public List<long> DefeatedUserIds = new List<long>();
    public EPurchaseType GiftPurchaseType = EPurchaseType.EPT_Activate;
    private PWCombinedReq CombinedReq;
    private static char[] Seperators = new char[1]{ ',' };

    public PWEndMatchServerReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWEndMatchServerReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_EndMatch;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage) ?? new string[0];
      if (strArray.Length != 2)
        return false;
      long result = PWServerBase.EmptyUserID;
      if (!string.IsNullOrEmpty(strArray[0]))
      {
        foreach (string s in strArray[0].Split(PWEndMatchServerReq.Seperators, StringSplitOptions.RemoveEmptyEntries) ?? new string[0])
        {
          if (long.TryParse(s, out result))
            this.VictoriousUserIds.Add(result);
        }
      }
      if (!string.IsNullOrEmpty(strArray[1]))
      {
        foreach (string s in strArray[1].Split(PWEndMatchServerReq.Seperators, StringSplitOptions.RemoveEmptyEntries) ?? new string[0])
        {
          if (long.TryParse(s, out result))
            this.DefeatedUserIds.Add(result);
        }
      }
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      PWSuperServer resource = this.Connection.OwningServer.GetResource<PWSuperServer>();
      if (resource != null && resource.GameServerEndMatch(this.Connection))
      {
        PWServerInfoRunning gameServer = resource.FindGameServer(this.Connection);
        EOfferType eofferType = EOfferType.EOT_EndNormalMatchItem;
        if (gameServer != null)
        {
          if (gameServer.BaseInfo.BaseInfo.bArbitrated)
            eofferType = EOfferType.EOT_EndRankedMatchItem;
          else if (gameServer.BaseInfo.BaseInfo.bPrivate)
            eofferType = EOfferType.EOT_EndPrivateMatchItem;
          this.Log("Sending End Match Offer Items for type " + eofferType.ToString(), false, ELoggingLevel.ELL_Informative);
          this.CombinedReq = new PWCombinedReq(this.Connection);
          this.CombinedReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
          if (this.VictoriousUserIds.Count > 0)
            this.CombinedReq.AddSubRequest((PWRequestBase) new PWGiftOfferReq()
            {
              UserIds = this.VictoriousUserIds,
              OfferType = eofferType,
              OfferData = (OfferDataBase) new MatchResultData(EMatchResult.EMR_Victory)
            });
          if (this.DefeatedUserIds.Count > 0)
            this.CombinedReq.AddSubRequest((PWRequestBase) new PWGiftOfferReq()
            {
              UserIds = this.DefeatedUserIds,
              OfferType = eofferType,
              OfferData = (OfferDataBase) new MatchResultData(EMatchResult.EMR_Defeat)
            });
          return this.CombinedReq.SubmitServerQuery();
        }
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.CombinedReq != null && this.CombinedReq.Successful;

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
