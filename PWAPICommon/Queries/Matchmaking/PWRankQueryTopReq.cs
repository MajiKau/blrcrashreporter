﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWRankQueryTopReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWRankQueryTopReq : PWRequestBase
  {
    private TopRankData OutData;

    public PWRankQueryTopReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWRankQueryTopReq(PWConnectionBase InClient)
      : base(InClient)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage) => true;

    public override bool SubmitClientQuery() => this.SendDefaultMessage();

    protected override EMessageType GetMessageType() => EMessageType.EMT_RankedQueryTop;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      string OutValue;
      if (!this.OwningServer.GetConfigValue<string>("MatchURL", out OutValue))
        return this.ProcessServerResultDefault();
      GetTopRanks getTopRanks;
      PWWebReq pwWebReq = new PWWebReq(this.Connection, OutValue, false, EWebRequestType.EWRT_POST, (object) getTopRanks, typeof (TopRankData));
      pwWebReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwWebReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (!(ForRequest is PWWebReq pwWebReq) || pwWebReq.Result == null || !(pwWebReq.Result.GetType() == typeof (TopRankData)))
        return false;
      this.OutData = (TopRankData) pwWebReq.Result;
      return true;
    }

    protected override bool ProcessServerResult() => this.Successful ? this.SendMessage(this.Serializer.SerializeObject<TopRankData>(this.OutData)) : this.SendDefaultMessage();

    public override bool ParseClientQuery(byte[] InMessage)
    {
      object[] objArray = this.Serializer.Deserialize(InMessage, typeof (TopRankData));
      if (objArray.Length != 1 || !(objArray[0].GetType() == typeof (TopRankData)))
        return false;
      this.OutData = (TopRankData) objArray[0];
      return true;
    }
  }
}
