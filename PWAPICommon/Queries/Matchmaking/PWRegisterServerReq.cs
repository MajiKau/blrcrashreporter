﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWRegisterServerReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWRegisterServerReq : PWRequestBase
  {
    public PWServerInfoFull ServerInfo;
    public PWMachineID MachineId;
    public string ServerVersion;
    private bool bRegisteredSuccessfully;

    public PWRegisterServerReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWRegisterServerReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_RegisterServer;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray1 = this.Serializer.Deserialize(InMessage);
      this.ServerInfo = new PWServerInfoFull();
      this.MachineId = new PWMachineID();
      if (strArray1.Length < 12 || !Guid.TryParse(strArray1[0], out this.ServerInfo.BaseInfo.RequestId) || (!int.TryParse(strArray1[3], out this.ServerInfo.GamePort) || !int.TryParse(strArray1[4], out this.ServerInfo.GMPort)) || (!int.TryParse(strArray1[9], out this.ServerInfo.BaseInfo.MaxPlayers) || !this.MachineId.TryParse(strArray1[12])))
        return false;
      this.ServerVersion = strArray1[1];
      this.ServerInfo.PublicIP = strArray1[2];
      this.ServerInfo.ServerID = Convert.ToInt32(strArray1[5]);
      this.ServerInfo.BaseInfo.ServerName = strArray1[6];
      this.ServerInfo.BaseInfo.OwnerName = strArray1[7];
      this.ServerInfo.BaseInfo.Password = strArray1[10];
      this.ServerInfo.ServerProperties.Clear();
      string str1 = strArray1[11];
      char[] chArray1 = new char[1]{ ',' };
      foreach (string str2 in str1.Split(chArray1))
      {
        char[] chArray2 = new char[1]{ '=' };
        string[] strArray2 = str2.Split(chArray2);
        if (strArray2.Length == 2)
          this.ServerInfo.ServerProperties[strArray2[0]] = strArray2[1];
      }
      return true;
    }

    protected override bool ProcessServerResult() => this.SendMessage(this.SerializeMessage(this.ServerInfo.ServerID.ToString()));

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.bRegisteredSuccessfully = false;
      if (this.ServerVersion != PWServerBase.Version)
      {
        this.Log("Attempted to register a server of the wrong version: " + this.ServerVersion, false, ELoggingLevel.ELL_Warnings);
        return this.ProcessServerResultDefault();
      }
      PWSuperServer resource = this.Connection.OwningServer.GetResource<PWSuperServer>();
      if (resource != null)
      {
        this.bRegisteredSuccessfully = resource.RegisterGameServer(this.ServerInfo, this.MachineId, this.Connection);
        this.Connection.Name = "Server " + (object) this.ServerInfo.ServerID;
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.bRegisteredSuccessfully;
  }
}
