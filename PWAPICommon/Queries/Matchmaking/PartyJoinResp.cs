﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PartyJoinResp
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Queries.Matchmaking
{
  [XmlRoot("party-added")]
  [ComVisible(true)]
  public struct PartyJoinResp
  {
    [XmlElement("status")]
    public string Status;
    [XmlElement("message")]
    public string Message;
    [XmlElement("season-name")]
    public string SeasonName;
    [XmlElement("season-id")]
    public int SeasonId;
    [XmlElement("reason-code")]
    public int ErrorCode;
  }
}
