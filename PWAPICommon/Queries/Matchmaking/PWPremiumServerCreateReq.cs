﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWPremiumServerCreateReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.Inventory;
using PWAPICommon.Wrappers;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWPremiumServerCreateReq : PWRequestBase
  {
    public long OwnerID;
    public Guid InventoryGuid;
    public string GameRules;
    public PremiumMatchItem Item;
    private PWSqlItemAddReq<PremiumMatchItem> ItemAddReq;
    private PWInvFindReq LookupReq;

    public PWPremiumServerCreateReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWPremiumServerCreateReq(PWConnectionBase InConn)
      : base(InConn)
    {
    }

    protected override EMessageType GetMessageType() => throw new NotImplementedException();

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.LookupReq == null)
      {
        this.LookupReq = new PWInvFindReq(this.Connection);
        this.LookupReq.OwnerID = this.OwnerID;
        this.LookupReq.GuidToFind = this.InventoryGuid;
        this.LookupReq.ClientResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.OwningServer.SendInternalQuery((PWRequestBase) this.LookupReq, EServerType.EST_Item, 5);
      }
      if (this.LookupReq.Successful && this.LookupReq.FoundStoreItem != (StoreItem) null && (this.LookupReq.FoundInvItem != null && this.ItemAddReq == null))
      {
        int activationQuantity = this.LookupReq.FoundStoreItem.GetTokenActivationQuantity();
        this.Item = new PremiumMatchItem();
        this.Item.OwnerID = this.OwnerID;
        this.Item.UniqueID = Guid.NewGuid();
        this.Item.CreationDate = PWServerBase.CurrentTime;
        this.Item.ExpirationDate = PWServerBase.CurrentTime + new TimeSpan(activationQuantity, 0, 0);
        this.Item.GameRules = this.GameRules;
        this.Item.SubRegion = this.Connection.GeoLoc.RegionTag;
        this.ItemAddReq = new PWSqlItemAddReq<PremiumMatchItem>(this.Connection);
        this.ItemAddReq.Item = this.Item;
        this.ItemAddReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.ItemAddReq.SubmitServerQuery();
      }
      if (this.ItemAddReq == null || !this.ItemAddReq.Successful)
        return this.ProcessServerResultDefault();
      PWInvDestroyReq pwInvDestroyReq = new PWInvDestroyReq(this.Connection);
      pwInvDestroyReq.GuidToRemove = this.InventoryGuid;
      pwInvDestroyReq.ClientResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
      this.OwningServer.GetResource<PWPremiumMatchCache>()?.CachedMatches.Add(this.Item);
      return this.OwningServer.SendInternalQuery((PWRequestBase) pwInvDestroyReq, EServerType.EST_Item, 5);
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.ItemAddReq != null && this.ItemAddReq.Successful;

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
