﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWPremiumServerRemoveReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWPremiumServerRemoveReq : PWRequestBase
  {
    public PWPremiumServerRemoveReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWPremiumServerRemoveReq(PWConnectionBase InConn)
      : base(InConn)
    {
    }

    protected override EMessageType GetMessageType() => throw new NotImplementedException();

    public override bool SubmitServerQuery() => base.SubmitServerQuery();

    public override bool ParseServerResult(PWRequestBase ForRequest) => base.ParseServerResult(ForRequest);

    protected override bool ProcessServerResult() => base.ProcessServerResult();
  }
}
