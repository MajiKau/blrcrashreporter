﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWCreateCustomServerReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Resources;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWCreateCustomServerReq : PWCreateServerReqBase
  {
    public PWCreateCustomServerReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWCreateCustomServerReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseClientQuery(byte[] InMessage)
    {
      object[] objArray = this.Serializer.Deserialize(InMessage, typeof (PWServerInfoFull));
      if (objArray.Length != 1 || objArray[0] == null || !(objArray[0].GetType() == typeof (PWServerInfoFull)))
        return true;
      this.ResultInfo = objArray[0] as PWServerInfoFull;
      return true;
    }

    public override bool ParseClientResult(PWRequestBase ForRequest) => this.ResultInfo != null;

    public override bool SubmitClientQuery() => this.SendMessage(this.Serializer.SerializeObject<PWServerInfoReq>(this.RequestInfo));

    public override bool ParseServerQuery(byte[] InMessage)
    {
      object[] objArray = this.Serializer.Deserialize(InMessage, typeof (PWServerInfoReq));
      if (objArray.Length != 1 || !(objArray[0].GetType() == typeof (PWServerInfoReq)))
        return false;
      this.RequestInfo = objArray[0] as PWServerInfoReq;
      return true;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_MasterServerCreateGame;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("Server Request to start a custom game...", false, ELoggingLevel.ELL_Informative);
      PWSuperServer resource1 = this.Connection.OwningServer.GetResource<PWSuperServer>();
      PWRankedManager resource2 = this.Connection.OwningServer.GetResource<PWRankedManager>();
      if (this.RequestInfo.bArbitrated && resource2 != null && !resource2.CreateRankedGame(this))
        return this.ProcessServerResultDefault();
      return resource1 != null && resource1.CreateServer((PWCreateServerReqBase) this) || this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.ResultInfo != null;

    protected override bool ProcessServerResult() => this.SendMessage(this.Serializer.SerializeObject<PWServerInfoFull>(this.ResultInfo));
  }
}
