﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWPartyRankedGameJoinQueueInternalReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWPartyRankedGameJoinQueueInternalReq : PWRequestBase
  {
    public PartyJoinReq InData;
    public PartyJoinResp OutData;
    public PWPartyRankedGameJoinQueueReq BaseRequest;
    public List<long> PartyMembers;

    private PWPartyRankedGameJoinQueueInternalReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWPartyRankedGameJoinQueueInternalReq(PWConnectionBase InConnection)
      : base(InConnection)
      => this.PartyMembers = new List<long>();

    protected override EMessageType GetMessageType() => EMessageType.EMT_PartyGameSearch;

    public override bool ParseServerQuery(byte[] InMessage) => true;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.InData.PartyMembers = new List<RankedPartyMember>();
      PWSuperServer resource = this.OwningServer.GetResource<PWSuperServer>();
      if (resource != null)
      {
        for (int index = 0; index < this.PartyMembers.Count; ++index)
        {
          PWConnectionBase loggedInConnFromId = resource.GetLoggedInConnFromId(this.PartyMembers[index]);
          if (loggedInConnFromId != null)
          {
            RankedPartyMember rankedPartyMember;
            rankedPartyMember.IP = loggedInConnFromId.EndPoint;
            rankedPartyMember.Level = -1;
            rankedPartyMember.UserID = this.PartyMembers[index];
            this.InData.PartyMembers.Add(rankedPartyMember);
            ++this.InData.TeamSize;
          }
        }
      }
      string OutValue;
      this.OwningServer.GetConfigValue<string>("MatchURL", out OutValue);
      PWWebReq pwWebReq = new PWWebReq(this.Connection, OutValue, false, EWebRequestType.EWRT_POST, (object) this.InData, typeof (PartyJoinResp));
      pwWebReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      this.Log("Sending party queue join web request with data: " + pwWebReq.ReadData, false, ELoggingLevel.ELL_Informative);
      return pwWebReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWWebReq pwWebReq = (PWWebReq) ForRequest;
      if (pwWebReq != null && pwWebReq.Result != null)
      {
        this.OutData = (PartyJoinResp) pwWebReq.Result;
        this.Log("Received party queue join web request with data: " + pwWebReq.ReadData, false, ELoggingLevel.ELL_Informative);
        if (this.OutData.Status == "success")
          return true;
      }
      else
        this.Log("Received party queue join response with no WebReq or no result", false, ELoggingLevel.ELL_Informative);
      return false;
    }

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
