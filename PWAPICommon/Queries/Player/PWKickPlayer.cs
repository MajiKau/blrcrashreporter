﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWKickPlayer
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWKickPlayer : PWRequestBase
  {
    public EKickPlayerCriteria KickCriteria;
    public long UserId;
    public string PlayerName;
    private PWProfileQueryBasicReq ProfileQueryReq;

    public PWKickPlayer()
      : this((PWConnectionBase) null)
    {
    }

    public PWKickPlayer(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_KickPlayer;

    public override bool ParseClientQuery(byte[] InMessage) => this.Serializer.Deserialize(InMessage).Length == 1;

    public override bool SubmitClientQuery()
    {
      base.SubmitClientQuery();
      string str = ((int) this.KickCriteria).ToString() + ":";
      string InString;
      switch (this.KickCriteria)
      {
        case EKickPlayerCriteria.EKPC_UserId:
          InString = str + this.UserId.ToString();
          break;
        case EKickPlayerCriteria.EKPC_Name:
          InString = str + this.PlayerName;
          break;
        default:
          return false;
      }
      return this.Connection.SendMessage(this.GetMessageType(), this.Serializer.SerializeRaw(InString));
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      int result;
      if (strArray.Length == 2 && int.TryParse(strArray[0], out result))
      {
        this.KickCriteria = (EKickPlayerCriteria) result;
        switch (this.KickCriteria)
        {
          case EKickPlayerCriteria.EKPC_UserId:
            return long.TryParse(strArray[1], out this.UserId);
          case EKickPlayerCriteria.EKPC_Name:
            this.PlayerName = strArray[1];
            return true;
        }
      }
      return false;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.ProfileQueryReq == null)
      {
        if (this.KickCriteria == EKickPlayerCriteria.EKPC_Name)
        {
          this.ProfileQueryReq = new PWProfileQueryBasicReq(this.Connection);
          this.ProfileQueryReq.ForUserName = this.PlayerName;
          this.ProfileQueryReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
          return this.ProfileQueryReq.SubmitServerQuery();
        }
      }
      else if (this.ProfileQueryReq.Successful)
        this.UserId = this.ProfileQueryReq.BasicProfile.UserId;
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWLoginCache resource = this.Connection.OwningServer.GetResource<PWLoginCache>();
      return resource != null && this.UserId != 0L && resource.ForceLogout(this.UserId);
    }

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
