﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWLoginRequest
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Servers;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Queries.Player
{
  [XmlRoot("login-request", Namespace = "")]
  [ComVisible(true)]
  public class PWLoginRequest : PWRequestBase
  {
    public string account;
    public string password;
    public int game;
    public string ip;
    [XmlIgnore]
    private string LanguageExt;
    [XmlIgnore]
    public string clientVersion;
    [XmlIgnore]
    public long SteamID;
    [XmlIgnore]
    private PWWebReq WebReq;
    private PWLoginResult Result;

    public PWLoginRequest()
      : base((PWConnectionBase) null)
    {
    }

    public PWLoginRequest(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (this.Connection == null || strArray.Length < 3)
        return false;
      this.account = strArray[0];
      this.password = strArray[1];
      this.clientVersion = strArray[2];
      this.game = this.Connection.OwningServer.PWGameID;
      this.ip = this.Connection.EndPoint;
      if (strArray.Length >= 4)
        long.TryParse(strArray[3], out this.SteamID);
      if (strArray.Length >= 5)
        this.LanguageExt = strArray[4];
      return true;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_Login;

    public override bool SubmitClientQuery() => this.SendMessage(this.Serializer.SerializeRaw(this.account + ":" + this.password + ":" + PWServerBase.Version + ":" + this.SteamID.ToString()));

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("[" + this.account + "] logging in", false, ELoggingLevel.ELL_Informative);
      if (this.clientVersion != PWServerBase.Version)
      {
        string str = 5.ToString() + ":Update Required Detected:0";
        this.Log(str, false, ELoggingLevel.ELL_Informative);
        return this.SendMessage(this.SerializeMessage(str));
      }
      switch (this.Connection.OwningServer.GetResource<PWLoginCache>().StartLogin(this.account, this.Connection))
      {
        case ELoginCacheResponse.ELCR_OK:
          string OutValue;
          if (!this.OwningServer.GetConfigValue<string>("AuthURL", out OutValue))
            return this.ProcessServerResultDefault();
          this.WebReq = new PWWebReq(this.Connection, OutValue, true, EWebRequestType.EWRT_POST, (object) this, typeof (PWLoginResult));
          this.WebReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
          return this.WebReq.SubmitServerQuery();
        case ELoginCacheResponse.ELCR_Duplicate:
          string ExtraString1 = 7.ToString() + ":Duplicate Login Detected:0";
          this.Log("[" + this.account + "] " + ExtraString1, false, ELoggingLevel.ELL_Informative);
          return this.SendMessage(this.SerializeMessage(ExtraString1));
        case ELoginCacheResponse.ELCR_Blacklisted:
          string ExtraString2 = 9.ToString() + ": Account is banned:0";
          this.Log("[" + this.account + "] " + ExtraString2, false, ELoggingLevel.ELL_Informative);
          return this.SendMessage(this.SerializeMessage(ExtraString2));
        case ELoginCacheResponse.ELCR_Whitelisted:
          string ExtraString3 = 6.ToString() + ":Server Undergoing maintenance:0";
          this.Log("[" + this.account + "] " + ExtraString3, false, ELoggingLevel.ELL_Informative);
          return this.SendMessage(this.SerializeMessage(ExtraString3));
        default:
          string ExtraString4 = 4.ToString() + ":Servers Unavailable:0";
          this.Log("[" + this.account + "] " + ExtraString4, false, ELoggingLevel.ELL_Informative);
          return this.SendMessage(this.SerializeMessage(ExtraString4));
      }
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (ForRequest != null && ForRequest.Successful)
      {
        this.Result = (PWLoginResult) ((PWWebReq) ForRequest).Result;
        if (this.Result != null)
          return this.Result.status == "success";
      }
      return false;
    }

    protected override bool ProcessServerResult()
    {
      if (this.Result == null)
      {
        string ExtraString = 4.ToString() + ":Servers Unavailable:0";
        this.Log("[" + this.account + "] " + ExtraString, false, ELoggingLevel.ELL_Informative);
        return this.SendMessage(this.SerializeMessage(ExtraString));
      }
      PWLoginCache resource = this.Connection.OwningServer.GetResource<PWLoginCache>();
      long result = -1;
      long.TryParse(this.Result.userid, out result);
      bool flag = false;
      switch (resource.FinishLogin(this.account, result, this.Successful, this.Connection.EndPoint, this.SteamID, this.LanguageExt))
      {
        case ELoginCacheResponse.ELCR_OK:
          flag = true;
          this.Connection.Name = this.account;
          this.Log("[" + this.account + "] Login Success: " + this.Result.message, false, ELoggingLevel.ELL_Informative);
          break;
        case ELoginCacheResponse.ELCR_Duplicate:
          result = 7L;
          this.Log("[" + this.account + "] Duplicate Login Detected", false, ELoggingLevel.ELL_Informative);
          break;
        case ELoginCacheResponse.ELCR_Blacklisted:
          result = 9L;
          this.Log("[" + this.account + "] User is Black-listed", false, ELoggingLevel.ELL_Informative);
          break;
        case ELoginCacheResponse.ELCR_Whitelisted:
          result = 6L;
          this.Log("[" + this.account + "] User is White-listed", false, ELoggingLevel.ELL_Informative);
          break;
        case ELoginCacheResponse.ELCR_Failed:
          result = 8L;
          this.Log("[" + this.account + "] Login Failed: " + this.Result.message, false, ELoggingLevel.ELL_Informative);
          break;
        default:
          if (this.WebReq == null || !this.WebReq.Successful)
          {
            result = 3L;
            this.Log("[" + this.account + "] Login: Web Request Failed", false, ELoggingLevel.ELL_Errors);
            break;
          }
          result = 2L;
          break;
      }
      return this.SendMessage(this.Serializer.SerializeRaw((flag ? "T:" : "F:") + result.ToString() + ":" + this.Result.usertype.ToString() + ":" + resource.NumPlayers.ToString() + ":" + PWServerApp.AssemblyName));
    }

    private enum EOnlineServerConnectionStatus
    {
      OSCS_NotConnected,
      OSCS_Connected,
      OSCS_UnknownError,
      OSCS_ServiceFailure,
      OSCS_ServiceUnavailable,
      OSCS_UpdateRequired,
      OSCS_ServerMaintenance,
      OSCS_DuplicateLoginDetected,
      OSCS_InvalidUser,
      OSCS_Banned,
    }
  }
}
