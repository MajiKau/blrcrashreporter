﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWQueryProfileBlobReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWQueryProfileBlobReq : PWRequestBase
  {
    public List<UserProfile> Users;

    public PWQueryProfileBlobReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWQueryProfileBlobReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => throw new NotImplementedException();

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("Querying Profile Blob", false, ELoggingLevel.ELL_Informative);
      SqlCommand InQuery = new SqlCommand("Select userid, username, experience, gender, titledata, badgedata, lastseentime From account");
      InQuery.CommandType = CommandType.Text;
      PWSQLReq pwsqlReq = new PWSQLReq(InQuery, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (ForRequest is PWSQLReq pwsqlReq)
      {
        IList<object> responseValues = pwsqlReq.ResponseValues;
        if (responseValues != null && responseValues.Count > 0)
        {
          this.Users = new List<UserProfile>(responseValues.Count);
          for (int index = 0; index < responseValues.Count; ++index)
          {
            UserProfile userProfile = new UserProfile();
            if (userProfile.ParseFromArray(responseValues[index] as object[], UserProfile.EProfileQueryType.EPQT_Basic))
              this.Users.Add(userProfile);
          }
          return true;
        }
      }
      return false;
    }

    protected override bool ProcessServerResult()
    {
      this.Log("Query Profile Blob Complete", false, ELoggingLevel.ELL_Informative);
      return this.SendMessage(this.SerializeMessage<List<UserProfile>>("", this.Users));
    }
  }
}
