﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWSuperServerLogin
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Queries.Backend;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWSuperServerLogin : PWRequestBase
  {
    private long UserID;
    private PWQueryIPInfoDBReq GeoReq;

    public PWSuperServerLogin()
      : base((PWConnectionBase) null)
    {
    }

    public PWSuperServerLogin(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_Login;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 1 && long.TryParse(strArray[0], out this.UserID);
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWConnectionBase connection = this.Connection;
      return connection != null && connection.UserID == this.UserID;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      PWSuperServer resource = this.OwningServer.GetResource<PWSuperServer>();
      if (resource == null)
        return false;
      if (this.GeoReq == null)
      {
        this.Connection.UserID = this.UserID;
        this.Connection.Name = this.UserID.ToString();
        this.GeoReq = new PWQueryIPInfoDBReq(this.Connection);
        this.GeoReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        this.GeoReq.SubmitServerQuery();
      }
      else if (this.GeoReq.Successful)
        resource.AddLoggedInPlayer(this.UserID, this.Connection);
      return this.ProcessServerResultDefault();
    }

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
