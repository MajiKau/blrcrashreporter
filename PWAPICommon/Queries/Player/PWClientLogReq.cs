﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWClientLogReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWClientLogReq : PWRequestBase
  {
    public long UserID;
    public string LogMessage;

    public PWClientLogReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (strArray.Length < 2 || !long.TryParse(strArray[0], out this.UserID))
        return false;
      this.LogMessage = "";
      for (int index = 1; index < strArray.Length; ++index)
      {
        PWClientLogReq pwClientLogReq = this;
        pwClientLogReq.LogMessage = pwClientLogReq.LogMessage + strArray[index] + ":";
      }
      return true;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ClientLog;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log(this.UserID.ToString() + " " + this.LogMessage, true, ELoggingLevel.ELL_Verbose);
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => true;

    protected override bool ProcessServerResult() => true;

    public override bool SubmitClientQuery() => this.SendMessage(this.Serializer.SerializeRaw(this.UserID.ToString() + ":" + this.LogMessage));

    public override bool ParseClientQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 1 && strArray[0] == "T";
    }

    public override bool ParseClientResult(PWRequestBase ForRequest) => true;
  }
}
