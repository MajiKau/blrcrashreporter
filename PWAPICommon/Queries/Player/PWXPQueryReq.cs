﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWXPQueryReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWXPQueryReq : PWRequestBase
  {
    public long UserId;
    public int ReadExperience;

    public PWXPQueryReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWXPQueryReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 1 && long.TryParse(strArray[0], out this.UserId);
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("Querying Experience for UserId " + (object) this.UserId, false, ELoggingLevel.ELL_Informative);
      SqlCommand InQuery = new SqlCommand("SP_Account_Get_Experience_By_UserId");
      InQuery.CommandType = CommandType.StoredProcedure;
      InQuery.Parameters.Add("@userid", SqlDbType.BigInt);
      InQuery.Parameters["@userid"].Value = (object) this.UserId;
      PWSQLReq pwsqlReq = new PWSQLReq(InQuery, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_QueryXP;

    protected override string GetMessageString(bool bForceSucceed)
    {
      string str = base.GetMessageString(bForceSucceed);
      if (str == "T")
        str = str + ":" + (object) this.UserId + ":" + (object) this.ReadExperience;
      return str;
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWSQLReq pwsqlReq = (PWSQLReq) ForRequest;
      if (pwsqlReq == null || pwsqlReq.ResponseValues == null || pwsqlReq.ResponseValues.Count != 1)
        return false;
      object[] responseValue = (object[]) pwsqlReq.ResponseValues[0];
      this.ReadExperience = responseValue[0].GetType() == typeof (int) ? (int) responseValue[0] : 0;
      return true;
    }

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
