﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWAddExperienceReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.General;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWAddExperienceReq : PWRequestBase
  {
    private long UserId;
    private int ExperienceToAdd;
    private int TotalExperience;
    private PWXPQueryReq QueryReq;
    private bool bNotify;

    protected override EMessageType GetMessageType() => EMessageType.EMT_AddExperience;

    public PWAddExperienceReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWAddExperienceReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] stringArray = this.Serializer.DeserializeToStringArray(InMessage);
      if (stringArray.Length < 2 || !long.TryParse(stringArray[0], out this.UserId) || !int.TryParse(stringArray[1], out this.ExperienceToAdd))
        return false;
      if (stringArray.Length > 2)
        this.bNotify = stringArray[2] == "T";
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.QueryReq == null)
      {
        this.QueryReq = new PWXPQueryReq(this.Connection);
        this.QueryReq.UserId = this.UserId;
        this.QueryReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.QueryReq.SubmitServerQuery();
      }
      if (!this.QueryReq.Successful)
        return this.ProcessServerResultDefault();
      this.TotalExperience = this.QueryReq.ReadExperience;
      this.ExperienceToAdd = PWLevelMap.AdjustForLevelCap(this.QueryReq.ReadExperience, this.ExperienceToAdd);
      if (this.ExperienceToAdd <= 0)
        return this.ProcessServerResultDefault();
      this.Log("Adding " + (object) this.ExperienceToAdd + " XP for UserID " + (object) this.UserId, false, ELoggingLevel.ELL_Informative);
      SqlCommand InQuery = new SqlCommand("SP_Account_Experience_Update");
      InQuery.CommandType = CommandType.StoredProcedure;
      InQuery.Parameters.Add("@userid", SqlDbType.BigInt);
      InQuery.Parameters["@userid"].Value = (object) this.UserId;
      InQuery.Parameters.Add("@Experience", SqlDbType.Int);
      InQuery.Parameters["@Experience"].Value = (object) this.ExperienceToAdd;
      PWSQLReq pwsqlReq = new PWSQLReq(InQuery, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (ForRequest == this && this.ExperienceToAdd <= 0)
        return true;
      PWSQLReq pwsqlReq = ForRequest as PWSQLReq;
      bool flag = false;
      int result = -1;
      if (pwsqlReq != null && pwsqlReq.Successful)
      {
        string[] strArray = ((string) ((object[]) pwsqlReq.ResponseValues[0])[0]).Split(new char[1]
        {
          ' '
        }, StringSplitOptions.RemoveEmptyEntries);
        if (strArray.Length == 2 && int.TryParse(strArray[0], out result))
        {
          this.Log("addxp " + " userid=" + (object) this.UserId + " xptoadd=" + (object) this.ExperienceToAdd + " serverid=" + (object) this.Connection.OwningServer.PWServerID + " ip=" + this.Connection.EndPoint, true, ELoggingLevel.ELL_Informative);
          this.TotalExperience = result;
          flag = true;
        }
      }
      if (flag)
      {
        int NewLevel = -1;
        int PreviousLevel = -1;
        if (PWLevelMap.PassedLevelThreshold(result - this.ExperienceToAdd, result, out PreviousLevel, out NewLevel))
        {
          PWOfferCache resource = this.OwningServer.GetResource<PWOfferCache>();
          if (resource != null)
          {
            for (int index = PreviousLevel + 1; index <= NewLevel; ++index)
              resource.AwardMatchingOffers(this.UserId, EOfferType.EOT_ExperienceThreshold, (OfferDataBase) new ExperienceThresholdData()
              {
                Level = index
              }, this.Connection);
          }
        }
      }
      return flag;
    }

    protected override bool ProcessServerResult()
    {
      if (this.Successful && this.bNotify)
      {
        this.QueryReq = new PWXPQueryReq(this.Connection);
        this.QueryReq.UserId = this.UserId;
        this.QueryReq.ReadExperience = this.TotalExperience;
        new PWRouteMessageReq(this.Connection, this.UserId, this.QueryReq.GetMessagePairing(true)).SubmitServerQuery();
      }
      this.OwningServer.GetResource<PWPlayerItemCache<UserProfile>>()?.MarkDirtyFor(this.UserId);
      return this.SendMessage(this.SerializeMessage(this.UserId.ToString()));
    }
  }
}
