﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWQueryLoginQueue
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWQueryLoginQueue : PWRequestBase
  {
    public string AccountName;
    private PWLoginCache LoginCache;
    private int QueueTime;
    private int PlayersInQueue;
    private int PositionInQueue;

    public PWQueryLoginQueue(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_CheckLoginQueue;

    public override bool ParseServerQuery(byte[] InData)
    {
      string[] strArray = this.Serializer.Deserialize(InData);
      if (strArray.Length != 1 || !(strArray[0] != ""))
        return false;
      this.AccountName = strArray[0];
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      this.LoginCache = this.OwningServer.GetResource<PWLoginCache>();
      this.LoginCache.QueueUser(this.AccountName, out this.QueueTime, out this.PlayersInQueue, out this.PositionInQueue);
      return true;
    }

    protected override bool ProcessServerResult() => this.SendMessage(this.SerializeMessage(this.PositionInQueue.ToString() + ":" + (object) this.PlayersInQueue + ":" + (object) this.QueueTime));
  }
}
