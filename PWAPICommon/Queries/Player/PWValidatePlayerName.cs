﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWValidatePlayerName
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWValidatePlayerName : PWRequestBase
  {
    public long UserId;
    public string PlayerName;
    private PWValidatePlayerName.EInvalidReason InvalidReason;
    private PWProfileQueryBasicReq ProfileQueryReq;
    private static readonly List<string> InvalidNameList = new List<string>()
    {
      PWServerBase.AdminName.ToLower(),
      PWServerBase.PurchaseSenderName.ToLower(),
      "gm",
      "perfectworld",
      "pwe",
      "sysadmin",
      "systemadmin",
      "systemsadmin",
      "systemop",
      "sysop"
    };

    public PWValidatePlayerName()
      : this((PWConnectionBase) null)
    {
    }

    public PWValidatePlayerName(PWConnectionBase InConnection)
      : base(InConnection)
      => this.InvalidReason = PWValidatePlayerName.EInvalidReason.EIR_Valid;

    protected override EMessageType GetMessageType() => EMessageType.EMT_PlayerNameValidate;

    private bool IsReservedName(string Name) => PWValidatePlayerName.InvalidNameList.Contains(Name.ToLower());

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (strArray.Length != 2 || !long.TryParse(strArray[0], out this.UserId))
        return false;
      this.PlayerName = strArray[1];
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.ProfileQueryReq == null)
      {
        char[] FilteredChars = new char[4]
        {
          '\'',
          '"',
          '<',
          '>'
        };
        char ch = ((IEnumerable<char>) this.PlayerName.ToCharArray()).FirstOrDefault<char>((Func<char, bool>) (NameChar => ((IEnumerable<char>) FilteredChars).Contains<char>(NameChar)));
        PWPlayerItemCache<string> resource = this.Connection.OwningServer.GetResource<PWPlayerItemCache<string>>();
        if (ch != char.MinValue)
          this.InvalidReason = PWValidatePlayerName.EInvalidReason.EIR_InvalidChars;
        else if (resource != null && (resource.ContainsValue(this.PlayerName) || this.IsReservedName(this.PlayerName)))
        {
          this.InvalidReason = PWValidatePlayerName.EInvalidReason.EIR_InUse;
        }
        else
        {
          this.ProfileQueryReq = new PWProfileQueryBasicReq(this.Connection);
          this.ProfileQueryReq.ForUserName = this.PlayerName;
          this.ProfileQueryReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
          return this.ProfileQueryReq.SubmitServerQuery();
        }
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ReqWrapper)
    {
      PWPlayerItemCache<string> resource = this.Connection.OwningServer.GetResource<PWPlayerItemCache<string>>();
      if (this.ProfileQueryReq != null && this.ProfileQueryReq.Successful && (this.ProfileQueryReq.BasicProfile.UserId != this.UserId && string.Compare(this.ProfileQueryReq.BasicProfile.UserName, this.PlayerName, true) == 0))
      {
        this.InvalidReason = PWValidatePlayerName.EInvalidReason.EIR_InUse;
        if (resource != null)
        {
          resource.MarkDirtyFor(this.UserId);
          resource.SetCachedItem(this.ProfileQueryReq.BasicProfile.UserId, this.ProfileQueryReq.BasicProfile.UserName);
        }
        return false;
      }
      if (resource != null && resource.ContainsValue(this.PlayerName))
        this.InvalidReason = PWValidatePlayerName.EInvalidReason.EIR_InUse;
      return this.InvalidReason == PWValidatePlayerName.EInvalidReason.EIR_Valid;
    }

    protected override bool ProcessServerResult() => this.SendMessage(this.SerializeMessage(this.UserId.ToString() + ":" + ((int) this.InvalidReason).ToString()));

    private enum EInvalidReason
    {
      EIR_Valid,
      EIR_InvalidChars,
      EIR_InUse,
      EIR_Unknown,
    }
  }
}
