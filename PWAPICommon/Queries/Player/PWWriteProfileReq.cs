﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWWriteProfileReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWWriteProfileReq : PWRequestBase
  {
    private long UserId;
    private int TitleData;
    private long BadgeData;
    private byte[] UserData;

    public PWWriteProfileReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWWriteProfileReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (strArray.Length > 3 && long.TryParse(strArray[0], out this.UserId) && (int.TryParse(strArray[1], out this.TitleData) && long.TryParse(strArray[2], out this.BadgeData)))
      {
        int sourceIndex = strArray[0].Length + strArray[1].Length + strArray[2].Length + 3;
        if (sourceIndex > 0 && sourceIndex < InMessage.Length)
        {
          this.UserData = new byte[InMessage.Length - sourceIndex];
          Array.Copy((Array) InMessage, sourceIndex, (Array) this.UserData, 0, this.UserData.Length);
          return true;
        }
      }
      return false;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_WriteProfile;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("Writing Profile for UserID " + (object) this.UserId, false, ELoggingLevel.ELL_Informative);
      SqlCommand InQuery = new SqlCommand("SP_Account_Profile_Update");
      InQuery.CommandType = CommandType.StoredProcedure;
      InQuery.Parameters.Add("@userid", SqlDbType.BigInt);
      InQuery.Parameters["@userid"].Value = (object) this.UserId;
      InQuery.Parameters.Add("@profile_data", SqlDbType.VarBinary);
      InQuery.Parameters["@profile_data"].Value = (object) this.UserData;
      InQuery.Parameters.Add("@TitleData", SqlDbType.Int);
      InQuery.Parameters["@TitleData"].Value = (object) this.TitleData;
      InQuery.Parameters.Add("@BadgeData", SqlDbType.BigInt);
      InQuery.Parameters["@BadgeData"].Value = (object) this.BadgeData;
      PWSQLReq pwsqlReq = new PWSQLReq(InQuery, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => ForRequest.Successful;

    protected override bool ProcessServerResult()
    {
      if (!this.Successful)
        this.Log("Profile Write Failed for UserID " + (object) this.UserId, false, ELoggingLevel.ELL_Warnings);
      this.OwningServer.GetResource<PWPlayerItemCache<UserProfile>>()?.MarkDirtyFor(this.UserId);
      return this.SendDefaultMessage();
    }
  }
}
