﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.General.PWHeartbeatRespondingReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.General
{
  [ComVisible(true)]
  public class PWHeartbeatRespondingReq : PWHeartbeatReq
  {
    public PWHeartbeatRespondingReq(PWConnectionBase InClient)
      : base(InClient)
    {
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => true;

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
