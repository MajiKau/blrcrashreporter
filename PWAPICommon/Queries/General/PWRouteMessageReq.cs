﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.General.PWRouteMessageReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.General
{
  [ComVisible(true)]
  public class PWRouteMessageReq : PWNotificationReq<PWMessageItem>
  {
    public PWRouteMessageReq()
    {
    }

    public PWRouteMessageReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public PWRouteMessageReq(PWConnectionBase InConnection, long InUserID, PWMessageItem InItem)
      : base(InConnection, InUserID, InItem)
    {
    }

    public PWRouteMessageReq(
      PWConnectionBase InConnection,
      long InUserID,
      IEnumerable<PWMessageItem> InItems)
      : base(InConnection, InUserID, InItems)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_RouteMessage;

    public override object Clone()
    {
      PWRouteMessageReq pwRouteMessageReq = new PWRouteMessageReq();
      pwRouteMessageReq.UserID = this.UserID;
      pwRouteMessageReq.Items = this.Items;
      return (object) pwRouteMessageReq;
    }

    protected override bool ProcessServerResult()
    {
      foreach (PWMessageItem pwMessageItem in this.Items)
        this.Connection.SendMessage(pwMessageItem.Type, this.CompressMessage(this.Serializer.SerializeRaw(pwMessageItem.Message)));
      return true;
    }
  }
}
