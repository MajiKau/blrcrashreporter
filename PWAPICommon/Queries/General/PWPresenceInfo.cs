﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.General.PWPresenceInfo
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Queries.General
{
  [ComVisible(true)]
  public class PWPresenceInfo
  {
    [XmlAttribute("UID")]
    public long UserID;
    [XmlAttribute("RN")]
    public string RegionName;

    public virtual void CopyFrom(PWPresenceInfo Presence)
    {
    }

    public override string ToString() => this.UserID.ToString() + ", " + this.RegionName;
  }
}
