﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.General.PWHeartbeatReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.General
{
  [ComVisible(true)]
  public class PWHeartbeatReq : PWRequestBase
  {
    private static byte[] HeartbeatData = new byte[2]
    {
      (byte) 72,
      (byte) 56
    };

    public PWHeartbeatReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWHeartbeatReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_Heartbeat;

    public override bool ParseServerQuery(byte[] InMessage) => InMessage.Length == 2 && (int) InMessage[0] == (int) PWHeartbeatReq.HeartbeatData[0] && (int) InMessage[1] == (int) PWHeartbeatReq.HeartbeatData[1];

    public override bool ParseClientQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 1 && strArray[0] == "T";
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      (this.Connection as PWConnectionTCPBase).HeartbeatReceived();
      return true;
    }

    public override bool SubmitClientQuery()
    {
      base.SubmitClientQuery();
      (this.Connection as PWConnectionTCPBase).HeartbeatReceived();
      return this.SendMessage(PWHeartbeatReq.HeartbeatData);
    }
  }
}
