﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.General.PWReadFileReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.General
{
  [ComVisible(true)]
  public class PWReadFileReq : PWRequestBase
  {
    private long UserID;
    private string FileToRead;
    private string ReadData;

    public PWReadFileReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWReadFileReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ReadFile;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (strArray.Length != 2 || !long.TryParse(strArray[0], out this.UserID) || string.IsNullOrEmpty(strArray[1]))
        return false;
      this.FileToRead = strArray[1];
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("User " + (object) this.UserID + " Reading File: " + this.FileToRead, false, ELoggingLevel.ELL_Informative);
      PWFileCache resource = this.OwningServer.GetResource<PWFileCache>();
      if (resource != null)
      {
        this.ReadData = resource.GetFile(this.FileToRead);
        if (this.ReadData != null)
          return this.ProcessServerResultDefault();
      }
      string OutValue;
      if (!this.OwningServer.GetConfigValue<string>("FileURL", out OutValue))
        return this.ProcessServerResultDefault();
      PWWebReq pwWebReq = new PWWebReq(this.Connection, OutValue + this.FileToRead, false, EWebRequestType.EWRT_GET, (object) null, (Type) null);
      pwWebReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwWebReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (ForRequest is PWWebReq pwWebReq && pwWebReq.Successful)
      {
        this.ReadData = pwWebReq.ReadData;
        this.OwningServer.GetResource<PWFileCache>()?.SetFile(this.FileToRead, this.ReadData);
      }
      return this.ReadData != null;
    }

    protected override bool ProcessServerResult() => this.SendMessage(this.SerializeMessage(this.FileToRead + ":" + this.ReadData));
  }
}
