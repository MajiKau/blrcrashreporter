﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.General.PWPresenceUpdateReq`1
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Resources;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.General
{
  [ComVisible(true)]
  public class PWPresenceUpdateReq<T> : PWRequestBase, ICloneable where T : PWPresenceInfo
  {
    public long UserID;
    public T PresenceInfo;
    public EPresenceUpdateType UpdateType;

    public PWPresenceUpdateReq()
      : this((PWConnectionBase) null)
    {
    }

    public PWPresenceUpdateReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_PresenceUpdate;

    public object Clone() => (object) new PWPresenceUpdateReq<T>()
    {
      UserID = this.UserID,
      PresenceInfo = this.PresenceInfo,
      UpdateType = this.UpdateType
    };

    public override bool ParseServerQuery(byte[] InMessage)
    {
      PWPresenceUpdateReq<T> presenceUpdateReq = this.DeserializeSelf<PWPresenceUpdateReq<T>>(InMessage);
      if (presenceUpdateReq == null)
        return base.ParseServerQuery(InMessage);
      this.UserID = presenceUpdateReq.UserID;
      this.PresenceInfo = presenceUpdateReq.PresenceInfo;
      this.UpdateType = presenceUpdateReq.UpdateType;
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => true;

    protected override bool ProcessServerResult()
    {
      PWPresenceCache<T> resource = this.Connection.OwningServer.GetResource<PWPresenceCache<T>>();
      if (resource == null || (object) this.PresenceInfo == null)
        return false;
      resource.ApplyPresence(this.UserID, this.UpdateType, this.PresenceInfo);
      return true;
    }

    public override bool SubmitClientQuery()
    {
      base.SubmitClientQuery();
      return this.Connection != null && this.SendMessage(this.SerializeSelf());
    }
  }
}
