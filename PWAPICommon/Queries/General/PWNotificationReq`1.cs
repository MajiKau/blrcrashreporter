﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.General.PWNotificationReq`1
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Queries.General
{
  [ComVisible(true)]
  public abstract class PWNotificationReq<T> : PWRequestBase, ICloneable
  {
    [XmlElement("UID")]
    public long UserID;
    [XmlElement("ITEM")]
    public T[] Items;

    public PWNotificationReq()
      : this((PWConnectionBase) null)
    {
    }

    public PWNotificationReq(PWConnectionBase InConnection)
      : this(InConnection, (IEnumerable<T>) new T[0])
    {
    }

    public PWNotificationReq(PWConnectionBase InConnection, T InItem)
      : this(InConnection, (IEnumerable<T>) new T[1]
      {
        InItem
      })
    {
    }

    public PWNotificationReq(PWConnectionBase InConnection, IEnumerable<T> InItems)
      : this(InConnection, PWServerBase.EmptyUserID, InItems)
    {
      if (this.Connection == null)
        return;
      this.UserID = this.Connection.UserID;
    }

    public PWNotificationReq(PWConnectionBase InConnection, long InUserID, T InItem)
      : this(InConnection, InUserID, (IEnumerable<T>) new T[1]
      {
        InItem
      })
    {
    }

    public PWNotificationReq(PWConnectionBase InConnection, long InUserID, IEnumerable<T> InItems)
      : base(InConnection)
    {
      this.UserID = InUserID;
      this.Items = (InItems ?? (IEnumerable<T>) new T[0]).ToArray<T>();
    }

    public abstract object Clone();

    public override bool ParseServerQuery(byte[] InMessage)
    {
      PWNotificationReq<T> pwNotificationReq = this.DeserializeSelf<PWNotificationReq<T>>(InMessage);
      if (pwNotificationReq == null)
        return base.ParseServerQuery(InMessage);
      this.UserID = pwNotificationReq.UserID;
      this.Items = pwNotificationReq.Items;
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.Connection.UserID != this.UserID)
      {
        this.ServerResultProcessor = new ProcessDelegate(this.RouteNotifications);
        this.ForceProcessResult = false;
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.Items != null && this.Items.Length > 0;

    protected bool RouteNotifications(PWRequestBase ForRequest)
    {
      if (this.Successful)
      {
        foreach (IPWNotificationCache<T> resource in this.OwningServer.GetResources<IPWNotificationCache<T>>())
        {
          if (resource.Enqueue(this))
            return true;
        }
      }
      return false;
    }

    public override bool ParseClientQuery(byte[] InMessage) => base.ParseClientQuery(InMessage);

    public override bool SubmitClientQuery()
    {
      base.SubmitClientQuery();
      return this.SendMessage(this.SerializeSelf());
    }

    public override bool ParseClientResult(PWRequestBase ForRequest) => base.ParseClientResult(ForRequest);

    protected override bool ProcessClientResult(PWRequestBase ForRequest) => base.ProcessClientResult(ForRequest);
  }
}
