﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Currency.PWGPAddReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Queries.Currency
{
  [ComVisible(true)]
  public class PWGPAddReq : PWGPChangeReq
  {
    public long UserId;
    public int Amount;
    private int TotalFunds = -1;

    [XmlIgnore]
    public override KeyValuePair<long, int> UserBalance => new KeyValuePair<long, int>(this.UserId, this.TotalFunds);

    public PWGPAddReq()
      : this((PWConnectionBase) null)
    {
    }

    public PWGPAddReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] stringArray = this.Serializer.DeserializeToStringArray(InMessage);
      if (stringArray.Length != 2 || !long.TryParse(stringArray[0], out this.UserId) || !int.TryParse(stringArray[1], out this.Amount))
        return false;
      return this.UserId <= 0L || this.Amount <= 0 || true;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_GPAdd;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("Adding " + (object) this.Amount + " GP to user " + (object) this.UserId, false, ELoggingLevel.ELL_Informative);
      SqlCommand InQuery = new SqlCommand("SP_Fund_Add_Money");
      InQuery.CommandType = CommandType.StoredProcedure;
      InQuery.Parameters.Add("@userid", SqlDbType.BigInt);
      InQuery.Parameters["@userid"].Value = (object) this.UserId;
      InQuery.Parameters.Add("@zombie_money ", SqlDbType.BigInt);
      InQuery.Parameters["@zombie_money "].Value = (object) this.Amount;
      PWSQLReq pwsqlReq = new PWSQLReq(InQuery, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWSQLReq pwsqlReq = (PWSQLReq) ForRequest;
      if (!pwsqlReq.Successful)
        return false;
      string[] strArray = ((string) ((object[]) pwsqlReq.ResponseValues[0])[0]).Split(new char[1]
      {
        ' '
      }, StringSplitOptions.RemoveEmptyEntries);
      int result = -1;
      if (strArray.Length == 5 && int.TryParse(strArray[3], out result))
        this.TotalFunds = result;
      else if (strArray.Length == 11 && int.TryParse(strArray[9], out result))
        this.TotalFunds = result;
      return true;
    }
  }
}
