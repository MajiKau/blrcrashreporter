﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Currency.PWZPRemoveResp
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Queries.Currency
{
  [XmlRoot("item-purchase-response")]
  [ComVisible(true)]
  public class PWZPRemoveResp
  {
    [XmlElement("userid")]
    public string userid;
    [XmlElement("server")]
    public int server;
    [XmlElement("status")]
    public string status;
    [XmlElement("reason-code")]
    public int reasoncode;
    [XmlElement("message")]
    public string message;
    [XmlElement("new-balance")]
    public string newbalance;
  }
}
