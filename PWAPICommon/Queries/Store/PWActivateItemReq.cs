﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Store.PWActivateItemReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.Clan;
using PWAPICommon.Queries.Currency;
using PWAPICommon.Queries.General;
using PWAPICommon.Queries.Inventory;
using PWAPICommon.Queries.Player;
using PWAPICommon.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace PWAPICommon.Queries.Store
{
  [ComVisible(true)]
  public class PWActivateItemReq : PWRequestBase
  {
    public long UserId;
    public int ActivationCount;
    public Guid TargetInstanceId;
    public string ActivationData;
    private PWCombinedReq LookupRequest;
    private PWStoreQueryUserReq StoreReq;
    private PWInvQueryReq InvQueryReq;
    public PWInventoryItem TargetInventoryItem;
    private StoreItem LinkedStoreItem;
    private PWRequestBase ActivationHandler;
    private Dictionary<Guid, PWInventoryItem> ChangedItems;
    public EPurchaseResult ActivateResult;
    private string ActivationStep;
    private static readonly List<PWActivationHandler> ActivationHandlers;

    public List<PWInventoryItem> UpdateItems => this.ChangedItems == null ? new List<PWInventoryItem>() : this.ChangedItems.Values.ToList<PWInventoryItem>();

    static PWActivateItemReq()
    {
      ActivationHandlerDataGenerator InGenerator1 = (ActivationHandlerDataGenerator) ((UserData, Req) => Req.UserId.ToString() + ":" + UserData);
      ActivationHandlerDataGenerator InGenerator2 = (ActivationHandlerDataGenerator) ((UserData, Req) => Req.UserId.ToString() + ":" + (object) Req.LinkedStoreItem.GetTokenActivationQuantity());
      ActivationHandlerDataGenerator InGenerator3 = (ActivationHandlerDataGenerator) ((UserData, Req) => Req.UserId.ToString() + ":" + (object) Req.LinkedStoreItem.GetTokenActivationQuantity() + ":T");
      PWActivateItemReq.ActivationHandlers = new List<PWActivationHandler>();
      PWActivateItemReq.AddActivationHandler(71, typeof (PWChangeNameReq), InGenerator1, EPurchaseResult.EPR_UnknownError);
      PWActivateItemReq.AddActivationHandler(72, typeof (PWGenderChangeReq), InGenerator1, EPurchaseResult.EPR_UnknownError);
      PWActivateItemReq.AddActivationHandler(73, typeof (PWUpdateSkillsReq), InGenerator1, EPurchaseResult.EPR_UnknownError);
      PWActivateItemReq.AddActivationHandler(74, typeof (PWClanCreate), InGenerator1, EPurchaseResult.EPR_UnknownError);
      PWActivateItemReq.AddActivationHandler(76000, 76499, typeof (PWGPAddReq), InGenerator2, EPurchaseResult.EPR_UnknownError);
      PWActivateItemReq.AddActivationHandler(76500, 76999, typeof (PWAddExperienceReq), InGenerator3, EPurchaseResult.EPR_UnknownError);
    }

    protected static bool AddActivationHandler(
      int ItemId,
      Type RequestType,
      ActivationHandlerDataGenerator InGenerator,
      EPurchaseResult InResult)
    {
      return PWActivateItemReq.AddActivationHandler(ItemId, ItemId, RequestType, InGenerator, InResult);
    }

    protected static bool AddActivationHandler(
      int ItemIdBegin,
      int ItemIdEnd,
      Type RequestType,
      ActivationHandlerDataGenerator InGenerator,
      EPurchaseResult InResult)
    {
      if (PWActivateItemReq.ActivationHandlers.Any<PWActivationHandler>((Func<PWActivationHandler, bool>) (x => x.Intersects(ItemIdBegin, ItemIdEnd))))
        return false;
      PWActivateItemReq.ActivationHandlers.Add(new PWActivationHandler(ItemIdBegin, ItemIdEnd, RequestType, InGenerator, InResult));
      return true;
    }

    protected static PWActivationHandler GetActivationHandler(int ItemId) => PWActivateItemReq.ActivationHandlers.SingleOrDefault<PWActivationHandler>((Func<PWActivationHandler, bool>) (x => x.Contained(ItemId)));

    public PWActivateItemReq()
      : this((PWConnectionBase) null)
    {
    }

    public PWActivateItemReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
      this.ActivateResult = EPurchaseResult.EPR_UnknownError;
      this.ActivationCount = 1;
      this.ChangedItems = new Dictionary<Guid, PWInventoryItem>();
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] stringArray = this.Serializer.DeserializeToStringArray(InMessage);
      if (stringArray.Length < 2 || !long.TryParse(stringArray[0], out this.UserId) || !Guid.TryParse(stringArray[1], out this.TargetInstanceId))
        return false;
      if (stringArray.Length >= 3)
      {
        this.ActivationData = stringArray[2];
        for (int index = 3; index < stringArray.Length; ++index)
        {
          PWActivateItemReq pwActivateItemReq = this;
          pwActivateItemReq.ActivationData = pwActivateItemReq.ActivationData + ":" + stringArray[index];
        }
      }
      return true;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ActivateItem;

    protected override bool ProcessServerResult()
    {
      if (this.Successful)
        new PWInvUpdatedReq(this.Connection, this.UserId, this.ChangedItems.Values.ToList<PWInventoryItem>()).SubmitServerQuery();
      return this.SendMessage(this.SerializeMessage(((byte) this.ActivateResult).ToString()));
    }

    public override bool ParseServerResult(PWRequestBase ForReq) => this.ActivationHandler != null && this.ActivationHandler.Completed && this.ActivationHandler.Successful;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.LookupRequest == null)
      {
        this.InvQueryReq = new PWInvQueryReq(this.Connection);
        this.InvQueryReq.UserId = this.UserId;
        this.StoreReq = new PWStoreQueryUserReq(this.Connection);
        this.StoreReq.UserId = this.UserId;
        this.StoreReq.UserType = (byte) 0;
        this.StoreReq.Filter = "";
        this.LookupRequest = new PWCombinedReq(this.Connection);
        this.LookupRequest.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        this.LookupRequest.AddSubRequest((PWRequestBase) this.InvQueryReq);
        this.LookupRequest.AddSubRequest((PWRequestBase) this.StoreReq);
        this.ActivationStep = "Querying Store/Inventory";
        return this.LookupRequest.SubmitServerQuery();
      }
      if (this.LookupRequest.Successful && this.TargetInventoryItem == null && (this.LinkedStoreItem == (StoreItem) null && this.InvQueryReq.Successful))
      {
        this.ActivationStep = "Finding Item Match in Store/Inventory";
        this.TargetInventoryItem = this.InvQueryReq.ReadInventory.Items.Find((Predicate<PWInventoryItem>) (x => x.InstanceId == this.TargetInstanceId));
        if (this.TargetInventoryItem != null)
        {
          this.Log("Activate Found Inventory Item", false, ELoggingLevel.ELL_Verbose);
          this.LinkedStoreItem = this.TargetInventoryItem.FindParentStoreItem(this.StoreReq.Items);
          if (this.LinkedStoreItem != (StoreItem) null)
          {
            this.Log("Activate Found Store Item", false, ELoggingLevel.ELL_Verbose);
            return this.SubmitServerQuery();
          }
          this.Log("Activate Failed to Find Store Item", false, ELoggingLevel.ELL_Verbose);
          this.ActivateResult = EPurchaseResult.EPR_InvalidItem;
          return this.ProcessServerResultDefault();
        }
        this.Log("Activate Failed to Find Inventory Item", false, ELoggingLevel.ELL_Verbose);
        this.ActivateResult = EPurchaseResult.EPR_InvalidItem;
        return this.ProcessServerResultDefault();
      }
      if (this.TargetInventoryItem != null && this.LinkedStoreItem != (StoreItem) null && this.ActivationHandler == null)
      {
        this.ActivationStep = "Running Activation Handler";
        this.ActivateResult = this.LinkedStoreItem.ValidateActivate(this.InvQueryReq.ReadInventory);
        if (this.ActivateResult == EPurchaseResult.EPR_MaxOwned)
        {
          this.Log("Max Owned Cap Reached for User " + this.UserId.ToString(), false, ELoggingLevel.ELL_Verbose);
          return this.ProcessServerResultDefault();
        }
        this.ActivationHandler = this.CreateActivationHandler();
        if (this.ActivationHandler != null)
        {
          this.ActivationHandler.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
          return this.ActivationHandler.SubmitServerQuery();
        }
        this.Log("Activate Process Failed", false, ELoggingLevel.ELL_Warnings);
        this.ActivateResult = EPurchaseResult.EPR_UnknownError;
        return this.ProcessServerResultDefault();
      }
      if (this.ActivationHandler != null && this.ActivationHandler.Completed && this.ActivationHandler.Successful)
      {
        this.Log("Activate Successful", false, ELoggingLevel.ELL_Informative);
        this.ActivateResult = EPurchaseResult.EPR_Success;
        this.Log("activate userid=" + (object) this.UserId + " instanceid=" + (object) this.TargetInstanceId + " itemid=" + (object) this.TargetInventoryItem.ItemId, true, ELoggingLevel.ELL_Verbose);
        return this.ProcessServerResultDefault();
      }
      if (this.TargetInventoryItem != null)
      {
        PWActivateItemReq pwActivateItemReq = this;
        pwActivateItemReq.ActivationStep = pwActivateItemReq.ActivationStep + " [" + (object) this.TargetInventoryItem.ItemId + ", " + (object) this.TargetInventoryItem.UserId + ", " + (object) this.TargetInventoryItem.InstanceId + "]";
      }
      this.Log("Activate Unknown Failure: " + this.ActivationStep, false, ELoggingLevel.ELL_Errors);
      this.ActivateResult = EPurchaseResult.EPR_UnknownError;
      return this.ProcessServerResultDefault();
    }

    private PWRequestBase CreateActivationHandler()
    {
      List<PWRequestBase> pwRequestBaseList = new List<PWRequestBase>();
      int val2 = this.ActivationCount;
      if (this.LinkedStoreItem.ActivationType == (byte) 4)
      {
        val2 = Math.Min(this.TargetInventoryItem.UsesLeft, val2);
        this.TargetInventoryItem.UsesLeft -= val2;
        if (this.TargetInventoryItem.UsesLeft <= 0)
        {
          pwRequestBaseList.Add((PWRequestBase) new PWInvDestroyReq(this.Connection)
          {
            ItemToRemove = this.TargetInventoryItem
          });
          this.TargetInventoryItem.UserId = PWServerBase.AdminUserID;
        }
        else
        {
          PWSqlItemUpdateReq<PWInventoryItem> sqlItemUpdateReq = new PWSqlItemUpdateReq<PWInventoryItem>(this.Connection);
          sqlItemUpdateReq.Item = this.TargetInventoryItem;
          pwRequestBaseList.Add((PWRequestBase) sqlItemUpdateReq);
        }
      }
      else if (this.LinkedStoreItem.ActivationType == (byte) 3 || this.LinkedStoreItem.ActivationType == (byte) 2)
      {
        pwRequestBaseList.Add((PWRequestBase) new PWInvDestroyReq(this.Connection)
        {
          ItemToRemove = this.TargetInventoryItem
        });
        this.TargetInventoryItem.UserId = PWServerBase.AdminUserID;
      }
      else
      {
        PWSqlItemUpdateReq<PWInventoryItem> sqlItemUpdateReq = new PWSqlItemUpdateReq<PWInventoryItem>(this.Connection);
        sqlItemUpdateReq.Item = this.TargetInventoryItem;
        pwRequestBaseList.Add((PWRequestBase) sqlItemUpdateReq);
        this.TargetInventoryItem.Activated = true;
        this.TargetInventoryItem.ActivationDate = PWServerBase.CurrentTime;
        this.TargetInventoryItem.ExpirationDate = StoreItem.GetExpirationDate((EPurchaseLength) this.TargetInventoryItem.PurchaseDuration);
      }
      for (int index = 0; index < val2; ++index)
      {
        PWTransactionAddReq transactionAddReq1 = new PWTransactionAddReq(this.Connection);
        transactionAddReq1.Item = new PWTransaction(this.UserId, ETransactionType.ETT_Activate, this.TargetInstanceId);
        pwRequestBaseList.Add((PWRequestBase) transactionAddReq1);
        this.ChangedItems[this.TargetInventoryItem.InstanceId] = this.TargetInventoryItem;
        PWActivationHandler activationHandler = PWActivateItemReq.GetActivationHandler(this.LinkedStoreItem.ItemId);
        if (activationHandler != null)
        {
          this.Log("Item has an activation handler specified, using that", false, ELoggingLevel.ELL_Verbose);
          byte[] bytes = new UTF8Encoding().GetBytes(activationHandler.GetActivationData(this.ActivationData, this));
          PWRequestBase instance = (PWRequestBase) Activator.CreateInstance(activationHandler.HandlerType, (object) this.Connection);
          if (instance.ParseServerQuery(bytes))
          {
            instance.ForceProcessResult = true;
            pwRequestBaseList.Add(instance);
          }
          else
          {
            this.Log("Request " + instance.ToString() + " is not valid, ignoring!", false, ELoggingLevel.ELL_Warnings);
            return (PWRequestBase) null;
          }
        }
        else if (this.LinkedStoreItem.ActivationType == (byte) 1)
        {
          this.Log("Item has an activation handler specified, using that", false, ELoggingLevel.ELL_Verbose);
          Guid result;
          if (Guid.TryParse(this.ActivationData, out result))
            this.TargetInventoryItem.AffectedInvId = result;
          else
            this.Log("Bad Target Item for Apply Activation Type " + this.ActivationData, false, ELoggingLevel.ELL_Warnings);
        }
        else
        {
          List<PWInventoryItem> NewItems = new List<PWInventoryItem>();
          List<PWInventoryItem> DeleteItems = new List<PWInventoryItem>();
          List<PWInventoryItem> ChangedItems = new List<PWInventoryItem>();
          if (this.LinkedStoreItem.GenerateActivationRequests(this.UserId, this.InvQueryReq.ReadInventory.Items, this.TargetInventoryItem, this.StoreReq.Items, ref NewItems, ref DeleteItems, ref ChangedItems))
          {
            foreach (PWInventoryItem pwInventoryItem in NewItems)
            {
              PWSqlItemAddReq<PWInventoryItem> pwSqlItemAddReq = new PWSqlItemAddReq<PWInventoryItem>(this.Connection);
              pwSqlItemAddReq.Item = pwInventoryItem;
              pwRequestBaseList.Add((PWRequestBase) pwSqlItemAddReq);
              PWTransactionAddReq transactionAddReq2 = new PWTransactionAddReq(this.Connection);
              transactionAddReq2.Item = new PWTransaction(this.UserId, ETransactionType.ETT_Activate, this.TargetInstanceId, pwInventoryItem.InstanceId);
              pwRequestBaseList.Add((PWRequestBase) transactionAddReq2);
              this.Log("activate userid=" + (object) this.UserId + " instanceid=" + (object) this.TargetInstanceId + " generated itemid=" + (object) pwInventoryItem.ItemId + " instanceid=" + (object) pwInventoryItem.InstanceId, true, ELoggingLevel.ELL_Verbose);
              this.ChangedItems.Add(pwInventoryItem.InstanceId, pwInventoryItem);
            }
            foreach (PWInventoryItem pwInventoryItem in DeleteItems)
            {
              PWSqlItemRemoveReq<PWInventoryItem> sqlItemRemoveReq = new PWSqlItemRemoveReq<PWInventoryItem>(this.Connection);
              sqlItemRemoveReq.Item = pwInventoryItem;
              pwRequestBaseList.Add((PWRequestBase) sqlItemRemoveReq);
              PWTransactionAddReq transactionAddReq2 = new PWTransactionAddReq(this.Connection);
              transactionAddReq2.Item = new PWTransaction(this.UserId, ETransactionType.ETT_Destroy, this.TargetInstanceId, pwInventoryItem.InstanceId);
              pwRequestBaseList.Add((PWRequestBase) transactionAddReq2);
              this.Log("activate userid=" + (object) this.UserId + " instanceid=" + (object) this.TargetInstanceId + " consumed itemid=" + (object) pwInventoryItem.ItemId + " instanceid=" + (object) pwInventoryItem.InstanceId, true, ELoggingLevel.ELL_Verbose);
              this.ChangedItems.Add(pwInventoryItem.InstanceId, pwInventoryItem);
            }
            foreach (PWInventoryItem pwInventoryItem in ChangedItems)
            {
              PWSqlItemUpdateReq<PWInventoryItem> sqlItemUpdateReq = new PWSqlItemUpdateReq<PWInventoryItem>(this.Connection);
              sqlItemUpdateReq.Item = pwInventoryItem;
              pwRequestBaseList.Add((PWRequestBase) sqlItemUpdateReq);
              PWTransactionAddReq transactionAddReq2 = new PWTransactionAddReq(this.Connection);
              transactionAddReq2.Item = new PWTransaction(this.UserId, ETransactionType.ETT_Destroy, this.TargetInstanceId, pwInventoryItem.InstanceId);
              pwRequestBaseList.Add((PWRequestBase) transactionAddReq2);
              this.Log("activate userid=" + (object) this.UserId + " instanceid=" + (object) this.TargetInstanceId + " consumed itemid=" + (object) pwInventoryItem.ItemId + " instanceid=" + (object) pwInventoryItem.InstanceId, true, ELoggingLevel.ELL_Verbose);
              this.ChangedItems.Add(pwInventoryItem.InstanceId, pwInventoryItem);
            }
          }
        }
      }
      PWCombinedReq pwCombinedReq = new PWCombinedReq(this.Connection);
      foreach (PWRequestBase NewRequest in pwRequestBaseList)
        pwCombinedReq.AddSubRequest(NewRequest);
      return (PWRequestBase) pwCombinedReq;
    }
  }
}
