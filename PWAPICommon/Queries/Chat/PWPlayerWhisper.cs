﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Chat.PWPlayerWhisper
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Chat
{
  [ComVisible(true)]
  public class PWPlayerWhisper : PWSocialReqBase
  {
    private SocialLoginData OtherPlayer;
    private string ChatMessage;

    public PWPlayerWhisper()
      : base((PWConnectionBase) null)
    {
    }

    public PWPlayerWhisper(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InData)
    {
      base.ParseServerQuery(InData);
      if (this.MsgArray.Length < 2)
        return false;
      object msg;
      try
      {
        msg = this.MsgArray[1];
      }
      catch (Exception ex)
      {
        if (ex is NullReferenceException)
        {
          this.Log("Chat server caught Null reference exception when trying to access MsgArray[1].", false, ELoggingLevel.ELL_Warnings);
          throw;
        }
        else
          throw;
      }
      try
      {
        this.ChatMessage = Convert.ToString(msg);
      }
      catch (Exception ex)
      {
        if (ex is NullReferenceException)
        {
          this.Log("Chat server caught Null reference exception when trying to convert content message.", false, ELoggingLevel.ELL_Warnings);
          throw;
        }
        else
          throw;
      }
      for (int index = 2; index < this.MsgArray.Length; ++index)
      {
        PWPlayerWhisper pwPlayerWhisper = this;
        pwPlayerWhisper.ChatMessage = pwPlayerWhisper.ChatMessage + ":" + Convert.ToString(this.MsgArray[index]);
      }
      return true;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_PlayerWhisper;

    public override bool SubmitServerQuery()
    {
      this.OtherPlayer = this.SocialServer.GetLoginData(Convert.ToString(this.MsgArray[0]));
      if (this.OtherPlayer == null)
        return this.SendMessage(this.SerializeMessage());
      string str = this.Encode64(this.ChatMessage);
      this.Log("whisper userid=" + Convert.ToString(this.ClientData.PlayerInfo.UserID) + ", targetuserid=" + Convert.ToString(this.OtherPlayer.PlayerInfo.UserID) + ", content=\"" + str + "\"", true, ELoggingLevel.ELL_Verbose);
      return this.OtherPlayer.Connection.SendMessage(this.MessageType, this.Serializer.Serialize("T:" + this.ClientData.PlayerInfo.UserName + ":" + this.ChatMessage));
    }

    protected override bool ProcessServerResult() => throw new NotImplementedException();

    public override bool SubmitClientQuery() => throw new NotImplementedException();

    protected override bool ProcessClientResult(PWRequestBase ForRequest) => throw new NotImplementedException();
  }
}
