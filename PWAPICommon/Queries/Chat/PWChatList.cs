﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Chat.PWChatList
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Chat
{
  [ComVisible(true)]
  public class PWChatList : PWSocialReqBase
  {
    protected string OutString;
    protected int ChannelCount;

    public PWChatList()
      : base((PWConnectionBase) null)
    {
    }

    public PWChatList(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ChatList;

    public override bool ParseServerQuery(byte[] InData) => true;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.OutString = "";
      this.ChannelCount = 0;
      if (this.ClientData != null && this.ClientData.ListeningRooms != null)
      {
        foreach (ChatRoom listeningRoom in this.ClientData.ListeningRooms)
        {
          ++this.ChannelCount;
          PWChatList pwChatList = this;
          pwChatList.OutString = pwChatList.OutString + listeningRoom.ToString() + ":";
        }
        return this.ProcessServerResultDefault();
      }
      this.Log("Attempted to get chatlist for null ClientData, or for Client with empty ListeningRooms", false, ELoggingLevel.ELL_Warnings);
      return false;
    }

    protected override bool ProcessServerResult() => this.SendMessage(this.Serializer.Serialize("T:" + Convert.ToString(this.ChannelCount) + ":" + this.OutString));
  }
}
