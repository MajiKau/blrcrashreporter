﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Chat.PWSocialLogin
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.Clan;
using PWAPICommon.Queries.Friends;
using PWAPICommon.Queries.Player;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Chat
{
  [ComVisible(true)]
  public class PWSocialLogin : PWSocialReqBase
  {
    private bool LoginSuccess;
    public long UserID;
    public string UserName;
    private bool IsStressTest;

    public PWSocialLogin()
      : base((PWConnectionBase) null)
    {
    }

    public PWSocialLogin(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_Login;

    public override bool SubmitClientQuery() => this.SendMessage(this.Serializer.Serialize(Convert.ToString(this.UserID) + ":" + this.UserName + ":StressTest"));

    public override bool ParseServerQuery(byte[] InData)
    {
      base.ParseServerQuery(InData);
      if (this.MsgArray.Length < 2)
      {
        this.Log("Not enough parameters for query", false, ELoggingLevel.ELL_Warnings);
        return false;
      }
      string s = Convert.ToString(this.MsgArray[0]);
      if (!long.TryParse(s, out this.UserID))
      {
        this.Log("Failed to parse User ID, as a string it was: " + s, false, ELoggingLevel.ELL_Errors);
        return false;
      }
      this.UserName = Convert.ToString(this.MsgArray[1]);
      if (this.MsgArray.Length == 3 && Convert.ToString(this.MsgArray[2]) == "StressTest")
        this.IsStressTest = true;
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.ClientData = new SocialLoginData(this.Connection);
      this.ClientData.PlayerInfo.UserID = this.UserID;
      this.ClientData.PlayerInfo.UserName = this.UserName;
      this.LoginSuccess = this.SocialServer.AddLoginData(this.ClientData);
      if (!this.LoginSuccess)
      {
        this.Log("Players login attempt failed after two attempts. Data is: " + this.ClientData.ToString(), false, ELoggingLevel.ELL_Warnings);
        this.SocialServer.RemoveLoginData(this.UserName, this.UserID, this.Connection);
        return this.ProcessServerResultDefault();
      }
      this.Log("New social login for Player " + this.ClientData.ToString(), false, ELoggingLevel.ELL_Verbose);
      this.SocialServer.AddListener(Convert.ToInt32((object) ChatMode.CM_GLOBAL), ref this.ClientData);
      SocialStatus socialStatus = new SocialStatus();
      socialStatus.StatusContext = (byte) 3;
      this.ClientData.UpdateLocation(socialStatus);
      if (this.Connection != null)
      {
        this.Connection.UserID = this.ClientData.PlayerInfo.UserID;
        this.Connection.Name = this.ClientData.PlayerInfo.UserID.ToString();
        this.OwningServer.ApplyUserIdentity<SocialStatus>(this.Connection, this.ClientData.PlayerInfo.UserID, socialStatus);
      }
      this.Log("Player " + this.ClientData.ToString() + "completed social login", false, ELoggingLevel.ELL_Informative);
      if (!this.IsStressTest)
      {
        new PWClanGetTag(this.Connection).SubmitServerQuery();
        PWQueryProfileReq pwQueryProfileReq = new PWQueryProfileReq(this.Connection);
        pwQueryProfileReq.ForUserID = this.ClientData.PlayerInfo.UserID;
        pwQueryProfileReq.ServerResultProcessor = new ProcessDelegate(this.HandleProfileQuery);
        pwQueryProfileReq.SubmitServerQuery();
        new PWFriendQueryReq(this.Connection)
        {
          UserId = this.ClientData.PlayerInfo.UserID
        }.SubmitServerQuery();
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.LoginSuccess;

    protected override bool ProcessServerResult() => this.SendDefaultMessage();

    protected virtual bool HandleProfileQuery(PWRequestBase ForRequest)
    {
      if (ForRequest is PWQueryProfileReq pwQueryProfileReq && pwQueryProfileReq.Successful && this.ClientData != null)
        this.ClientData.PlayerInfo.CopyFromUserProfile(pwQueryProfileReq.FoundProfile);
      return true;
    }
  }
}
