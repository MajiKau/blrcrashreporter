﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Chat.PWAdminMessage
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Chat
{
  [ComVisible(true)]
  public class PWAdminMessage : PWSocialReqBase
  {
    private const byte ContentIndex = 3;
    public string ContentMsg;
    public byte AdminMsgType;
    public string PlayerIdentifier;
    public bool bIsMajor;
    private long PlayerID;

    public PWAdminMessage()
      : base((PWConnectionBase) null)
    {
    }

    public PWAdminMessage(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_AdminMessage;

    protected override void StartUpReq()
    {
      try
      {
        this.SocialServer = this.Connection.OwningServer as PWSocialServer;
      }
      catch (Exception ex)
      {
        switch (ex)
        {
          case InvalidCastException _:
          case NullReferenceException _:
            this.SocialServer = (PWSocialServer) null;
            break;
          default:
            throw;
        }
      }
      if (this.SocialServer == null || this.ClientData != null)
        return;
      this.ClientData = this.SocialServer.GetLoginData(this.Connection);
    }

    public override bool ParseClientQuery(byte[] InMessage) => true;

    public override bool SubmitClientQuery()
    {
      if (this.ContentMsg == "")
        return false;
      string str = Convert.ToString(this.AdminMsgType) + (this.bIsMajor ? ":1:" : ":0:");
      if (this.AdminMsgType == (byte) 1)
        str += this.PlayerIdentifier;
      return this.Connection.SendMessage(this.MessageType, this.Serializer.Serialize(str + ":" + this.ContentMsg));
    }

    protected override bool ProcessClientResult(PWRequestBase ForRequest) => true;

    public override bool ParseServerQuery(byte[] InData)
    {
      if (this.ClientData != null && !this.ClientData.PlayerInfo.IsDeveloper)
        return false;
      base.ParseServerQuery(InData);
      return this.MsgArray.Length >= 4;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.AdminMsgType = Convert.ToByte(this.MsgArray[0]);
      this.bIsMajor = Convert.ToBoolean(Convert.ToByte(this.MsgArray[1]));
      this.PlayerIdentifier = Convert.ToString(this.MsgArray[2]);
      long.TryParse(this.PlayerIdentifier, out this.PlayerID);
      this.ContentMsg = Convert.ToString(this.MsgArray[3]);
      for (int index = 4; index < this.MsgArray.Length; ++index)
      {
        PWAdminMessage pwAdminMessage = this;
        pwAdminMessage.ContentMsg = pwAdminMessage.ContentMsg + ":" + Convert.ToString(this.MsgArray[index]);
      }
      this.Log("Admin Message Received: " + this.ContentMsg, false, ELoggingLevel.ELL_Verbose);
      this.Log("AdminMessage, content=\"" + this.Encode64(this.ContentMsg) + "\"", true, ELoggingLevel.ELL_Verbose);
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => true;

    protected override bool ProcessServerResult()
    {
      bool flag = false;
      byte[] numArray = this.Serializer.Serialize("T:" + (this.bIsMajor ? "1:" : "0:") + this.ContentMsg);
      switch (this.AdminMsgType)
      {
        case 0:
          this.SocialServer.SendMessageToAllClients(this.MessageType, numArray);
          flag = true;
          break;
        case 1:
          SocialLoginData socialLoginData = this.PlayerID == 0L ? this.SocialServer.GetLoginData(this.PlayerIdentifier) : this.SocialServer.GetLoginData(this.PlayerID);
          flag = socialLoginData != null && socialLoginData.Connection.SendMessage(this.MessageType, numArray);
          break;
      }
      if (this.ClientData == null)
        this.Connection.SendMessage(this.MessageType, this.Serializer.Serialize(flag ? "T" : "F"));
      return true;
    }

    public enum AdminMessageType
    {
      AMT_BROADCAST,
      AMT_PLAYER,
    }
  }
}
