﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Chat.PWQueryVoiceServer
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Chat
{
  [ComVisible(true)]
  public class PWQueryVoiceServer : PWRequestBase
  {
    private int HandlerID;
    private int Port;
    private char[] ChannelName;

    public PWQueryVoiceServer()
      : base((PWConnectionBase) null)
    {
    }

    public PWQueryVoiceServer(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage) => true;

    protected override EMessageType GetMessageType() => EMessageType.EMT_QueryVoice;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      PWVoiceServer resource = this.OwningServer.GetResource<PWVoiceServer>();
      if (resource != null)
      {
        this.HandlerID = 0;
        this.Port = 0;
        this.ChannelName = new char[10];
        resource.DoServerCreate(this.Connection, out this.HandlerID, out this.ChannelName, out this.Port);
      }
      else
        this.Log("Attempted to do voice server query when the voice server is not up!", false, ELoggingLevel.ELL_Errors);
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.Port != 0;

    protected override bool ProcessServerResult()
    {
      this.Log("Submitting voice query message with HandlerID: " + this.HandlerID.ToString() + " Port: " + this.Port.ToString() + " and channel name: " + new string(this.ChannelName), false, ELoggingLevel.ELL_Informative);
      return this.SendMessage(this.SerializeMessage(this.HandlerID.ToString() + ":" + this.Port.ToString() + ":" + new string(this.ChannelName)));
    }
  }
}
