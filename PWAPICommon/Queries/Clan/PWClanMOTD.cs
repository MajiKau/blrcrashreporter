﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Clan.PWClanMOTD
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Clan
{
  [ComVisible(true)]
  public class PWClanMOTD : PWSocialReqBase
  {
    private string DesiredMOTD;

    public PWClanMOTD()
      : base((PWConnectionBase) null)
    {
    }

    public PWClanMOTD(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ClanMOTD;

    public override bool ParseServerQuery(byte[] InData)
    {
      base.ParseServerQuery(InData);
      if (this.MsgArray.Length < 1)
      {
        this.Log("ClanMOTD - Invalid parameter count", false, ELoggingLevel.ELL_Informative);
        return false;
      }
      this.DesiredMOTD = Convert.ToString(this.MsgArray[0]);
      for (int index = 1; index < this.MsgArray.Length; ++index)
      {
        PWClanMOTD pwClanMotd = this;
        pwClanMotd.DesiredMOTD = pwClanMotd.DesiredMOTD + ":" + this.MsgArray[index];
      }
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.ClientData == null || this.ClientData.ClanInfo == null)
        return this.ProcessServerResultDefault();
      if (PWSocialServer.HasClanPermissions(this.ClientData.ClanInfo.FindMember(this.ClientData.PlayerInfo.UserID)))
      {
        this.Log("Valid MOTD change request " + this.DesiredMOTD, false, ELoggingLevel.ELL_Verbose);
        SqlCommand sqlCommand = new SqlCommand("SP_Clan_Set_Message");
        sqlCommand.CommandType = CommandType.StoredProcedure;
        sqlCommand.AddParameter<byte[]>("@ID", SqlDbType.VarBinary, this.ClientData.ClanInfo.ClanItem.ClanID.ToByteArray());
        sqlCommand.AddParameter<string>("@Message", SqlDbType.VarChar, this.DesiredMOTD);
        PWSQLReq pwsqlReq = new PWSQLReq(sqlCommand, this.Connection);
        pwsqlReq.ServerParseOverride = new ProcessDelegate(this.ParseDBReq);
        pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
        return pwsqlReq.SubmitServerQuery();
      }
      this.Log("ClanMOTd - no permissions to change MOTD", false, ELoggingLevel.ELL_Informative);
      return this.ProcessServerResult();
    }

    protected bool ParseDBReq(PWRequestBase ForRequest) => ForRequest is PWSQLReq pwsqlReq && pwsqlReq.ResponseValues != null && pwsqlReq.ResponseValues.Count > 0;

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (ForRequest is PWSQLReq pwsqlReq && pwsqlReq.Successful)
      {
        this.Log("setmotd userid=" + (object) this.ClientData.PlayerInfo.UserID + " " + this.ClientData.ClanInfo.ClanItem.ToPWString() + " MOTD=" + this.DesiredMOTD, true, ELoggingLevel.ELL_Informative);
        this.ClientData.ClanInfo.ClanItem.MOTD = this.DesiredMOTD;
        return true;
      }
      this.Log("ClanMOTD - Request was null or failed. ClientData is " + (object) this.ClientData != null ? this.ClientData.ToString() : "null", false, ELoggingLevel.ELL_Errors);
      return false;
    }

    protected override bool ProcessServerResult()
    {
      if (!this.Successful)
        return this.SendDefaultMessage();
      byte[] Message = this.SerializeMessage(this.DesiredMOTD);
      if (this.SocialServer == null)
        return this.SendMessage(Message);
      this.SocialServer.SendMessageToClanMates(this.MessageType, ref Message, this.ClientData);
      return true;
    }
  }
}
