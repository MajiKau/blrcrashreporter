﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Clan.PWClanQuit
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Clan
{
  [ComVisible(true)]
  public class PWClanQuit : PWClanReqBase
  {
    public long PlayerID;

    public PWClanQuit()
      : base((PWConnectionBase) null)
    {
    }

    public PWClanQuit(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ClanQuit;

    public override bool ParseServerQuery(byte[] InData)
    {
      base.ParseServerQuery(InData);
      if (this.ClientData == null)
      {
        this.Log("ClanQuit - no client data.", false, ELoggingLevel.ELL_Warnings);
        return false;
      }
      if (this.ClientData.ClanInfo == null)
      {
        this.Log("ClanQuit  - no clan info attached to client data. User Info: " + this.PrintClientInfo(), false, ELoggingLevel.ELL_Warnings);
        return false;
      }
      this.PlayerID = this.ClientData.PlayerInfo.UserID;
      return true;
    }

    public override bool SubmitServerQuery()
    {
      if (this.PlayerID == 0L)
      {
        this.Log("ClanQuit - PlayerID is zero and was not detected in ParseServerQuery", false, ELoggingLevel.ELL_Errors);
        return false;
      }
      if (this.ClientData.ClanInfo.ClanItem.OwnerID == this.PlayerID)
      {
        this.Log("ClanQuit - Player is owner and cannot quit clan, must disband", false, ELoggingLevel.ELL_Informative);
        return this.ProcessServerResultDefault();
      }
      base.SubmitServerQuery();
      this.Log("Updating DB with quit request", false, ELoggingLevel.ELL_Verbose);
      SqlCommand sqlCommand = new SqlCommand("SP_ClanMembers_Remove");
      sqlCommand.CommandType = CommandType.StoredProcedure;
      sqlCommand.AddParameter<long>("@UserID", SqlDbType.BigInt, this.PlayerID);
      PWSQLReq pwsqlReq = new PWSQLReq(sqlCommand, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (!(ForRequest is PWSQLReq pwsqlReq))
        return false;
      if (pwsqlReq.Successful)
      {
        if (this.ClientData != null)
        {
          this.Log("clanquit userid=" + Convert.ToString(this.ClientData.PlayerInfo.UserID) + " " + this.ClientData.ClanInfo.ClanItem.ToPWString(), true, ELoggingLevel.ELL_Verbose);
          lock (this.ClientData)
          {
            this.ClientData.RefreshOurDataToClan(EClanMemberUpdateType.ECMUT_LeaveClan);
            this.ClientData.ClanInfo.RemoveMember(this.ClientData.PlayerInfo.UserID);
            this.ClientData.ClanInfo = (PWClanInfo) null;
          }
        }
        return true;
      }
      this.Log("ClanQuit - Request failed parse. ClientData is: " + this.ClientData.ToString(), false, ELoggingLevel.ELL_Errors);
      return false;
    }

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
