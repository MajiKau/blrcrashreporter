﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Backend.PWSetConfigValueReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Config;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Backend
{
  [ComVisible(true)]
  public class PWSetConfigValueReq : PWRequestBase
  {
    public PWSetConfigValueReq.EConfigAction Action;
    public string Key;
    public string Value;

    public PWSetConfigValueReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWSetConfigValueReq(PWConnectionBase InConn)
      : base(InConn)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_SetConfig;

    public override bool SubmitClientQuery()
    {
      if (this.Connection == null)
        return false;
      base.SubmitClientQuery();
      return this.SendMessage(this.SerializeSelf());
    }

    public override bool ParseClientQuery(byte[] InMessage) => InMessage.Length == 1 && InMessage[0] == (byte) 84;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      PWSetConfigValueReq setConfigValueReq = this.DeserializeSelf<PWSetConfigValueReq>(InMessage);
      if (setConfigValueReq == null)
        return false;
      this.Key = setConfigValueReq.Key;
      this.Value = setConfigValueReq.Value;
      this.Action = setConfigValueReq.Action;
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.Action == PWSetConfigValueReq.EConfigAction.ECA_SetKey)
      {
        this.Log("Setting Config Value [" + this.Key + "] to [" + this.Value + "]", false, ELoggingLevel.ELL_Errors);
        ConfigSettings.SetKey(this.Key, this.Value);
      }
      else if (this.Action == PWSetConfigValueReq.EConfigAction.ECA_ReloadAll)
      {
        this.Log("Reloading Configuration from disk", false, ELoggingLevel.ELL_Errors);
        ConfigSettings.Reload();
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => true;

    protected override bool ProcessServerResult() => this.SendDefaultMessage();

    protected override bool ProcessClientResult(PWRequestBase ForRequest) => true;

    public enum EConfigAction
    {
      ECA_SetKey,
      ECA_ReloadAll,
    }
  }
}
