﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Backend.PWInvalidateCacheReq`1
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Backend
{
  [ComVisible(true)]
  public abstract class PWInvalidateCacheReq<T> : PWRequestBase where T : PWCacheBase
  {
    public PWInvalidateCacheReq()
      : this((PWConnectionBase) null)
    {
    }

    public PWInvalidateCacheReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseClientQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 1 && strArray[0] == nameof (T);
    }

    public override bool SubmitClientQuery() => this.SendDefaultMessage();

    public override bool ParseServerQuery(byte[] InMessage) => true;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => true;

    protected override bool ProcessServerResult()
    {
      this.OwningServer.GetResource<T>()?.MarkDirty();
      return this.SendDefaultMessage();
    }
  }
}
