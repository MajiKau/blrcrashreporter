﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.PWRequestBase
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Queries
{
  [ComVisible(true)]
  public abstract class PWRequestBase
  {
    [XmlIgnore]
    public PWConnectionBase Connection;
    [XmlIgnore]
    private bool bSuccessful;
    [XmlIgnore]
    protected bool bSubmitted;
    [XmlIgnore]
    protected bool bCompleted;
    [XmlIgnore]
    public bool ForceProcessResult;
    [XmlIgnore]
    public ProcessDelegate ServerResultProcessor;
    [XmlIgnore]
    public ProcessDelegate ClientResultProcessor;
    [XmlIgnore]
    public ProcessDelegate ServerParseOverride;
    [XmlIgnore]
    protected MessageSerializer Serializer;
    [XmlIgnore]
    private DateTime CreationTime;

    protected PWServerBase OwningServer => this.Connection == null ? (PWServerBase) null : this.Connection.OwningServer;

    public bool Successful => this.bSuccessful;

    public bool Submitted => this.bSubmitted;

    public virtual bool Completed => this.bCompleted;

    public EMessageType MessageType => this.GetMessageType();

    [XmlIgnore]
    public virtual string WebResponse => !this.Successful ? "Failed, Request Failed" : "Succeeded, Message Sent";

    public PWRequestBase(PWConnectionBase InConnection)
    {
      this.Connection = InConnection;
      this.bSuccessful = false;
      this.bSubmitted = false;
      this.CreationTime = PWServerBase.CurrentTime;
      this.Serializer = new MessageSerializer();
    }

    protected abstract EMessageType GetMessageType();

    protected virtual string GetMessageString(bool bSuccess) => !bSuccess ? "F" : "T";

    public PWMessageItem GetMessagePairing(bool bSuccess) => new PWMessageItem(this.MessageType, this.GetMessageString(bSuccess));

    public virtual bool ParseServerQuery(byte[] InMessage) => false;

    public virtual bool ResubmitServerQuery(PWRequestBase CompletedRequest) => this.SubmitServerQuery();

    public virtual bool SubmitServerQuery() => this.SubmitBaseServerQuery();

    public virtual bool SubmitBaseServerQuery()
    {
      this.bSubmitted = true;
      PWServerStats pwServerStats = (PWServerStats) null;
      if (this.OwningServer != null)
        pwServerStats = this.OwningServer.GetResource<PWServerStats>();
      if (pwServerStats != null)
      {
        try
        {
          pwServerStats.MessageCreated(this.GetMessageType());
          pwServerStats.MessageTransactionStart(this.GetMessageType());
        }
        catch (NotImplementedException ex)
        {
        }
      }
      this.Log(this.ToString() + " SubmitServerQuery", false, ELoggingLevel.ELL_Verbose);
      return false;
    }

    public bool ProcessServerResultDefault() => this.ProcessServerResultBase(this);

    protected bool ProcessServerResultBase(PWRequestBase ForRequest)
    {
      if (ForRequest == null)
        this.Log(this.ToString() + " ProcessServerResultBase with NULL ForRequest", false, ELoggingLevel.ELL_Warnings);
      if (!this.bSubmitted)
        this.Log(this.ToString() + " ProcessServerResultBase without bSubmitted", false, ELoggingLevel.ELL_Warnings);
      this.Log(this.ToString() + " Start Parsing Result", false, ELoggingLevel.ELL_Verbose);
      bool flag = false;
      try
      {
        this.bSuccessful = this.ServerParseOverride == null ? this.ParseServerResult(ForRequest) : this.ServerParseOverride(ForRequest);
        this.Log(this.ToString() + " Finish Parsing Result=" + this.bSuccessful.ToString(), false, ELoggingLevel.ELL_Verbose);
      }
      catch (Exception ex)
      {
        this.Log("ParseResult Parse Exception: " + ex.ToString(), false, ELoggingLevel.ELL_Errors);
      }
      this.bCompleted = true;
      if (this.OwningServer != null)
        this.OwningServer.CancelQuery(this);
      try
      {
        this.Log(this.ToString() + " Start Processing Result", false, ELoggingLevel.ELL_Verbose);
        if (this.ServerResultProcessor != null)
          flag = this.ServerResultProcessor(this);
        if (this.ServerResultProcessor == null || this.ForceProcessResult)
          flag = this.ProcessServerResult();
        this.Log(this.ToString() + " Finish Processed Result=" + flag.ToString(), false, ELoggingLevel.ELL_Verbose);
        PWServerStats pwServerStats = (PWServerStats) null;
        if (this.OwningServer != null)
          pwServerStats = this.OwningServer.GetResource<PWServerStats>();
        if (pwServerStats != null)
        {
          try
          {
            if (!this.bSuccessful)
              pwServerStats.MessageFailed(this.GetMessageType());
            pwServerStats.MessageDuration(this.GetMessageType(), PWServerBase.CurrentTime - this.CreationTime);
            pwServerStats.MessageTransactionEnd(this.GetMessageType());
          }
          catch (NotImplementedException ex)
          {
          }
        }
      }
      catch (Exception ex)
      {
        this.Log("ProcessResult Process Exception: " + ex.ToString(), false, ELoggingLevel.ELL_Errors);
      }
      return flag;
    }

    public virtual bool ParseServerResult(PWRequestBase ForRequest) => ForRequest.Successful;

    protected virtual bool ProcessServerResult() => false;

    public virtual bool ParseClientQuery(byte[] InMessage) => false;

    public virtual bool SubmitClientQuery()
    {
      this.bSubmitted = true;
      PWServerStats pwServerStats = (PWServerStats) null;
      if (this.OwningServer != null)
        pwServerStats = this.OwningServer.GetResource<PWServerStats>();
      if (pwServerStats != null)
      {
        try
        {
          pwServerStats.MessageCreated(this.GetMessageType());
        }
        catch (NotImplementedException ex)
        {
        }
      }
      this.Log(this.ToString() + " SubmitClientQuery", false, ELoggingLevel.ELL_Verbose);
      return false;
    }

    public virtual bool ParseClientResult(PWRequestBase ForRequest) => true;

    public bool ProcessClientResultBase(PWRequestBase ForRequest)
    {
      this.Log(this.ToString() + " Start Parsing Client Result", false, ELoggingLevel.ELL_Verbose);
      this.bSuccessful = this.ParseClientResult(ForRequest);
      this.Log(this.ToString() + " Finish Parsing Client Result=" + this.bSuccessful.ToString(), false, ELoggingLevel.ELL_Verbose);
      this.bCompleted = true;
      if (this.OwningServer != null)
        this.OwningServer.CancelQuery(this);
      this.Log(this.ToString() + " Start Processing Client Result", false, ELoggingLevel.ELL_Verbose);
      bool flag;
      if (this.ClientResultProcessor != null)
      {
        flag = this.ClientResultProcessor(ForRequest);
        if (this.ForceProcessResult)
          this.ProcessClientResult(ForRequest);
      }
      else
        flag = this.ProcessClientResult(ForRequest);
      this.Log(this.ToString() + " Finish Processing Client Result=" + flag.ToString(), false, ELoggingLevel.ELL_Verbose);
      PWServerStats pwServerStats = (PWServerStats) null;
      if (this.OwningServer != null)
        pwServerStats = this.OwningServer.GetResource<PWServerStats>();
      if (pwServerStats != null)
      {
        try
        {
          if (!this.bSuccessful)
            pwServerStats.MessageFailed(this.GetMessageType());
          TimeSpan InDuration = PWServerBase.CurrentTime - this.CreationTime;
          pwServerStats.MessageDuration(this.GetMessageType(), InDuration);
        }
        catch (NotImplementedException ex)
        {
        }
      }
      return flag;
    }

    protected virtual bool ProcessClientResult(PWRequestBase ForRequest) => this.Successful;

    public virtual bool MatchesRequest(PWRequestBase Other) => this.GetType() == Other.GetType();

    protected virtual List<Type> GetSerializableTypes() => new List<Type>()
    {
      this.GetType()
    };

    protected byte[] SerializeMessage<T>(string ExtraString, T Data)
    {
      string InPreamble = this.bSuccessful ? nameof (T) : "F";
      if (ExtraString != "")
        InPreamble = InPreamble + ":" + ExtraString;
      return this.Serializer.Serialize<T>(InPreamble, Data);
    }

    protected byte[] SerializeMessage<T>(
      string ExtraString,
      T Data,
      XmlTinyAttribute[] IgnoreAttributes)
    {
      string InPreamble = this.bSuccessful ? nameof (T) : "F";
      if (ExtraString != "")
        InPreamble = InPreamble + ":" + ExtraString;
      return this.Serializer.Serialize<T>(InPreamble, Data, IgnoreAttributes);
    }

    protected byte[] SerializeMessage(string ExtraString, byte[] Data)
    {
      string InPreamble = this.bSuccessful ? "T" : "F";
      if (ExtraString != "")
        InPreamble = InPreamble + ":" + ExtraString;
      return this.Serializer.Serialize(InPreamble, Data);
    }

    protected byte[] SerializeMessage(string ExtraString)
    {
      string InPreamble = this.bSuccessful ? "T" : "F";
      if (ExtraString != "")
        InPreamble = InPreamble + ":" + ExtraString;
      return this.Serializer.Serialize(InPreamble);
    }

    protected byte[] SerializeMessage(byte[] InData)
    {
      string InPreamble = this.bSuccessful ? "T" : "F";
      if (InData.Length > 0)
        InPreamble += ":";
      byte[] numArray1 = this.Serializer.Serialize(InPreamble);
      byte[] numArray2 = new byte[numArray1.Length + InData.Length];
      Array.Copy((Array) numArray1, (Array) numArray2, numArray1.Length);
      Array.Copy((Array) InData, 0, (Array) numArray2, numArray1.Length, InData.Length);
      return numArray2;
    }

    protected byte[] SerializeMessage() => this.Serializer.Serialize(this.bSuccessful ? "T" : "F");

    protected byte[] CompressMessage(byte[] InData)
    {
      this.Log(this.ToString() + " Compressing Message", false, ELoggingLevel.ELL_Verbose);
      return this.Serializer.Compress(InData);
    }

    protected void Log(string Text, bool bLogToRemoteServer, ELoggingLevel InLogLevel)
    {
      if (this.OwningServer == null)
        return;
      this.OwningServer.Log(Text, this.Connection, bLogToRemoteServer, InLogLevel);
    }

    protected bool SendMessage(byte[] MessageData)
    {
      this.Log(this.ToString() + " Sending Blob Length " + MessageData.Length.ToString(), false, ELoggingLevel.ELL_Verbose);
      return this.Connection.SendMessage(this.MessageType, MessageData);
    }

    protected bool SendDefaultMessage()
    {
      this.Log(this.ToString() + " Sending Default T/F Response", false, ELoggingLevel.ELL_Verbose);
      return this.Connection.SendMessage(this.MessageType, this.Serializer.SerializeRaw(this.GetMessageString(this.Successful)));
    }

    protected virtual byte[] SerializeSelf() => this.Serializer.SerializeObject<PWRequestBase>(this, this.GetSerializableTypes().ToArray());

    protected virtual T DeserializeSelf<T>(byte[] InMessage)
    {
      object[] objArray = this.Serializer.Deserialize(InMessage, typeof (PWRequestBase), this.GetSerializableTypes().ToArray());
      return objArray.Length == 1 && objArray[0] != null && objArray[0].GetType() == this.GetType() ? (T) objArray[0] : default (T);
    }

    public string EnumToString(Enum Value) => Convert.ToString(Convert.ToByte((object) Value));
  }
}
