﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Friends.PWFriendAddReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.Player;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Friends
{
  [ComVisible(true)]
  public class PWFriendAddReq : PWRequestBase
  {
    private long UserId;
    private long FriendUserId;
    private string FriendName;
    private string Message;
    private PWFriendItem MyNewFriend;
    private PWFriendItem TheirNewFriend;
    private PWProfileQueryBasicReq TheirProfileLookupReq;
    private PWProfileQueryBasicReq MyBasicProfileLookupReq;
    private PWFriendQueryInternalReq FriendLookupReq;
    private PWFriendAddReq.EFriendAddResponse ResponseCode;

    public PWFriendAddReq()
      : base((PWConnectionBase) null)
      => this.ResponseCode = PWFriendAddReq.EFriendAddResponse.EFAR_UnknownError;

    public PWFriendAddReq(PWConnectionBase InClient)
      : base(InClient)
      => this.ResponseCode = PWFriendAddReq.EFriendAddResponse.EFAR_UnknownError;

    protected override EMessageType GetMessageType() => EMessageType.EMT_FriendAdd;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (strArray.Length != 4 || !long.TryParse(strArray[0], out this.UserId) || !long.TryParse(strArray[1], out this.FriendUserId))
        return false;
      this.FriendName = strArray[2];
      this.Message = strArray[3];
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.FriendLookupReq == null)
      {
        this.FriendLookupReq = new PWFriendQueryInternalReq(this.Connection);
        this.FriendLookupReq.UserId = this.UserId;
        this.FriendLookupReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.FriendLookupReq.SubmitServerQuery();
      }
      if (this.TheirProfileLookupReq == null)
      {
        this.TheirProfileLookupReq = new PWProfileQueryBasicReq(this.Connection);
        this.TheirProfileLookupReq.ForUserID = this.FriendUserId;
        this.TheirProfileLookupReq.ForUserName = this.FriendName;
        this.TheirProfileLookupReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.TheirProfileLookupReq.SubmitServerQuery();
      }
      if (this.MyBasicProfileLookupReq == null)
      {
        this.MyBasicProfileLookupReq = new PWProfileQueryBasicReq(this.Connection);
        this.MyBasicProfileLookupReq.ForUserID = this.UserId;
        this.MyBasicProfileLookupReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.MyBasicProfileLookupReq.SubmitServerQuery();
      }
      if (!this.FriendLookupReq.Successful || !this.MyBasicProfileLookupReq.Successful)
      {
        this.Log("Friends list look up or basic profile look up for initiaating user failed", false, ELoggingLevel.ELL_Errors);
        this.ResponseCode = PWFriendAddReq.EFriendAddResponse.EFAR_UnknownError;
        return this.ProcessServerResultDefault();
      }
      if (!this.TheirProfileLookupReq.Successful)
      {
        this.Log("Friend did not exist, sending back failed request", false, ELoggingLevel.ELL_Informative);
        this.ResponseCode = PWFriendAddReq.EFriendAddResponse.EFAR_UnknownPlayer;
        return this.ProcessServerResultDefault();
      }
      if (this.MyBasicProfileLookupReq.BasicProfile.UserName.ToLower() == this.TheirProfileLookupReq.BasicProfile.UserName.ToLower())
      {
        this.Log("User trying to invite themselves to be a friend, denied", false, ELoggingLevel.ELL_Informative);
        this.ResponseCode = PWFriendAddReq.EFriendAddResponse.EFAR_FriendYourself;
        return this.ProcessServerResultDefault();
      }
      this.FriendUserId = this.TheirProfileLookupReq.BasicProfile.UserId;
      PWFriendItem pwFriendItem = this.FriendLookupReq.AllFriends.Find((Predicate<PWFriendItem>) (x => x.OwnerID == this.FriendUserId || x.FriendID == this.FriendUserId));
      this.CreateFriendItemsForPush();
      if (pwFriendItem == null)
        return this.CreateNewFriendEntry();
      if ((int) pwFriendItem.State == (int) Convert.ToByte((object) PWFriendItem.FriendState.EFS_Pending))
      {
        this.ResponseCode = PWFriendAddReq.EFriendAddResponse.EFAR_AlreadyPending;
        return this.ProcessServerResultDefault();
      }
      if ((int) pwFriendItem.State == (int) Convert.ToByte((object) PWFriendItem.FriendState.EFS_Accepted))
      {
        this.ResponseCode = PWFriendAddReq.EFriendAddResponse.EFAR_AlreadyFriend;
        return this.ProcessServerResultDefault();
      }
      if ((int) pwFriendItem.State != (int) Convert.ToByte((object) PWFriendItem.FriendState.EFS_Denied))
        return this.ProcessServerResultDefault();
      PWFriendUpdateReq pwFriendUpdateReq = new PWFriendUpdateReq(this.Connection);
      pwFriendItem.State = Convert.ToByte((object) PWFriendItem.FriendState.EFS_Pending);
      long ownerId = pwFriendItem.OwnerID;
      pwFriendItem.OwnerID = pwFriendItem.FriendID;
      pwFriendItem.FriendID = ownerId;
      pwFriendUpdateReq.Item = pwFriendItem;
      pwFriendUpdateReq.ServerResultProcessor = new ProcessDelegate(this.TryCreateNewFriendEntry);
      return pwFriendUpdateReq.SubmitServerQuery();
    }

    private void CreateFriendItemsForPush()
    {
      this.MyNewFriend = new PWFriendItem();
      this.MyNewFriend.State = Convert.ToByte((object) PWFriendItem.FriendState.EFS_Pending);
      this.MyNewFriend.FriendID = this.TheirProfileLookupReq.BasicProfile.UserId;
      this.MyNewFriend.OwnerID = this.UserId;
      this.MyNewFriend.PlayerInfo.CopyFromUserProfile(this.TheirProfileLookupReq.BasicProfile);
      this.TheirNewFriend = new PWFriendItem();
      this.TheirNewFriend.State = Convert.ToByte((object) PWFriendItem.FriendState.EFS_Pending);
      this.TheirNewFriend.FriendID = this.MyBasicProfileLookupReq.BasicProfile.UserId;
      this.TheirNewFriend.OwnerID = this.UserId;
      this.TheirNewFriend.PlayerInfo.CopyFromUserProfile(this.MyBasicProfileLookupReq.BasicProfile);
    }

    private bool TryCreateNewFriendEntry(PWRequestBase ForRequest)
    {
      if (ForRequest == null || !ForRequest.Successful)
        return false;
      this.ResponseCode = PWFriendAddReq.EFriendAddResponse.EFAR_Success;
      return this.ProcessServerResultDefault();
    }

    private bool CreateNewFriendEntry()
    {
      SqlCommand sqlCommand = new SqlCommand("SP_Friend_Add");
      sqlCommand.CommandType = CommandType.StoredProcedure;
      sqlCommand.AddParameter<byte[]>("@ID ", SqlDbType.VarBinary, Guid.NewGuid().ToByteArray());
      sqlCommand.AddParameter<long>("@Owner ", SqlDbType.BigInt, this.UserId);
      sqlCommand.AddParameter<long>("@Friend ", SqlDbType.BigInt, this.FriendUserId);
      sqlCommand.AddParameter<DateTime>("@FriendTime ", SqlDbType.DateTime, PWServerBase.CurrentTime);
      sqlCommand.AddParameter<PWFriendItem.FriendState>("@State ", SqlDbType.TinyInt, PWFriendItem.FriendState.EFS_Pending);
      PWSQLReq pwsqlReq = new PWSQLReq(sqlCommand, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (!(ForRequest is PWSQLReq pwsqlReq) || !pwsqlReq.Successful)
        return false;
      this.Log("friendadd userid=" + (object) this.UserId + ", friendid=" + (object) this.FriendUserId, true, ELoggingLevel.ELL_Verbose);
      PWPlayerItemCache<List<PWFriendItem>> resource = this.OwningServer.GetResource<PWPlayerItemCache<List<PWFriendItem>>>();
      if (resource != null)
      {
        resource.MarkDirtyFor(this.UserId);
        resource.MarkDirtyFor(this.FriendUserId);
      }
      this.ResponseCode = PWFriendAddReq.EFriendAddResponse.EFAR_Success;
      return true;
    }

    protected override bool ProcessServerResult()
    {
      byte[] MessageData;
      if (this.MyNewFriend != null && this.TheirNewFriend != null && this.ResponseCode == PWFriendAddReq.EFriendAddResponse.EFAR_Success)
      {
        byte[] Data1 = this.Serializer.SerializeObject<PWFriendItem>(this.MyNewFriend);
        MessageData = this.Serializer.Serialize("T:" + this.ResponseCode.ToString(), Data1);
        byte[] Data2 = this.Serializer.SerializeObject<PWFriendItem>(this.TheirNewFriend);
        byte[] BinaryData = this.Serializer.Serialize("T:" + this.ResponseCode.ToString(), Data2);
        SocialLoginData loginData = (this.Connection.OwningServer != null ? (PWSocialServer) this.Connection.OwningServer : (PWSocialServer) null)?.GetLoginData(this.FriendUserId);
        if (loginData != null && loginData.Connection != null)
          loginData.Connection.SendMessage(EMessageType.EMT_FriendAdd, BinaryData);
        PWPlayerItemCache<List<PWFriendItem>> resource = this.OwningServer.GetResource<PWPlayerItemCache<List<PWFriendItem>>>();
        if (resource != null)
        {
          resource.MarkDirtyFor(this.UserId);
          resource.MarkDirtyFor(this.FriendUserId);
        }
      }
      else
        MessageData = this.SerializeMessage(Convert.ToByte((object) this.ResponseCode).ToString());
      return this.SendMessage(MessageData);
    }

    private enum EFriendAddResponse
    {
      EFAR_Success,
      EFAR_UnknownPlayer,
      EFAR_AlreadyPending,
      EFAR_AlreadyFriend,
      EFAR_AlreadyDenied,
      EFAR_YourListFull,
      EFAR_TheirListFull,
      EFAR_FriendYourself,
      EFAR_UnknownError,
    }
  }
}
