﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Friends.PWFriendRemoveReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Friends
{
  [ComVisible(true)]
  public class PWFriendRemoveReq : PWRequestBase
  {
    public long UserId;
    public long FriendID;
    private PWFriendQueryInternalReq FriendQueryReq;
    private PWFriendItem FoundFriend;

    public PWFriendRemoveReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWFriendRemoveReq(PWConnectionBase InClient)
      : base(InClient)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_FriendRemove;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 2 && long.TryParse(strArray[0], out this.UserId) && (this.UserId > 0L && long.TryParse(strArray[1], out this.FriendID));
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.FriendQueryReq == null)
      {
        this.FriendQueryReq = new PWFriendQueryInternalReq(this.Connection);
        this.FriendQueryReq.UserId = this.UserId;
        this.FriendQueryReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.FriendQueryReq.SubmitServerQuery();
      }
      if (this.FriendQueryReq.Successful && this.FoundFriend == null)
      {
        this.FoundFriend = this.FriendQueryReq.AllFriends.Find((Predicate<PWFriendItem>) (x =>
        {
          if (x.OwnerID == this.FriendID && x.FriendID == this.UserId)
            return true;
          return x.OwnerID == this.UserId && x.FriendID == this.FriendID;
        }));
        if (this.FoundFriend != null)
        {
          SqlCommand sqlCommand = new SqlCommand("SP_Friend_Remove");
          sqlCommand.CommandType = CommandType.StoredProcedure;
          sqlCommand.AddParameter<byte[]>("@ID", SqlDbType.VarBinary, this.FoundFriend.UniqueID.ToByteArray());
          PWSQLReq pwsqlReq = new PWSQLReq(sqlCommand, this.Connection);
          pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
          return pwsqlReq.SubmitServerQuery();
        }
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (!(ForRequest is PWSQLReq pwsqlReq) || !pwsqlReq.Successful)
        return false;
      this.Log("friendremove userid=" + (object) this.UserId + ", friendid=" + (object) this.FriendID, true, ELoggingLevel.ELL_Verbose);
      PWPlayerItemCache<List<PWFriendItem>> resource = this.OwningServer.GetResource<PWPlayerItemCache<List<PWFriendItem>>>();
      if (resource != null)
      {
        resource.MarkDirtyFor(this.UserId);
        resource.MarkDirtyFor(this.FriendID);
      }
      return true;
    }

    protected override bool ProcessServerResult()
    {
      if (!this.Successful)
        return this.SendDefaultMessage();
      SocialLoginData loginData = (this.Connection.OwningServer != null ? (PWSocialServer) this.Connection.OwningServer : (PWSocialServer) null)?.GetLoginData(this.FriendID);
      if (loginData != null && loginData.Connection != null)
        loginData.Connection.SendMessage(EMessageType.EMT_FriendRemove, this.SerializeMessage(Convert.ToString(this.UserId)));
      return this.SendMessage(this.SerializeMessage(Convert.ToString(this.FriendID)));
    }
  }
}
