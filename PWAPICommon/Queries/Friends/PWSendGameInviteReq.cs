﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Friends.PWSendGameInviteReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Friends
{
  [ComVisible(true)]
  public class PWSendGameInviteReq : PWRequestBase
  {
    private long InviterPlayerId;
    private string InviterPlayerName;
    private long InviteePlayerId;

    public PWSendGameInviteReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWSendGameInviteReq(PWConnectionBase InClient)
      : base(InClient)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_FriendQuery;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 2 && long.TryParse(strArray[0], out this.InviteePlayerId) && (long.TryParse(strArray[1], out this.InviterPlayerId) && this.InviteePlayerId > 0L) && this.InviterPlayerId > 0L;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      PWSocialServer pwSocialServer = this.Connection.OwningServer != null ? (PWSocialServer) this.Connection.OwningServer : (PWSocialServer) null;
      SocialLoginData loginData1 = pwSocialServer.GetLoginData(this.InviterPlayerId);
      SocialLoginData loginData2 = pwSocialServer.GetLoginData(this.InviteePlayerId);
      if (loginData1 != null && loginData2 != null && (loginData2.Connection != null && loginData1.PlayerInfo != null) && loginData1.PlayerInfo.UserName != "")
      {
        this.InviterPlayerName = loginData1.PlayerInfo.UserName;
        byte[] BinaryData = this.Serializer.Serialize("T:" + (object) this.InviterPlayerId + ":" + this.InviterPlayerName);
        byte[] FinalBlob = (byte[]) null;
        MessageSerializer.Encode(EMessageType.EMT_GameInviteReceive, BinaryData, out FinalBlob);
        loginData2.Connection.SendEncodedMessage(EMessageType.EMT_GameInviteReceive, FinalBlob);
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.InviterPlayerName != "";

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
