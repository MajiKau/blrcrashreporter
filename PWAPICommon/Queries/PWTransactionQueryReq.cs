﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.PWTransactionQueryReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Wrappers;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries
{
  [ComVisible(true)]
  public class PWTransactionQueryReq : PWRequestBase
  {
    public List<PWTransaction> Transactions;

    public PWTransactionQueryReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWTransactionQueryReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      PWSqlItemQueryAllReq<PWTransaction> sqlItemQueryAllReq = new PWSqlItemQueryAllReq<PWTransaction>(this.Connection);
      sqlItemQueryAllReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return sqlItemQueryAllReq.SubmitServerQuery();
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_TransactionQuery;

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWSQLReq pwsqlReq = (PWSQLReq) ForRequest;
      if (pwsqlReq == null || pwsqlReq.ResponseValues == null || pwsqlReq.ResponseValues.Count == 0)
        return false;
      this.Log("Finished Reading Transactions " + (object) pwsqlReq.ResponseValues.Count + " Read", false, ELoggingLevel.ELL_Informative);
      string str = ((object[]) pwsqlReq.ResponseValues[0])[0].ToString();
      if (str.Contains("Account not Exists"))
      {
        this.Transactions = (List<PWTransaction>) null;
        return true;
      }
      if (str.Contains("Failed"))
        return false;
      this.Transactions = new List<PWTransaction>(pwsqlReq.ResponseValues.Count);
      this.Log("Start Creating Transactions", false, ELoggingLevel.ELL_Informative);
      for (int index = 0; index < pwsqlReq.ResponseValues.Count; ++index)
        this.Transactions.Add(new PWTransaction());
      this.Log("Start Parsing Transactions", false, ELoggingLevel.ELL_Informative);
      for (int index = 0; index < pwsqlReq.ResponseValues.Count; ++index)
      {
        object[] responseValue = (object[]) pwsqlReq.ResponseValues[index];
        this.Transactions[index].ParseFromArray(responseValue);
      }
      this.Log("Finished Parsing Transactions", false, ELoggingLevel.ELL_Informative);
      return true;
    }

    protected override bool ProcessServerResult() => this.Successful;
  }
}
