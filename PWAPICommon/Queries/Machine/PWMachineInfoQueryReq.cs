﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Machine.PWMachineInfoQueryReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Wrappers;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Machine
{
  [ComVisible(true)]
  public class PWMachineInfoQueryReq : PWRequestBase
  {
    public PWMachineID InMachineId;
    public PWMachineInfo OutMachineInfo;

    public PWMachineInfoQueryReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWMachineInfoQueryReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => throw new NotImplementedException();

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.OutMachineInfo = new PWMachineInfo();
      PWSqlItemQueryKeyedReq<PWMachineInfo, PWMachineID> itemQueryKeyedReq = new PWSqlItemQueryKeyedReq<PWMachineInfo, PWMachineID>(this.Connection);
      itemQueryKeyedReq.Key = this.InMachineId;
      itemQueryKeyedReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return itemQueryKeyedReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWSQLReq pwsqlReq = (PWSQLReq) ForRequest;
      return pwsqlReq != null && pwsqlReq.ResponseValues != null && (pwsqlReq.ResponseValues.Count == 1 && this.OutMachineInfo.ParseFromArray(pwsqlReq.ResponseValues[0] as object[]));
    }

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
