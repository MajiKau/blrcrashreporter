﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Inventory.PWInvUpdatedReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.General;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Inventory
{
  [ComVisible(true)]
  public class PWInvUpdatedReq : PWNotificationReq<PWInventoryItem>
  {
    public PWInvUpdatedReq()
    {
    }

    public PWInvUpdatedReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public PWInvUpdatedReq(
      PWConnectionBase InConnection,
      long InUserID,
      PWInventoryItem InUpdatedItem)
      : this(InConnection, InUserID, new List<PWInventoryItem>()
      {
        InUpdatedItem
      })
    {
    }

    public PWInvUpdatedReq(
      PWConnectionBase InConnection,
      long InUserID,
      List<PWInventoryItem> InUpdatedItems)
      : base(InConnection, InUserID, (IEnumerable<PWInventoryItem>) InUpdatedItems)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_InventoryUpdated;

    public override object Clone()
    {
      PWInvUpdatedReq pwInvUpdatedReq = new PWInvUpdatedReq();
      pwInvUpdatedReq.UserID = this.UserID;
      pwInvUpdatedReq.Items = this.Items;
      return (object) pwInvUpdatedReq;
    }

    protected override bool ProcessServerResult() => this.SendMessage(this.CompressMessage(this.SerializeMessage<PWInventoryItem[]>(this.UserID.ToString(), ((IEnumerable<PWInventoryItem>) this.Items).ToArray<PWInventoryItem>())));
  }
}
