﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Inventory.PWInvQueryReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Wrappers;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Inventory
{
  [ComVisible(true)]
  public class PWInvQueryReq : PWRequestBase
  {
    public long UserId;
    public PWInventory ReadInventory;
    public bool bInvalidateCached;

    public override string WebResponse => !this.Successful ? base.WebResponse : this.Serializer.SerializeObjectToString<PWInventory>(this.ReadInventory);

    public PWInvQueryReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWInvQueryReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_QueryInventory;

    public override bool ParseClientQuery(byte[] InMessage)
    {
      object[] objArray = this.Serializer.Deserialize(InMessage, typeof (PWInventory));
      if (objArray.Length != 3 || !(objArray[0].ToString() == "T") || !long.TryParse(objArray[1].ToString(), out this.UserId))
        return false;
      this.ReadInventory = objArray[2] as PWInventory;
      return this.ReadInventory != null;
    }

    public override bool SubmitClientQuery() => this.SendMessage(this.Serializer.SerializeRaw(this.UserId.ToString() + ":T"));

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (strArray.Length != 2 || !long.TryParse(strArray[0], out this.UserId))
        return false;
      this.bInvalidateCached = strArray[1] == "T";
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("Query Inventory for UserId " + (object) this.UserId, false, ELoggingLevel.ELL_Informative);
      PWPlayerItemCache<PWInventory> resource = this.Connection.OwningServer.GetResource<PWPlayerItemCache<PWInventory>>();
      if (resource != null)
      {
        if (this.bInvalidateCached)
          resource.MarkDirtyFor(this.UserId);
        this.ReadInventory = resource.GetCachedItem(this.UserId);
      }
      if (this.ReadInventory != null)
        return this.ProcessServerResultDefault();
      PWSqlItemQueryKeyedReq<PWInventoryItem, long> itemQueryKeyedReq = new PWSqlItemQueryKeyedReq<PWInventoryItem, long>(this.Connection);
      itemQueryKeyedReq.Key = this.UserId;
      itemQueryKeyedReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return itemQueryKeyedReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (this.ReadInventory != null)
        return true;
      if (!(ForRequest is PWSqlItemQueryKeyedReq<PWInventoryItem, long> itemQueryKeyedReq) || !itemQueryKeyedReq.Successful)
        return false;
      PWPlayerItemCache<PWInventory> resource = this.Connection.OwningServer.GetResource<PWPlayerItemCache<PWInventory>>();
      if (resource != null)
        this.ReadInventory = resource.GetCachedItem(this.UserId);
      if (this.ReadInventory == null)
        this.ReadInventory = new PWInventory(itemQueryKeyedReq.ResponseItems);
      return true;
    }

    protected override bool ProcessServerResult() => this.SendMessage(this.CompressMessage(this.SerializeMessage<PWInventory>(this.UserId.ToString(), this.ReadInventory)));
  }
}
