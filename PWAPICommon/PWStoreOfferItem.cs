﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PWStoreOfferItem
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Common;
using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class PWStoreOfferItem
  {
    [XmlAttribute("OG")]
    public Guid OfferGuid;
    [XmlAttribute("IG")]
    public Guid ItemGuid;
    [DefaultValue("")]
    [XmlAttribute("TN")]
    public string Tag;
    [XmlAttribute("LD")]
    public DateTime StartDate;
    [XmlAttribute("XD")]
    public DateTime EndDate;
    [DefaultValue(1f)]
    [XmlAttribute("DC")]
    public double Discount;
    [XmlAttribute("SI")]
    [DefaultValue(-1)]
    public int SortIndex;

    public PWStoreOfferItem()
    {
    }

    public PWStoreOfferItem(PWOfferItem OfferItem) => this.FromOfferItem(OfferItem);

    public void FromOfferItem(PWOfferItem OfferItem)
    {
      this.OfferGuid = OfferItem.UniqueID;
      this.ItemGuid = OfferItem.TargetItem;
      this.StartDate = OfferItem.StartDate;
      this.EndDate = OfferItem.EndDate;
      this.Discount = 1.0;
      this.SortIndex = OfferItem.SortIndex;
      switch (OfferItem.OfferTypeEnum)
      {
        case EOfferType.EOT_Featured:
          this.Tag = "Featured";
          break;
        case EOfferType.EOT_Discount:
          this.Tag = "Sale";
          this.Discount = OfferItem.Discount;
          break;
        case EOfferType.EOT_AARAdvertise:
          this.Tag = "AARAdvertise";
          break;
        case EOfferType.EOT_Popular:
          this.Tag = "Popular";
          break;
        case EOfferType.EOT_DealOfTheDay:
          this.Tag = "DealOfTheDay";
          this.Discount = OfferItem.Discount;
          break;
        case EOfferType.EOT_EventItem:
          this.Tag = "EventItem";
          break;
      }
    }
  }
}
