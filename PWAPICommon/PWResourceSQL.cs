﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PWResourceSQL
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Resources;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Timers;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class PWResourceSQL : IPWResource
  {
    private SqlConnection DBConnection;
    private PWServer OwningServer;
    private string DBName;
    private bool bDBOpen;
    private ConcurrentQueue<SqlCommand> Batch;
    private System.Timers.Timer BatchTimer;
    private string ConnectionString;

    public PWResourceSQL(string InConnectionString, PWServer InServer, string InDBName)
    {
      this.bDBOpen = false;
      this.DBName = InDBName;
      this.OwningServer = InServer;
      this.ConnectionString = InConnectionString;
      this.DBConnection = new SqlConnection();
      this.DBConnection.StateChange += new StateChangeEventHandler(this.StateChangeHandler);
      this.DBConnection.ConnectionString = InConnectionString;
      this.BatchTimer = new System.Timers.Timer(30000.0);
      this.BatchTimer.Elapsed += new ElapsedEventHandler(this.OnBatchTimer);
      this.Batch = new ConcurrentQueue<SqlCommand>();
    }

    private void StateChangeHandler(object mySender, StateChangeEventArgs myEvent)
    {
      bool flag = myEvent.OriginalState == ConnectionState.Open;
      this.bDBOpen = myEvent.CurrentState == ConnectionState.Open;
      this.OwningServer.Log(this.DBName + " Connection State Changed From " + myEvent.OriginalState.ToString() + " To " + myEvent.CurrentState.ToString(), (PWConnectionBase) null, false, (ELoggingLevel) (flag ? 2 : 4));
      this.TryConnectDB();
    }

    private void TryConnectDB()
    {
      lock (this.DBConnection)
      {
        try
        {
          if (this.bDBOpen && this.DBConnection.State == ConnectionState.Open)
            return;
          this.DBConnection.Open();
        }
        catch (Exception ex)
        {
          this.OwningServer.Log("Failed to connect to SQL Database [" + this.DBConnection.ConnectionString + "] Exception: " + ex.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Critical);
        }
      }
    }

    public bool SubmitRequest(PWSQLReq InRequest)
    {
      try
      {
        if (InRequest.bBatchQuery)
        {
          this.Batch.Enqueue(InRequest.RequestQuery);
          return InRequest.ProcessServerResultDefault();
        }
        if (!this.bDBOpen)
          return this.RetryRequest(InRequest, 100);
        InRequest.RequestQuery.Connection = this.DBConnection;
        InRequest.RequestQuery.BeginExecuteReader(new AsyncCallback(this.ExecuteReaderCallback), (object) InRequest);
        return true;
      }
      catch (InvalidOperationException ex)
      {
        this.OwningServer.Log(InRequest.ToString() + " SubmitRequest System.InvalidOperationException, Will retry in 100ms: " + ex.ToString(), InRequest.Connection, false, ELoggingLevel.ELL_Errors);
        return this.RetryRequest(InRequest, 100);
      }
      catch (SqlException ex)
      {
        this.OwningServer.Log(InRequest.ToString() + " SubmitRequest System.Data.SqlClient.SqlException, will retry in 100ms Exception: " + ex.ToString(), InRequest.Connection, false, ELoggingLevel.ELL_Errors);
        return this.RetryRequest(InRequest, 100);
      }
      catch (Exception ex)
      {
        this.OwningServer.Log(InRequest.ToString() + " SubmitRequest Exception: " + ex.ToString(), InRequest.Connection, false, ELoggingLevel.ELL_Errors);
        return InRequest.ProcessServerResultDefault();
      }
    }

    private bool RetryRequest(PWSQLReq InRequest, int MaxTimeoutMS)
    {
      if (!InRequest.CanRetry)
        return InRequest.ProcessServerResultDefault();
      int num = 0;
      int millisecondsTimeout = 50;
      this.TryConnectDB();
      for (; !this.bDBOpen && num < MaxTimeoutMS; num += millisecondsTimeout)
        Thread.Sleep(millisecondsTimeout);
      return this.SubmitRequest(InRequest);
    }

    private void ExecuteReaderCallback(IAsyncResult async)
    {
      PWSQLReq asyncState = (PWSQLReq) async.AsyncState;
      SqlDataReader sqlDataReader = (SqlDataReader) null;
      try
      {
        sqlDataReader = asyncState.RequestQuery.EndExecuteReader(async);
        if (sqlDataReader.HasRows)
        {
          object[] values = new object[sqlDataReader.FieldCount];
          while (sqlDataReader.Read())
          {
            sqlDataReader.GetValues(values);
            asyncState.ResponseValues.Add(values.Clone());
          }
          sqlDataReader.Close();
        }
        asyncState.ProcessServerResultDefault();
      }
      catch (SqlException ex)
      {
        this.OwningServer.Log(asyncState.ToString() + " System.Data.SqlClient.SqlException, will retry in 100ms Exception: " + ex.ToString(), asyncState.Connection, false, ELoggingLevel.ELL_Errors);
        this.RetryRequest(asyncState, 100);
      }
      catch (Exception ex)
      {
        sqlDataReader?.Close();
        this.OwningServer.Log(asyncState.ToString() + " ExecuteReaderCallback Exception: " + ex.ToString(), asyncState.Connection, false, ELoggingLevel.ELL_Errors);
        asyncState.ProcessServerResultDefault();
      }
    }

    public virtual void StartResource() => this.BatchTimer.Start();

    public virtual void StopResource()
    {
      this.BatchTimer.Stop();
      this.SubmitBatch();
    }

    public Dictionary<string, string> CheckHealth() => new Dictionary<string, string>();

    private void OnBatchTimer(object Source, ElapsedEventArgs args) => this.SubmitBatch();

    private bool SubmitBatch()
    {
      try
      {
        if (!this.Batch.IsEmpty)
        {
          this.OwningServer.Log("Submitting " + (object) this.Batch.Count + " batched SQL Queries", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
          Stopwatch stopwatch = new Stopwatch();
          stopwatch.Start();
          using (SqlConnection sqlConnection = new SqlConnection(this.ConnectionString))
          {
            sqlConnection.Open();
            using (SqlTransaction sqlTransaction = sqlConnection.BeginTransaction())
            {
              SqlCommand result = (SqlCommand) null;
              while (this.Batch.TryDequeue(out result))
              {
                result.Transaction = sqlTransaction;
                result.Connection = sqlConnection;
                result.ExecuteNonQuery();
              }
              sqlTransaction.Commit();
            }
          }
          stopwatch.Stop();
          this.OwningServer.Log("Batched Queries submitted in " + (object) stopwatch.ElapsedMilliseconds + "ms", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
        }
      }
      catch (Exception ex)
      {
        this.OwningServer.Log("Failed to submit batched SQL Queries with Exception: " + ex.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
      }
      return true;
    }
  }
}
