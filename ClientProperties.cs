﻿// Decompiled with JetBrains decompiler
// Type: ClientProperties
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;

[ComVisible(true)]
public enum ClientProperties
{
  CLIENT_UNIQUE_IDENTIFIER,
  CLIENT_NICKNAME,
  CLIENT_VERSION,
  CLIENT_PLATFORM,
  CLIENT_FLAG_TALKING,
  CLIENT_INPUT_MUTED,
  CLIENT_OUTPUT_MUTED,
  CLIENT_INPUT_HARDWARE,
  CLIENT_OUTPUT_HARDWARE,
  CLIENT_INPUT_DEACTIVATED,
  CLIENT_IDLE_TIME,
  CLIENT_DEFAULT_CHANNEL,
  CLIENT_DEFAULT_CHANNEL_PASSWORD,
  CLIENT_SERVER_PASSWORD,
  CLIENT_META_DATA,
  CLIENT_IS_MUTED,
  CLIENT_IS_RECORDING,
  CLIENT_VOLUME_MODIFICATOR,
  CLIENT_ENDMARKER,
}
