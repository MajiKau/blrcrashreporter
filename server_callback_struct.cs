﻿// Decompiled with JetBrains decompiler
// Type: server_callback_struct
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;

[ComVisible(true)]
public struct server_callback_struct
{
  public onVoiceDataEvent_type onVoiceDataEvent_delegate;
  public onClientStartTalkingEvent_type onClientStartTalkingEvent_delegate;
  public onClientStopTalkingEvent_type onClientStopTalkingEvent_delegate;
  public onClientConnected_type onClientConnected_delegate;
  public onClientDisconnected_type onClientDisconnected_delegate;
  public onClientMoved_type onClientMoved_delegate;
  public onChannelCreated_type onChannelCreated_delegate;
  public onChannelEdited_type onChannelEdited_delegate;
  public onChannelDeleted_type onChannelDeleted_delegate;
  public onServerTextMessageEvent_type onServerTextMessageEvent_delegate;
  public onChannelTextMessageEvent_type onChannelTextMessageEvent_delegate;
  public onUserLoggingMessageEvent_type onUserLoggingMessageEvent_delegate;
  public onAccountingErrorEvent_type onAccountingErrorEvent_delegate;
  public dummy_type dummy1_delegate;
  public dummy_type dummy2_delegate;
}
