﻿// Decompiled with JetBrains decompiler
// Type: callback
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Runtime.InteropServices;

[ComVisible(true)]
public static class callback
{
  public static void onClientConnected(
    ulong serverID,
    ushort clientID,
    ulong channelID,
    ref uint removeClientError)
  {
    Console.WriteLine("Client {0} joined channel {1} on virtual server {2}", (object) clientID, (object) channelID, (object) serverID);
  }

  public static void onClientDisconnected(ulong serverID, ushort clientID, ulong channelID) => Console.WriteLine("Client {0} left channel {1} on virtual server {2}", (object) clientID, (object) channelID, (object) serverID);

  public static void onClientMoved(
    ulong serverID,
    ushort clientID,
    ulong oldChannelID,
    ulong newChannelID)
  {
    Console.WriteLine("Client {0} moved from channel {1} to channel {2} on virtual server {3}\n", new object[4]
    {
      (object) clientID,
      (object) oldChannelID,
      (object) newChannelID,
      (object) serverID
    });
  }

  public static void onChannelCreated(ulong serverID, ushort invokerClientID, ulong channelID) => Console.WriteLine("Channel {0} created by {1} on virtual server {2}", (object) channelID, (object) invokerClientID, (object) serverID);

  public static void onChannelEdited(ulong serverID, ushort invokerClientID, ulong channelID) => Console.WriteLine("Channel {0} edited by {1} on virtual server {2}", (object) channelID, (object) invokerClientID, (object) serverID);

  public static void onChannelDeleted(ulong serverID, ushort invokerClientID, ulong channelID) => Console.WriteLine("Channel {0} deleted by {1} on virtual server {2}", (object) channelID, (object) invokerClientID, (object) serverID);

  public static void onServerTextMessageEvent(
    ulong serverID,
    ushort invokerClientID,
    string textMessage)
  {
    Console.WriteLine("Text message in server chat by {0}: {1}", (object) invokerClientID, (object) textMessage);
  }

  public static void onChannelTextMessageEvent(
    ulong serverID,
    ushort invokerClientID,
    ulong targetChannelID,
    string textMessage)
  {
    Console.WriteLine("Text message in channel ({0})chat by {1}: {2}", (object) targetChannelID, (object) invokerClientID, (object) textMessage);
  }

  public static void onClientStartTalkingEvent(ulong serverID, ushort clientID) => Console.WriteLine("onClientStartTalkingEvent serverID={0}, clientID={1}", (object) serverID, (object) clientID);

  public static void onClientStopTalkingEvent(ulong serverID, ushort clientID) => Console.WriteLine("onClientStopTalkingEvent serverID={0}, clientID={1}", (object) serverID, (object) clientID);

  public static void onAccountingErrorEvent(ulong serverID, int errorCode)
  {
    Console.WriteLine("onAccountingErrorEvent serverID={0}, errorCode={1}", (object) serverID, (object) errorCode);
    Environment.Exit(1);
  }
}
