﻿// Decompiled with JetBrains decompiler
// Type: CrashReporter.CrashReporterForm
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon;
using PWAPICommon.Common;
using PWAPICommon.Queries;
using PWAPICommon.Queries.Backend;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrashReporter
{
  public class CrashReporterForm : Form
  {
    private IContainer components;
    private TextBox EmailBox;
    private TextBox ReproBox;
    private Label label2;
    private Label label3;
    private Button SendButton;
    private Button CancelReportButton;
    private GroupBox InputGroup;
    private GroupBox StatusGroup;
    private RichTextBox StatusText;
    private Label label1;
    private TextBox NameBox;
    private Label label5;
    private ComboBox RegionCombo;
    private Label label4;
    private TextBox PlayerNameBox;
    private string[] LaunchArgs;
    private bool bFromServerCrash;
    private Font CompletedFont = new Font("Verdana", 8f, FontStyle.Regular, GraphicsUnit.Point);
    private Font NewFont = new Font("Verdana", 8f, FontStyle.Bold, GraphicsUnit.Point);
    private Task ReportTask;
    private string CurrentStep;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (CrashReporterForm));
      this.EmailBox = new TextBox();
      this.ReproBox = new TextBox();
      this.label2 = new Label();
      this.label3 = new Label();
      this.SendButton = new Button();
      this.CancelReportButton = new Button();
      this.InputGroup = new GroupBox();
      this.label1 = new Label();
      this.NameBox = new TextBox();
      this.label5 = new Label();
      this.RegionCombo = new ComboBox();
      this.label4 = new Label();
      this.PlayerNameBox = new TextBox();
      this.StatusGroup = new GroupBox();
      this.StatusText = new RichTextBox();
      this.InputGroup.SuspendLayout();
      this.StatusGroup.SuspendLayout();
      this.SuspendLayout();
      this.EmailBox.Location = new Point(147, 163);
      this.EmailBox.Name = "EmailBox";
      this.EmailBox.Size = new Size(325, 22);
      this.EmailBox.TabIndex = 3;
      this.ReproBox.Location = new Point(11, 235);
      this.ReproBox.Multiline = true;
      this.ReproBox.Name = "ReproBox";
      this.ReproBox.Size = new Size(508, 159);
      this.ReproBox.TabIndex = 4;
      this.label2.AutoSize = true;
      this.label2.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.label2.Location = new Point(11, 163);
      this.label2.Name = "label2";
      this.label2.Size = new Size(130, 20);
      this.label2.TabIndex = 4;
      this.label2.Text = "E-mail Address";
      this.label3.AutoSize = true;
      this.label3.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.label3.Location = new Point(90, 212);
      this.label3.Name = "label3";
      this.label3.Size = new Size(382, 20);
      this.label3.TabIndex = 5;
      this.label3.Text = "What were you doing when the game crashed?";
      this.SendButton.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.SendButton.Location = new Point(374, 400);
      this.SendButton.Name = "SendButton";
      this.SendButton.Size = new Size(145, 42);
      this.SendButton.TabIndex = 6;
      this.SendButton.Text = "Send";
      this.SendButton.UseVisualStyleBackColor = true;
      this.SendButton.Click += new EventHandler(this.SendButton_Click);
      this.CancelReportButton.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.CancelReportButton.Location = new Point(15, 400);
      this.CancelReportButton.Name = "CancelReportButton";
      this.CancelReportButton.Size = new Size(145, 42);
      this.CancelReportButton.TabIndex = 5;
      this.CancelReportButton.Text = "Don't Send";
      this.CancelReportButton.UseVisualStyleBackColor = true;
      this.CancelReportButton.Click += new EventHandler(this.CancelButton_Click);
      this.InputGroup.Controls.Add((Control) this.label1);
      this.InputGroup.Controls.Add((Control) this.NameBox);
      this.InputGroup.Controls.Add((Control) this.label5);
      this.InputGroup.Controls.Add((Control) this.RegionCombo);
      this.InputGroup.Controls.Add((Control) this.label4);
      this.InputGroup.Controls.Add((Control) this.PlayerNameBox);
      this.InputGroup.Controls.Add((Control) this.CancelReportButton);
      this.InputGroup.Controls.Add((Control) this.SendButton);
      this.InputGroup.Controls.Add((Control) this.EmailBox);
      this.InputGroup.Controls.Add((Control) this.label3);
      this.InputGroup.Controls.Add((Control) this.ReproBox);
      this.InputGroup.Controls.Add((Control) this.label2);
      this.InputGroup.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.InputGroup.Location = new Point(12, 2);
      this.InputGroup.Name = "InputGroup";
      this.InputGroup.Size = new Size(534, 448);
      this.InputGroup.TabIndex = 8;
      this.InputGroup.TabStop = false;
      this.InputGroup.Text = "Input";
      this.label1.AutoSize = true;
      this.label1.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.label1.Location = new Point(52, 133);
      this.label1.Name = "label1";
      this.label1.Size = new Size(89, 20);
      this.label1.TabIndex = 13;
      this.label1.Text = "Full Name";
      this.NameBox.Location = new Point(147, 131);
      this.NameBox.Name = "NameBox";
      this.NameBox.Size = new Size(325, 22);
      this.NameBox.TabIndex = 2;
      this.label5.AutoSize = true;
      this.label5.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.label5.Location = new Point(11, 54);
      this.label5.Name = "label5";
      this.label5.Size = new Size(286, 20);
      this.label5.TabIndex = 11;
      this.label5.Text = "What region were you logged into?";
      this.RegionCombo.DropDownStyle = ComboBoxStyle.DropDownList;
      this.RegionCombo.FormattingEnabled = true;
      this.RegionCombo.Items.AddRange(new object[3]
      {
        (object) "West",
        (object) "East",
        (object) "EU"
      });
      this.RegionCombo.Location = new Point(147, 87);
      this.RegionCombo.Name = "RegionCombo";
      this.RegionCombo.Size = new Size(121, 24);
      this.RegionCombo.TabIndex = 1;
      this.label4.AutoSize = true;
      this.label4.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.label4.Location = new Point(32, 21);
      this.label4.Name = "label4";
      this.label4.Size = new Size(109, 20);
      this.label4.TabIndex = 9;
      this.label4.Text = "Player Name";
      this.PlayerNameBox.Location = new Point(147, 21);
      this.PlayerNameBox.Name = "PlayerNameBox";
      this.PlayerNameBox.Size = new Size(325, 22);
      this.PlayerNameBox.TabIndex = 0;
      this.StatusGroup.Controls.Add((Control) this.StatusText);
      this.StatusGroup.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.StatusGroup.Location = new Point(553, 2);
      this.StatusGroup.Name = "StatusGroup";
      this.StatusGroup.Size = new Size(309, 448);
      this.StatusGroup.TabIndex = 9;
      this.StatusGroup.TabStop = false;
      this.StatusGroup.Text = "Status";
      this.StatusText.Location = new Point(7, 20);
      this.StatusText.Name = "StatusText";
      this.StatusText.ReadOnly = true;
      this.StatusText.Size = new Size(296, 422);
      this.StatusText.TabIndex = 0;
      this.StatusText.TabStop = false;
      this.StatusText.Text = "";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(884, 462);
      this.Controls.Add((Control) this.StatusGroup);
      this.Controls.Add((Control) this.InputGroup);
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.Name = nameof (CrashReporterForm);
      this.Text = "Blacklight: Retribution Crash Reporter";
      this.InputGroup.ResumeLayout(false);
      this.InputGroup.PerformLayout();
      this.StatusGroup.ResumeLayout(false);
      this.ResumeLayout(false);
    }

    public CrashReporterForm(string[] Args)
    {
      this.LaunchArgs = Args;
      this.InitializeComponent();
      this.RegionCombo.Items.Clear();
      this.RegionCombo.Items.AddRange((object[]) PWRegionMap.GetRegionNames().ToArray());
      if (this.LaunchArgs == null)
      {
        int num = (int) MessageBox.Show("Invalid number of launch args, expected 4, got " + this.LaunchArgs.Length.ToString());
        Application.Exit();
      }
      if (this.LaunchArgs.Length < 5)
      {
        int num = (int) MessageBox.Show("Invalid number of launch args, expected at least 4, got " + this.LaunchArgs.Length.ToString());
        Application.Exit();
      }
      if (this.LaunchArgs.Length >= 6)
        this.ReproBox.Text = this.LaunchArgs[5];
      if (this.LaunchArgs.Length >= 7 && this.LaunchArgs[6] == "server")
        this.bFromServerCrash = true;
      this.StartProgress("Killing Crashed Game");
      bool bResult = false;
      try
      {
        Process.GetProcessById(int.Parse(this.LaunchArgs[4])).Kill();
        bResult = true;
      }
      catch (Exception ex)
      {
      }
      this.EndProgress(bResult);
      if (this.bFromServerCrash)
      {
        this.PlayerNameBox.Text = "Dedicated Server";
        this.SendButton_Click((object) null, (EventArgs) null);
      }
      else
        this.StartProgress("Waiting For Input");
    }

    protected override void OnFormClosed(FormClosedEventArgs e)
    {
      this.PurgeDumpFiles();
      base.OnFormClosed(e);
    }

    private void StartProgress(string StepName)
    {
      this.CurrentStep = StepName;
      this.UpdateProgress("Working");
    }

    private void UpdateProgress(string AdditionalInfo) => this.UpdateProgress(AdditionalInfo, this.NewFont);

    private void UpdateProgress(string AdditionalInfo, Font StepFont)
    {
      if (this.InvokeRequired)
      {
        this.Invoke((Delegate) new Action<string, Font>(this.UpdateProgress), (object) AdditionalInfo, (object) StepFont);
      }
      else
      {
        int startIndex = this.StatusText.Text.IndexOf(this.CurrentStep);
        if (startIndex != -1)
          this.StatusText.Text = this.StatusText.Text.Remove(startIndex);
        string TextName = this.CurrentStep + "...";
        RichTextBox statusText = this.StatusText;
        statusText.Text = statusText.Text + this.CurrentStep + "... " + AdditionalInfo;
        this.FormatText(TextName, StepFont, Color.Black);
        this.FormatText("Succeeded", this.CompletedFont, Color.Green);
        this.FormatText("Failed", this.CompletedFont, Color.Red);
        this.StatusText.Update();
      }
    }

    private void EndProgress(bool bResult) => this.UpdateProgress((bResult ? "Succeeded" : "Failed") + Environment.NewLine, this.CompletedFont);

    private string GenerateDataSizeString(long nBytes)
    {
      long num1 = nBytes / 1024L;
      long num2 = num1 / 1024L;
      long num3 = num2 / 1024L;
      if (num3 > 0L)
      {
        long num4 = num2 - num3 * 1024L;
        return num3.ToString() + ((double) num4 / 1024.0).ToString(".00") + "GB";
      }
      if (num2 > 0L)
      {
        long num4 = num1 - num2 * 1024L;
        return num2.ToString() + ((double) num4 / 1024.0).ToString(".00") + "MB";
      }
      if (num1 <= 0L)
        return nBytes.ToString() + "B";
      nBytes -= num1 * 1024L;
      return num1.ToString() + ((double) nBytes / 1024.0).ToString(".00") + "KB";
    }

    private void SendFile(string StepName, PWServerClient Client, PWFileUploadReq FileUploadReq)
    {
      this.StartProgress(StepName);
      if (Client.SendQuery((PWRequestBase) FileUploadReq))
      {
        string dataSizeString = this.GenerateDataSizeString(FileUploadReq.TargetReadBytes);
        while (!FileUploadReq.Completed)
        {
          this.UpdateProgress("[" + this.GenerateDataSizeString(FileUploadReq.CurrentReadBytes) + " of " + dataSizeString + "]");
          Thread.Sleep(100);
        }
      }
      this.EndProgress(FileUploadReq.Successful);
    }

    private void ProcessStart(string RegionName)
    {
      try
      {
        this.EndProgress(true);
        this.StartProgress("Resolving Host");
        PWServerClient Client = new PWServerClient(RegionName, EServerType.EST_Report);
        Client.AddMessageMap(EMessageType.EMT_CrashReportID, typeof (PWCrashReportIDReq));
        this.EndProgress(true);
        string launchArg1 = this.LaunchArgs[0];
        string launchArg2 = this.LaunchArgs[1];
        int num1 = launchArg1.LastIndexOf("\\");
        string str1 = launchArg1.Remove(0, num1 + 1);
        int num2 = launchArg2.LastIndexOf("\\");
        string str2 = launchArg2.Remove(0, num2 + 1);
        this.StartProgress("Connecting to Server");
        Client.StartServer();
        if (!Client.WaitForConnect(3))
        {
          Client.StopServer("Failed, Client failed to connect");
          throw new Exception();
        }
        this.EndProgress(true);
        PWCrashReportIDReq crashReportIdReq = new PWCrashReportIDReq();
        crashReportIdReq.MachineID = Client.HardwareID;
        crashReportIdReq.Name = this.NameBox.Text;
        crashReportIdReq.Email = this.EmailBox.Text;
        crashReportIdReq.PlayerName = this.PlayerNameBox.Text;
        crashReportIdReq.Description = this.ReproBox.Text;
        crashReportIdReq.CrashID = this.ReadCrashSignature(this.LaunchArgs[3]);
        this.StartProgress("Sending Crash ID");
        this.EndProgress(Client.SendQuery((PWRequestBase) crashReportIdReq));
        this.StartProgress("Waiting For Crash ID Response");
        this.EndProgress(Client.WaitForQuery((PWRequestBase) crashReportIdReq, 3));
        if (crashReportIdReq.ResultCode == PWCrashReportIDReq.ECrashReportResult.ECRR_NeedMore || crashReportIdReq.ResultCode == PWCrashReportIDReq.ECrashReportResult.ECRR_New)
        {
          MessageSerializer messageSerializer = new MessageSerializer();
          string dxDiag = this.GenerateDxDiag();
          if (!string.IsNullOrEmpty(dxDiag))
            this.SendFile("Sending DxDiag", Client, new PWFileUploadReq()
            {
              InputFileData = messageSerializer.Serialize(dxDiag),
              OutputFilePath = crashReportIdReq.DestinationDir + "DxDiag.txt"
            });
          string InPreamble1 = this.ReadEngineIni(this.LaunchArgs[2]);
          if (!string.IsNullOrEmpty(InPreamble1))
            this.SendFile("Sending Config Files", Client, new PWFileUploadReq()
            {
              InputFileData = messageSerializer.Serialize(InPreamble1),
              OutputFilePath = crashReportIdReq.DestinationDir + "PCConsole-FoxEngine.ini"
            });
          string InPreamble2 = this.ReadPunkBusterFiles(this.LaunchArgs[2]);
          if (!string.IsNullOrEmpty(InPreamble2))
            this.SendFile("Sending PB Directory Info", Client, new PWFileUploadReq()
            {
              InputFileData = messageSerializer.Serialize(InPreamble2),
              OutputFilePath = crashReportIdReq.DestinationDir + "PbDirInfo.txt"
            });
          PWFileUploadReq FileUploadReq = new PWFileUploadReq();
          if (crashReportIdReq.ResultCode == PWCrashReportIDReq.ECrashReportResult.ECRR_New && !string.IsNullOrEmpty(launchArg2))
          {
            FileUploadReq.OutputFilePath = crashReportIdReq.DestinationDir + str2;
            FileUploadReq.InputFilePath = launchArg2;
          }
          else if (!string.IsNullOrEmpty(launchArg1))
          {
            FileUploadReq.OutputFilePath = crashReportIdReq.DestinationDir + str1;
            FileUploadReq.InputFilePath = launchArg1;
          }
          this.SendFile("Sending Crash Dump", Client, FileUploadReq);
        }
        Client.StopServer("Web Request Finished");
        this.ProcessFinished("Crash report sent successfully, thank you!", "SUCCESS");
      }
      catch (Exception ex)
      {
        this.EndProgress(false);
        string str1 = "ERROR";
        string str2 = "Failed to submit crash report.";
        if (this.bFromServerCrash)
        {
          this.ProcessFinished(str2, str1);
        }
        else
        {
          int num = (int) MessageBox.Show(str2, str1);
          this.InputGroup.Enabled = true;
        }
      }
    }

    private void ProcessFinished(string Message, string Caption)
    {
      if (this.InvokeRequired)
      {
        this.Invoke((Delegate) new Action<string, string>(this.ProcessFinished), (object) Message, (object) Caption);
      }
      else
      {
        if (!this.bFromServerCrash)
        {
          int num = (int) MessageBox.Show(Message, Caption);
        }
        Application.Exit();
      }
    }

    private void PurgeDumpFiles()
    {
      try
      {
        File.Delete(this.LaunchArgs[1]);
      }
      catch (Exception ex)
      {
      }
      try
      {
        File.Delete(this.LaunchArgs[0]);
      }
      catch (Exception ex)
      {
      }
    }

    private void FormatText(string TextName, Font F, Color C)
    {
      int start1;
      for (int start2 = 0; start2 < this.StatusText.TextLength; start2 = start1 + 1)
      {
        start1 = this.StatusText.Find(TextName, start2, RichTextBoxFinds.WholeWord);
        if (start1 < 0)
          break;
        this.StatusText.Select(start1, TextName.Length);
        this.StatusText.SelectionFont = F;
        this.StatusText.SelectionColor = C;
      }
    }

    private void SendButton_Click(object sender, EventArgs e)
    {
      Color red = Color.Red;
      Color white = Color.White;
      string ChosenRegionName = this.RegionCombo.SelectedItem != null ? this.RegionCombo.SelectedItem.ToString() : "";
      if (ChosenRegionName == "")
        ChosenRegionName = "WEST";
      if (this.PlayerNameBox.Text == "")
        this.PlayerNameBox.Text = "NotSpecified";
      else
        this.PlayerNameBox.BackColor = white;
      this.RegionCombo.BackColor = white;
      if (this.NameBox.Text == "")
        this.NameBox.Text = "NotSpecified";
      else
        this.NameBox.BackColor = white;
      if (this.EmailBox.Text == "")
        this.EmailBox.Text = "NotSpecified";
      else
        this.EmailBox.BackColor = white;
      if (this.ReproBox.Text == "")
        this.ReproBox.Text = "NotSpecified";
      else
        this.ReproBox.BackColor = white;
      this.InputGroup.Enabled = false;
      this.ReportTask = Task.Factory.StartNew((Action) (() => this.ProcessStart(ChosenRegionName)));
    }

    private void CancelButton_Click(object sender, EventArgs e) => Application.Exit();

    private string GenerateDxDiag()
    {
      this.StartProgress("Generating DxDiag");
      string path = "C:\\dxdiag.txt";
      bool bResult = false;
      string str = "";
      try
      {
        Process process = new Process();
        process.EnableRaisingEvents = false;
        process.StartInfo.FileName = "C:\\Windows\\system32\\dxdiag.exe";
        process.StartInfo.Arguments = "/whql:on /t" + path;
        process.Start();
        process.WaitForExit();
        FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
        if (fileStream != null)
        {
          byte[] numArray = new byte[fileStream.Length];
          fileStream.Read(numArray, 0, numArray.Length);
          str = new MessageSerializer().DeserializeToString(numArray);
          bResult = str != null && str != "";
        }
      }
      catch (Exception ex)
      {
      }
      this.EndProgress(bResult);
      return str;
    }

    private string ReadPunkBusterFiles(string BaseDir)
    {
      this.StartProgress("Reading PB Directory");
      bool bResult = false;
      string OutputInfo = "";
      try
      {
        this.GetDirectoryFileInfo(ref OutputInfo, new DirectoryInfo(BaseDir + "\\Binaries\\Win32\\pb"));
        bResult = OutputInfo != null && OutputInfo != "";
      }
      catch (Exception ex)
      {
        OutputInfo = "";
      }
      this.EndProgress(bResult);
      return OutputInfo;
    }

    private void GetDirectoryFileInfo(ref string OutputInfo, DirectoryInfo ParentDirectory)
    {
      FileInfo[] files = ParentDirectory.GetFiles();
      ref string local1 = ref OutputInfo;
      local1 = local1 + ParentDirectory.LastWriteTimeUtc.ToString("G") + "\t" + ParentDirectory.FullName + Environment.NewLine;
      foreach (DirectoryInfo directory in ParentDirectory.GetDirectories())
        this.GetDirectoryFileInfo(ref OutputInfo, directory);
      foreach (FileInfo fileInfo in files)
      {
        ref string local2 = ref OutputInfo;
        local2 = local2 + fileInfo.LastWriteTimeUtc.ToString("G") + "\t" + fileInfo.FullName + Environment.NewLine;
      }
    }

    private string ReadEngineIni(string BaseDir)
    {
      this.StartProgress("Reading INIs");
      bool bResult = false;
      string str = "";
      try
      {
        FileStream fileStream = new FileStream(BaseDir + "\\FoxGame\\Config\\PCConsole\\Cooked\\PCConsole-FoxEngine.ini", FileMode.Open, FileAccess.Read);
        if (fileStream != null)
        {
          byte[] numArray = new byte[fileStream.Length];
          fileStream.Read(numArray, 0, numArray.Length);
          str = new MessageSerializer().DeserializeToString(numArray);
        }
        bResult = str != null && str != "";
      }
      catch (Exception ex)
      {
      }
      this.EndProgress(bResult);
      return str;
    }

    private string ReadCrashSignature(string FileToLoad)
    {
      this.StartProgress("Reading Crash Signature");
      bool bResult = false;
      string str = "";
      try
      {
        FileStream fileStream = new FileStream(FileToLoad, FileMode.Open, FileAccess.Read);
        if (fileStream != null)
        {
          byte[] numArray = new byte[fileStream.Length];
          fileStream.Read(numArray, 0, numArray.Length);
          str = new MessageSerializer().DeserializeToString(numArray);
        }
        bResult = str != null && str != "";
      }
      catch (Exception ex)
      {
      }
      this.EndProgress(bResult);
      return str;
    }
  }
}
